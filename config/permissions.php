<?php
use Mona\Core\User\Membership;

return [
    Membership::NONE  => ['label'       => 'public',
                          'permissions' => ['quest_bar', 'blogs', 'create_photo']
    ],
    Membership::BASIC => ['label'       => 'basic',
                          'permissions' => ['profile']
    ],
    Membership::FULL  => ['label'       => 'full',
                          'permissions' => ['wheel', 'video_room','add_video', 'add_letter','letter_room']
    ],
    Membership::ADMIN => ['label'       => 'admin',
                          'permissions' => ['dashboard']
    ]
];