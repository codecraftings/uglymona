<!DOCTYPE html>
<html ng-app="app" ng-csp>
<head>
	<meta charset="utf-8">
	<title ng-bind="pageTitle">
		Uglymona
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="{{url('css/all.min.css')}}"/>
	<script type="text/javascript">
		var base_url = "{{url('/')}}";
		var api_url = "{{url('api/v1')}}";
		var asset_url = "{{url('/')}}";
	</script>
	<script type="text/javascript">
		document.createElement('video');document.createElement('audio');document.createElement('track');
	</script>
	<base href="{{url('').'/'}}"></base>
</head>
<body class="mona-app" mona-freezable="bg">
	@include("common.nav-bar2")
	<div id="main-wrapper">
		<div ng-view autoscroll="true">
		</div>
	</div>
	@include('common.footer')
	<div class="mona-page-loader" mona-page-loader>
		<span class="loading-icon"></span>
	</div>
	<div id="toaster"></div>
	<div inbox></div>
</body>
</html>