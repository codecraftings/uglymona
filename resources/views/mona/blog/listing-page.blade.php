@extends('layouts.blog')
@section('window-title')
Blog | @parent
@stop
@section('top-content')
<div class="cf"></div>
<div class="blog-room-slogan">
	Please check the Dear Mona life articles for inspirational stories, ideas & events
</div>
<div class="content-title blogroom">
	Add Your Video
</div>
@stop
@section('main-content')
<center style="font-size:25px;font-family:Lato;font-weight:300;margin:30px 0px;">No post added yet!</center>
<!--
<div class="blog-post">
	<span class="blog-date"><i class="sprite calender"></i> August 30, 2014</span>
	<h2 class="blog-title">5 Tips For Choosing <b>The Perfect First Date Outfit</b></h2>
	<div class="blog-content">
		<a class="blog-author">from Mona's Fashon Adviser</a>
		&nbsp;|&nbsp; &nbsp; 
		At MONA, we love luxury essentials. We always have. We can't imagine what we'd wear if not for the crisp, the clever and the classic.
	</div>
	<p><a href=# class="read-more-btn">Read More</a></p>
</div>
<div class="smallsep"></div>
<div class="blog-post">
	<span class="blog-date"><i class="sprite calender"></i> August 30, 2014</span>
	<h2 class="blog-title">5 Tips For Choosing <b>The Perfect First Date Outfit</b></h2>
	<div class="blog-content">
		<a class="blog-author">from Mona's Fashon Adviser</a>
		&nbsp;|&nbsp; &nbsp; 
		At MONA, we love luxury essentials. We always have. We can't imagine what we'd wear if not for the crisp, the clever and the classic.
	</div>
	<p><a href=# class="read-more-btn">Read More</a></p>
</div>
-->
<div class="cf"></div>
@stop