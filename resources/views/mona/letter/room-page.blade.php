@extends('layouts.main')

@section('window-title')
Mona's Room | @parent
@stop
@section('js-loading')
@parent
<script type="text/javascript">
    $(function(){
        $('.list-view-container').css("height", 460);
        _.defer(function(){
            $('.list-view-container').mCustomScrollbar({
                axis: "y",
                scrollbarPosition: "outside",
                autoHideScrollbar: true,
                snapAmount: 20,
                snapOffset: 0,
                //scrollInertia: 500,
                mouseWheel: {
                    normalizeDelta: true,
                    //scrollAmount: 218,
                    //deltaFactor: 88
                }
            });
        });

    })
</script>
@stop

@section('top-content')
<div class="cf"></div>
<div class="mona-room-slogan">
    When you are broken hearted, remember that at uglymona you are never alone.
</div>
<div class="content-title monaroom">
    Dear Mona - Letter Room
</div>
@stop
@section('separator')
<div class="cf content-title-fix full"></div>
@stop
@section('main-content')
<span class="fl hidden" style="">10 of 100 letters</span>
<a style="margin:20px 0px 10px;width:200px;" href="{{url('letter/add')}}" class="btn purple fr">Write A Letter To Mona</a>
<div class="cf"></div>
<div class="list-view-container">
    <ul class="list-view">
        @foreach($letters as $letter)
        <li class="list-item letter-list-item">
            <div class="single-letter-view">
                <div class="col-1 fl">
                    <img class="user-pic" src="{{$letter->user->profile->profile_pic}}"/>
                    <a class="user-link" href="{{$letter->user->permalink}}">{{$letter->user->present()->name()}}</a>
                </div>
                <div class="col-2 fr">
                    <div class="letter-message-view">
                        {{$letter->present()->formatted_message()}}
                    </div>
                    @if($letter->status>=Mona\Core\Letter\Letter::STATUS_REPLIED)
                    <div class="letter-reply-view">
                        {{$letter->present()->formatted_reply()}}
                    </div>
                    @endif
                </div>
                <div class="cf"></div>
            </div>
        </li>
        @endforeach
    </ul>
</div>
<div class="cf"></div>
@stop