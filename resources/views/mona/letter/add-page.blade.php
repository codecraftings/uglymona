@extends('layouts.main')

@section('window-title')
Mona's Room | @parent
@stop

@section('top-content')
<div class="cf"></div>
<div class="mona-room-slogan">
    When you are broken hearted, remember that at uglymona you are never alone.
</div>
<div class="content-title monaroom">
    Dear Mona - Letter Room
</div>
@stop
@section('separator')
<div class="cf content-title-fix full"></div>
@stop
@section('main-content')
<div class="main-form-container monaroom">
    @if(Session::has("error"))
    <div style="margin: 26px 0px -33px; font-size: 21px;" class="form-notice error">
        {{Session::get('error')}}
    </div> 
    @endif
    @if(Session::has("success"))
    <div style="margin: 26px 0px -33px; font-size: 21px;" class="form-notice success">
        Your letter is sent to Mona successfully. She will write back to you soon.
    </div> 
    @endif
    {{Form::open(array("method"=>"post", "autocomplete"=>"off"))}}
    <label class="monatextarealabel" for="monatextarea">Dear Mona,</label>
    <textarea name="message" id="monatextarea" placeholder="Write your letter here.." class="input"></textarea>
    <label class="letterfooter">Thank you Mona. Hope to hear back soon.</label>
    <br>
    <button type="submit" class="btn purple fr">Send Letter To Mona</button>
    <div class="mona-notice fr">
    Notice: Please note that Mona only responds once a week to one note at a time.
    </div>
    {{Form::close()}}
</div>
<div class="cf"></div>
@stop