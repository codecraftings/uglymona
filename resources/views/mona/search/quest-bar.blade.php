@extends('layouts.main')

@section('window-title')
    Quest Bar | @parent
@stop
@section("extra-attr",'ng-controller="QuestBarController as vm"')
@section('top-content')
    <div class="cf"></div>
    <div class="search-room-slogan">
        Use the Search Bar to find the love of your dream. Be sure to update your friendly profile data so that your
        profile can be located in the search.
    </div>
    <div class="searchbar-container">
        <form novalidate>
            <input type="text" ng-model="vm.searchString" class="mainsearchbar" placeholder="i.e. woman 20 years old in indiana">
            <input awe-button normal-action="vm.search()" control-state="vm.searchButtonState" type="submit"
                   class="mainsearchbutton"/>
            <a ng-click="vm.showOptionsPopup()" class="advanceoptbtn" href="javascript:void(0)">Advanced search options</a>
        </form>
    </div>
    <div mona-popup on-done="vm.searchFiltersUpdated()" visibility-condition="vm.optionsPopupVisible" max-width="500px" title="Advanced Search Options">
        <form>
            <div ng-repeat="field in vm.filterFields" class="form-field">
                <label class="field-label fl">@{{ field.label }}</label>
                <span class="field-control fr">
                    <div dropdown-select="field.options"
                         dropdown-model="vm.searchFilters[field.key]"
                            dropdown-item-label="label">
                    </div>
                </span>
                <div class="cf"></div>
            </div>
        </form>
    </div>
    <div class="content-title searchroom">
        <span class="paired-line" ng-show="vm.users.length<1">
            Use above bar to search for users
        </span>
        <span class="paired-line" ng-show="vm.users.length>0">
            @{{vm.users.length}} Profile(s) Found
        </span>
    </div>

@stop
@section('separator')
    <div class="cf content-title-fix full"></div>
@stop
@section('main-content')
    <div ng-hide="vm.loadingState" class="listing-tag">
        <span class="paired-line" ng-show="vm.users.length<1">
            Your search result will be displayed here!
        </span>
        <span class="paired-line" ng-show="vm.users.length>1">
            for more result scroll down your mouse
        </span>
    </div>
    <div ng-show="vm.loadingState" class="loading-indicator-group">
        <span class="loading-indicator first"></span>
        <span class="loading-indicator second"></span>
        <span class="loading-indicator third"></span>
        <span class="loading-indicator fourth"></span>
        <span class="loading-indicator fifth"></span>
    </div>

    <div scroll-view max-height="368px" class="users-list-container quest-bar">

        <ul class="users-list">
            <li ng-repeat="user in vm.users" class="users-list-item">
                <div class="user-thumb">
                    <div class="pp fl">
                        <img ng-src="@{{ user.profile.profile_pic}}"/>
                    </div>
                    <div class="info fl">
                        <p class="line1"><i class="sprite eye"></i> &nbsp;@{{ user.profile.view_count || 0 }} Views</p>

                        <p class="line2"><span>@{{ user.full_name|limit:15 }}</span>, @{{ user.profile.age }} y.o.</p>

                        <p ng-attr-title="@{{user.city+', '+user.state}}" class="line3">@{{user.city|limit:10}}, @{{ user.state|limit:8 }}</p>
                    </div>
                    <div class="cf"></div>
                </div>
            </li>
        </ul>
    </div>
    <div class="cf"></div>
@stop