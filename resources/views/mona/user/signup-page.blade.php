@extends('layouts.main')
@section('window-title')
Sign Up | @parent
@stop
@section('top-content')
<div class="cf"></div>
<div class="build-profile-slogan">
	Build Profile- Answer Questions to gain visibility
</div>
<div class="content-title signup">
	New To Uglymona?
</div>
@stop

@section('main-content')
<div class="cf content-title-fix"></div>
<div class="main-form-container">
	@if(Session::has('error'))
	<h3 class="form-title error">Error: {{Session::get('error')}}</h3>
	@elseif(Session::has('success'))
	<h3 class="form-title success">{{Session::get('success')}}</h3>
	@else
	<h3 class="form-title">Create an account below</h3>
	@endif
	<form id="signup-form" autocomplete="off" method="post" action="{{url('signup')}}">
		{{Form::token()}}
		<input type="text" name="username" value="{{Session::get('username')}}" placeholder="Username" class="input"/>
		<input type="text" name="email" value="{{Session::get('email')}}" placeholder="Email Address" class="input"/>
		<input type="password" name="password" value="{{Session::get('password')}}" placeholder="Password" class="input"/>
		<input type="password" name="password_confirmation" value="{{Session::get('password_confirmation')}}" placeholder="Confirm Password" class="input"/>
		<div class="form-extra">
			<input type="checkbox" name="terms" value="yes" {{Session::get('terms')=="yes"?'checked':''}} id="acceptterms">
			<label for="acceptterms">I agree to the <a href="#">terms and the conditions</a>(to be extended)</label>
		</div>
		<button class="btn primary bold">Create An Account</button>
	</form>
</div>
<div class="cf"></div>
@stop