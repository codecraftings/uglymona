@extends('layouts.main')
@section('window-title')
Sign Up | @parent
@stop
@section('top-content')
<div class="cf"></div>
<div class="build-profile-slogan">
	Build Profile- Answer Questions to gain visibility
</div>
<div class="content-title signup">
	New To Uglymona?
</div>
@stop

@section('main-content')
<div class="cf content-title-fix"></div>
<div class="main-form-container">
	<h3 class="form-title success">
	Your email was verified successfully. <a href="{{url('login')}}">Click here</a> to log into your account now!
	</h3>
</div>
<div class="cf"></div>
@stop