@extends('layouts.main')
@section('window-title')
Login | @parent
@stop
@section('top-content')
<div class="cf"></div>
<div class="build-profile-slogan">
	Build Profile- Answer Questions to gain visibility
</div>
<div class="content-title login">
	Login / Registration?
</div>
@stop

@section('main-content')
<div class="cf content-title-fix"></div>
<div class="login-column one fl">
	@if(Session::has('error'))
	<h3>{{Session::get('error')}}</h3>
	@else
	<h3>Already Member? <a href="javascript:void()">Login Now!</a></h3>
	@endif
	<form id="login_form" name="login-form" method="post" action="{{url('login')}}" autocomplete="off">
		{{Form::token()}}
		@if(Session::has('url.intended'))
		<input type="hidden" name="url.intended" value="{{Session::get('url.intended')}}">
		@endif
		<input id="email" type="email" value="{{Session::get('email')}}" name="email" class="input" placeholder="Email Address"> 
		<input type="password" name="password" value="{{Session::get('password')}}" class="input" placeholder="Email Password">
		<button class="btn primary">Login</button>
		<div class="form-extra">
			<label class="fl"><input name="remember_me" value="yes" type="checkbox"> Remember me</label>
			<a href="#" class="fr">Forgot your password?</a>
			<div class="cf"></div>
		</div>
	</form>
</div>
<div class="login-column two fl">
	<h3>Not a member yet? <a href="{{url('signup')}}">Join Now!</a></h3>
	<p>
		Everyday hundreds of people join Uglymona - the leading community for singles. Our easy features, thousands of members, and countless success stories make this the place to meet someone special.
	</p>
	<p class="small">
		Connect with hundreds of thousands of singles around the world
	</p>
	<a href="{{url('signup')}}" class="btn primary">Click to continue</a>
</div>
<div class="cf"></div>
@stop