@extends('layouts.main')
@section('window-title')
Build Your Profile | @parent
@stop
@section('top-content')
<div class="cf"></div>
<div class="build-profile-slogan">
	Build Profile- Answer Questions to gain visibility
</div>
<div class="content-title build">
    Build Your Profile Below
</div>
@stop

@section('main-content')
<div class="cf content-title-fix"></div>
<div class="main-form-container">
    <div id="build_profile_progressbar" class="progress-bar">
        <span style="width:0%;" class="indicator"></span>
        <span style="left:20%" class="status-text">0%</span>
    </div>
    <form autocomplete="off" action="{{url('profile/build')}}" enctype="multipart/form-data" method="post" id="build-profile-form" class="build-step-container">
    {{Form::token()}}
        <div class="build-step-1">
            <div data-label="I'm a" class="select-container input">
                {{Form::select("gender", array(
                    'man' => 'Man',
                    'woman' => 'Woman'
                    ), $current_user->profile->gender, array('class'=>'input'))}}
                </div>
                <div class="select-container input" data-label="Looking for">
                    {{Form::select("looking_for", array(
                        'man' => 'Man',
                        'woman' => 'Woman'
                        ), $current_user->profile->looking_for, array('class'=>'input'))}}
                    </div>
                    <input type="text" name="full_name" value="{{$current_user->full_name}}" placeholder="Full Name" class="input" />
                    <input type="text" name="date_of_birth" value="{{($date = $current_user->profile->date_of_birth)?$date->format('Y-m-d'):''}}" placeholder="Date of birth (yyyy/mm/dd)" class="input" />

                    <button type="button" onclick="builder.goto_step(2);" class="btn primary bold">Next</button>
                </div>
                <div class="build-step-2 hidden">
                    <div class="select-container input" data-label="Age group looking for">
                        {{Form::select("looking_for_age", array(
                            '18-21' =>'18-20',
                            '21-25' =>'21-25',
                            '26-30' =>'26-30',
                            '31-35' =>'31-35'
                            ), $current_user->profile->looking_for_age, array('class'=>'input'))}}
                    </div>
                        <div class="select-container input" data-label="Zodiac sign">
                            {{Form::select("zodiac_sign", array(
                                'Aries' => 'Aries',
                                'Taurus' => 'Taurus',
                                'Hemini' => 'Gemini',
                                'Cancer' => 'Cancer',
                                'Leo' => 'Leo',
                                'Virgo' => 'Virgo',
                                'Libra' => 'Libra',
                                'Scorpio' => 'Scorpio',
                                'Sagittarius' => 'Sagittarius',
                                'Capricorn' => 'Capricorn',
                                'Aquarius' => 'Aquarius',
                                'Pisces' => 'Pisces'
                                ), $current_user->profile->zodiac_sign, array('class'=>'input'))}}
                            </div>
                            <div class="select-container input" data-label="Your relationship status">

                                {{Form::select("relationship_status", array(
                                    'single' => 'Single',
                                    'married' => 'Married',
                                    'divorced' => 'Divorced'
                                    ), $current_user->profile->relationship_status, array('class'=>'input'))}}
                                </div>
                                <div class="select-container input" data-label="Do you want children?">

                                    {{Form::select("want_children", array(
                                        '1' => 'Yes',
                                        '0' => 'No'
                                        ), $current_user->profile->want_children, array('class'=>'input'))}}
                                    </div>
                                    <button type="button" onclick="builder.goto_step(3);" class="btn primary bold">Next</button>
                                </div>
                                <div class="build-step-3 hidden">
                                    <div class="select-container input" data-label="Education">

                                        {{Form::select("education", array(
                                            'high_school' => 'High School',
                                            'graduated' => 'Graduated',
                                            'college' => 'College'
                                            ), $current_user->profile->education, array('class'=>'input'))}}
                                        </div>
                                        <div class="select-container input" data-label="Work">
                                            {{Form::select("work", array(
                                                'engineer' => 'Engineer',
                                                'doctor' => 'Doctor',
                                                'business' => 'Business',
                                                'other' => 'Other'
                                                ), $current_user->profile->work, array('class'=>'input'))}}
                                            </div>
                                            <div class="select-container input" data-label="Personality">
                                                {{Form::select("personality", array(
                                                    'Artist' => 'Artist',
                                                    'Protector' => 'Protector',
                                                    'Idealist' => 'Idealist',
                                                    'Scientist' => 'Scientist',
                                                    'Thinker' => 'Thinker',
                                                    'Doer' => 'Doer',
                                                    'Guardian' => 'Guardian',
                                                    'Performer' => 'Performer',
                                                    'Inspirer' => 'Inspirer'
                                                    ), $current_user->profile->personality, array('class'=>'input'))}}
                                                </div>
                                                <div class="select-container input" data-label="Ethnicity">

                                                    {{Form::select("ethnicity", array(
                                                        'indian' =>'Indian',
                                                        'german' =>'German',
                                                        'asian' =>'Asian'
                                                        ), $current_user->profile->ethnicity, array('class'=>'input'))}}
                                                    </div>
                                                    <button type="button" onclick="builder.goto_step(4);" class="btn primary bold">Next</button>
                                                </div>
                                                <div class="build-step-4 hidden">
                                                    <div class="select-container input" data-label="Build Type">
                                                        {{Form::select("build_type", array(
                                                           'fat' => 'Fat',
                                                           'average' => 'Average',
                                                           'slim' => 'Slim'
                                                           ), $current_user->profile->build_type, array('class'=>'input'))}}
                                                       </div>
                                                       <div class="select-container input" data-label="Hair Color">

                                                        {{Form::select("hair_color", array(
                                                           'black' => 'Black',
                                                           'golden' => 'Golden',
                                                           'red' => 'Red',
                                                           'white' => 'White',
                                                           'brown' => 'Brown'
                                                           ), $current_user->profile->hair_color, array('class'=>'input'))}}
                                                       </div>
                                                       <div class="select-container input" data-label="Eye Color">

                                                        {{Form::select("eye_color", array(
                                                            'black'=>'Black',
                                                            'red'=>'Red',
                                                            'blue'=>'Blue',
                                                            'Brown'=>'Brown'
                                                            ), $current_user->profile->eye_color, array('class'=>'input'))}}
                                                        </div>
                                                        <div class="select-container input" data-label="Do you smoke?">
                                                            {{Form::select("do_smoke", array(
                                                                '1' =>' Yes',
                                                                '0'=>'No'
                                                                ), $current_user->profile->do_smoke, array('class'=>'input'))}}
                                                            </div>
                                                            <button type="button" onclick="builder.goto_step(5);" class="btn primary bold">Next</button>
                                                        </div>
                                                        <div class="build-step-5 hidden">
                                                        <textarea name="about_text" class="input" placeholder="Say as many words about yourself">{{$current_user->profile->about_text}}</textarea>
                                                            <button type="button" onclick="builder.goto_step(6);" class="btn primary bold">Next</button>
                                                        </div>
                                                        <div class="build-step-6 hidden">
                                                            <div class="circle-photo-holder super-form-drop">

                                                            </div>
                                                            <input class="hidden super-form-fileinp" type="file" name="photo"/>
                                                            <div style="text-align:center;" class="form-extra">
                                                                Choose a photo to upload as your profile photo    
                                                            </div>
                                                            <button id="submit-build-profile" type="button" onclick="builder.submit();" class="btn primary bold">Upload and complete profile</button>
                                                        </div>
                                                        <div class="build-step-7 hidden">
                                                            <span class="build-completed-message">Congrats!! your profile is completed Now! Lets spin the wheel and find your soulmate! Wait while we redirect you to profile wheel!</span>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="cf"></div>
                                                @stop
