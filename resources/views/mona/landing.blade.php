@extends('layouts.landing')
@section('window-title')
	Welcome to @parent
@stop
@section('js-loading')
@parent
@stop
@section('top-content')
	<div class="cf"></div>
	<div class="landing-welcome-text">
	    Welcome to UGLYMONA - Where who you are has never been so hot!
	</div>
	<div class="landing-page-divider">
	    <a href="javascript:void()" class="landing-divider-arrow"></a>
	    @if(!isset($current_user))
	    <a href="{{url('signup')}}" class="landing-register-btn"></a>
	    @endif
	</div>
@stop

@section('bottom-content')
	<div class="cf"></div>
	<div class="register-slogan-container">
	    Register for FREE and start searching for your soulmate NOW!
	</div>
	<div class="landing-slider-container">
	    <ul class="landing-slider">
	        <li class="landing-slider-item">
	            <img src="{{url('images/profile-wheel.png')}}">
	            <h2>PROFILE WHEEL</h2>
	            <a class="see-more-btn" href="{{url('profile-wheel')}}">Click to see more +</a>
	        </li>
	        <li class="landing-slider-item">
	            <img src="{{url('images/quest-bar.png')}}">
	            <h2>QUEST BAR</h2>
	            <a class="see-more-btn" href="{{url('quest-bar')}}">Click to see more +</a>
	        </li>
	        <li style="margin-right:0px;" class="landing-slider-item">
	            <img src="{{url('images/video-room.png')}}">
	            <h2>VIDEO MINGLE</h2>
	            <a class="see-more-btn" href="{{url('videos')}}">Click to see more +</a>
	        </li>
	        <li class="landing-slider-item">
	            <img src="{{url('images/mona-room-thumb.png')}}">
	            <h2>LETTER ROOM</h2>
	            <a class="see-more-btn" href="{{url('mona-room')}}">Click to see more +</a>
	        </li>
	        <li class="landing-slider-item">
	            <img src="{{url('images/blog-thumb.png')}}">
	            <h2>LIFE ARTICLES</h2>
	            <a class="see-more-btn" href="{{url('quest-bar')}}">Click to see more +</a>
	        </li>
	    </ul>
	    <a href="javascript:void(0);" class="slider-control-left"></a>
	    <a href="javascript:void(0);" class="slider-control-right"></a>
	    <div class="cf"></div>
	</div>
@stop