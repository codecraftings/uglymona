@extends('layouts.main')

@section('window-title')
    {{$video->title}} | Video Room | @parent
@stop
@section('js-loading')
    @parent
    {{HTML::script('js/build/plugins.min.js')}}
    <script>
        videojs.options.flash.swf = "{{url('js/video-js.swf')}}";
    </script>
    <script type="text/javascript">
        $(function (){
            $('.list-view-container').css("height", 218 * 2);
            _.defer(function (){
                $('.list-view-container').mCustomScrollbar({
                    axis             : "y",
                    scrollbarPosition: "outside",
                    autoHideScrollbar: true,
                    snapAmount       : 20,
                    snapOffset       : 0,
                    scrollInertia    : 500,
                    mouseWheel       : {
                        //normalizeDelta: true,
                        //scrollAmount: 218,
                        deltaFactor: 218
                    }
                });
            });

        })
    </script>
@stop
@section('top-content')
    <div class="cf"></div>
    <div class="single-video-container">
        <div class="user-details fl">
            <div class="user-thumb single-video-page">
                <div class="pp fl">
                    <img src="{{$video->user->profile->profile_pic}}"/>
                </div>
                <div class="info fl">
                    <p class="line1"><i class="sprite heart-purple"></i> &nbsp;{{$video->user->profile->views_count}}
                        Views</p>

                    <p class="line2">{{$video->user->present()->name(15)}}, {{$video->user->profile->present()->age()}}
                        y.o.</p>

                    <p class="line3">{{$video->user->city}}, {{$video->user->state}}</p>
                </div>
                <div class="cf"></div>
            </div>
            <ul class="profile-nav single-video-page">
                <li><a href=#><span class="sprite profile"></span> View Profile</a>
                </li>
                <li><a href=#><span class="sprite message"></span> Send A Message</a>
                </li>
                <li><a href=#><span class="sprite user-groups"></span> Add To Connection</a>
                </li>
            </ul>
        </div>
        <div style="" class="video-details fr">
            <video id="um-user-video" class="video-js vjs-default-skin vjs-big-play-centered" controls
                   preload="auto" width="100%" height="100%" poster="{{$video->thumb_url}}"
                   data-setup='{}'>
                <source src="{{$video->video_url}}" type='video/mp4'>
                <p class="vjs-no-js">
                    To view this video please enable JavaScript, and consider upgrading to a web browser
                    that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                </p>
            </video>
        </div>
        <div class="cf"></div>
    </div>
    <div class="content-title videoroom">
        <a class="fl" href="{{url('videos')}}">Browse Videos</a><a class="fr" href="{{url('videos/add')}}">Add Your
            Video</a>
    </div>
@stop
@section('separator')
    <div class="cf content-title-fix full"></div>
@stop
@section('main-content')
    <div class="listing-tag">for more result scroll down your mouse</div>
    <div class="list-view-container singlevideopage">
        <ul class="list-view">
            @foreach($videos as $video)
                <li class="list-item">
                    <div class="video-thumb">
                        <div class="img-container">
                            <img src="{{$video->thumb_url}}"/>
                            <a href="{{$video->permalink}}" class="sprite play"></a>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="cf"></div>
@stop