@extends('layouts.main')

@section('window-title')
    Video Room | @parent
@stop
@section('js-loading')
    @parent
    <script type="text/javascript">
        jQuery(document).ready(function ($){
            $('.list-view-container').css("height", 218 * 2);
            _.defer(function (){
                $('.list-view-container').mCustomScrollbar({
                                                               axis: "y",
                                                               scrollbarPosition: "outside",
                                                               autoHideScrollbar: true,
                                                               snapAmount: 20,
                                                               snapOffset: 0,
                                                               scrollInertia: 500,
                                                               mouseWheel: {
                                                                   //normalizeDelta: true,
                                                                   //scrollAmount: 218,
                                                                   deltaFactor: 218
                                                               }
                                                           });
            });

        });
    </script>
@stop

@section('top-content')
    <div class="cf"></div>
    <div class="video-room-slogan">
        Use Uglymona video room to make a brief statement about yourself
    </div>
    <div class="content-title videoroom">
        <a class="fl active">Browse Videos</a><a class="fr" href="{{url('videos/add')}}">Add Your Video</a>
    </div>
@stop
@section('separator')
    <div class="cf content-title-fix full"></div>
@stop
@section('main-content')
    <a href="{{url('videos/add')}}" class="add-video-button btn purple fl">Add Your Video</a>
    <div class="cf"></div>
    <div class="listing-tag">for more result scroll down your mouse</div>
    <div class="list-view-container videoroom">
        <ul class="list-view">
            @foreach($videos as $video)
                <li class="list-item">
                    <div class="video-thumb">
                        <div class="img-container">
                            <img src="{{$video->thumb_url}}"/>
                            <a href="{{$video->permalink}}" class="sprite play"></a>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="cf"></div>
@stop