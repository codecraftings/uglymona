@extends('layouts.main')

@section('window-title')
Video Room | @parent
@stop
@section('top-content')
<div class="cf"></div>
<div class="video-room-slogan">
    Use Uglymona video room to make a brief statement about yourself
</div>
<div class="content-title videoroom">
    <a class="fl active" href="#">Add Your Video</a><a class="fr">Browse Videos</a>
</div>
@stop
@section('separator')
<div class="cf content-title-fix full"></div>
@stop
@section('main-content')
<div class="main-form-container videoroom">
    
    <form method="post" id="video_upload_form" autocomplete="off" action="{{url('videos/add')}}">
    <div id="" class="progress-bar hidden">
        <span style="width:0%;" class="indicator"></span>
        <span style="left:20%" class="status-text">0%</span>
    </div>
    @if(Session::has('error'))
    <div class="videoroombigtext form-notice error">
        {{Session::get('error')}}
    </div>
    @else
    <div class="videoroombigtext form-notice info">
        Upload your video! It's so easy.
    </div>
    @endif
        <div class="vd-col-1 fl">
            {{Form::token()}}
            <input type="text" name="title" class="input" value="{{Session::get('title')}}" placeholder="Video Title*">
            <input type="text" name="description" class="input" value="{{Session::get('description')}}" placeholder="Video Description">
            <div class="select-container input" data-label="Privacy Level*">
                {{Form::select("privacy",array('anyone' => "Anyone", "users"=>"Only registered users","friends"=>"Only friends"), Session::get('privacy'))}}
            </div>
        </div>
        <div class="vd-col-2 fl">

        </div>
        <div class="vd-col-3 fl">
            <input type="hidden" name="file_id" value="{{Session::get('file_id')}}"/>
            <input type="hidden" name="file_name" value="{{Session::get('file_name')}}"/>
            <input type="file" class="input file-browser" accept="video/mp4,video/webm"/>
            <div class="select-container input" data-label="Allow comments?*">
                {{Form::select("allow_comments",array("yes"=>"Yes","no"=>"No"), Session::get('allow_comments'))}}
            </div>
            <button type="button" class="submit-button btn purple">Add Your Video</button>
        </div>
    </form>
    <div style="margin:" class="mona-notice fr">
        Notice: Your videos/photos will be stored & shared on our servers,
        and can be removed by Uglymona, Inc.
        Customer Support Representatives

    </div>
</div>
<div class="cf"></div>
@stop