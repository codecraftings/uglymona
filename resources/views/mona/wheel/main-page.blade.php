@extends('layouts.wheel')
@section('window-title')
    Spin The Wheel | @parent
@stop
@section('js-loading')
    @parent
    {{HTML::style('http://fonts.googleapis.com/css?family=Anonymous+Pro')}}
@stop
@section('top-content')
    <div class="wheel-page-title">
    </div>
@stop

@section('main-content')
    <div class="profile-wheel-container wheel-view">
        <div class="wheel-user-pic-container">
            <div class="pic-holder hidden"><img id="wheel-user-pic-src-holder" src="{{url('images/avater.png')}}"/>
                <a href="javascript:void(0)" class="wheel-cross-btn"></a>
            </div>
            <span class="angled-arrow hidden"></span>
        </div>
        <div class="wheel-bottom-layer">
            <div class="wheel-menu-items-wrapper">
            <div id="wheel-menu-items">FLIRT | FRIENDSHIP | ADVENTURE | MARRIAGE | CRAZY LOVE | CHAT | TRUE SOULMATE | LOVE | ROMANCE | DATTING | VIDEO CHATTING | SINGLE | </div>
            </div>
        </div>
        <div class="wheel-top-layer">
            <div id="wheel-top-layer-title"></div>
            <div class="hidden2" id="wheel-most-top-layer">
                <!--
                <a href="javascript:void(0)" id="wheel-start-spin-btn" class="wheel-icon spin btn"></a>
                -->
                <a href="javascript:void(0)" id="wheel-start-spin-btn" class="wheel-start-spin-text">SPIN</a>
                <div class="wheel-matching-text hidden">
                    <small>match</small>
                    <span class="count">98%</span></div>
                <a href="javascript:void(0)" class="wheel-icon tick disabled"></a>
                <a href="javascript:void(0)" class="wheel-icon cross disabled"></a>
            </div>
            <div class="hidden" id="wheel-user-details-layer">
                <div class="wheel-profile-data">
                    <h5>Profile</h5>
                    <h6>Jack Jhonson</h6>
                    <ul>
                        <li>Independent constructor</li>
                        <li>Higher education</li>
                        <li>Wants marriage</li>
                        <li>Divorced</li>
                        <li>Non-smoker</li>
                    </ul>
                </div>
                <a href="#" class="wheel-button-msg sprite notification"></a>
                <a href="#" class="wheel-button-photos sprite camera2"></a>
            </div>
        </div>
    </div>
    <div class="cf"></div>
        <div class="wheel-nav-buttons">
            <a href="javascript:void(0)" class="wheel-nav-button prev fl"><span class="sprite left-arrow"></span>
                Previous
                Profile</a>
            <a href="javascript:void(0)" class="wheel-nav-button next fr">Next Profile <span
                        class="sprite right-arrow"></span></a>
        </div>
    <div class="cf"></div>
@stop
