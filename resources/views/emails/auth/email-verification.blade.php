@extends('emails.layout')
@section('email-body')
<p>Hi {{$user->name}}</p>
<p>Thank you for registering on Uglymona. Please confirm your email by clicking on the below link</p>
<table>
	<tr>
		<td class="padding">
			<p><a href="{{api_url('verify/email?token='.$user->email_status.'&email='.$user->email)}}" class="btn-primary">Confirm Your Email</a></p>
		</td>
	</tr>
</table>
<p>If you are unaware about this registration, please ignore it. We won't disturb you again.</p>
<p>Thanks and Regards</p>
<p><a href="{{web_url('/')}}">Uglymona Support Team</a>
@stop