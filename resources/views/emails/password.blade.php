@extends('emails.layout')
@section('email-body')
    <p>Hi {{$user->name}}</p>
    <p>You have requested to reset your password on Uglymona. Please click following link to reset your password</p>
    <table>
        <tr>
            <td class="padding">
                <p><a href="{{web_url('login?reset_token='.$token)}}" class="btn-primary">Confirm Your Email</a></p>
            </td>
        </tr>
    </table>
    <p>This link will expire in {{ Config::get('auth.password.expire', 60) }} minutes.</p>
    <p>If you are unaware about this request. Just ignore it. It wont change your password!</p>
    <p>Thanks and Regards</p>
    <p><a href="{{web_url('/')}}">Uglymona Support Team</a>
@stop