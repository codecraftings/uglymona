<!DOCTYPE html>
<html ng-app="app">
<head>
	@section('html-head')
	@include('common.html-head')
	@show
</head>
<body mona-freezable="bg">
	<div @yield("extra-attr") id="main-wrapper">
		<header class="row header-section common">
			@include('common.nav-bar')
			<div class="row top-content">
				<div class="container">
					@yield('top-content')
				</div>
			</div>
		</header>
		<div class="row main-content-section">
		@yield('separator')
			<div class="container">
				@yield('main-content')
			</div>
		</div>
		@include('common.footer')
	</div>
</body>