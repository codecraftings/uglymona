<!DOCTYPE html>
<html>
<head>
	@section('html-head')
	@include('common.html-head')
	@show
</head>
<body>
	<div id="main-wrapper">
		<header class="row header-section landing">
			@include('common.nav-bar')
			<div class="row top-content">
				<div class="container">
					@yield('top-content')
				</div>
			</div>
		</header>
		<div class="row content-section-landing">
			<div class="container">
				@yield('bottom-content')
			</div>
		</div>
		@include('common.footer')
	</div>
</body>