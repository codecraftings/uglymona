<div mona-navbar class="row nav-bar">
    <div class="container">
        <h1 class="top-logo-container fl">
            <a href="{{url('/')}}" class="top-logo">Uglymona</a>
        </h1>
        <ul ng-cloak class="top-main-menu fl">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>/</li>
            <li id="rooms-menu" class="has-submenu">
                <a class="submenu-toggle" ng-click="roomsMenuVisible=!roomsMenuVisible" href="javascript:void(0);">Mona's Rooms</a>
                <ul ng-attr-class="@{{roomsMenuVisible?'submenu magictime boingInUp visible':'submenu magictime boingOutDown'}}">
                    <span class="up-arrow"></span>
                    <li>
                        <a href="{{url('profile-wheel')}}">Profile Wheel</a>
                    </li>
                    <li>
                        <a href="{{url('quest-bar')}}">Quest Bar</a>
                    </li>
                    <li>
                        <a href="{{url('videos')}}">Video Mingle</a>
                    </li>
                    <li>
                        <a href="{{url('letter-room')}}">Letter Room</a>
                    </li>
                </ul>
            </li>
            <li>/</li>
            <li>
                <a href="{{url('blogs')}}">Life Articles</a>
            </li>
        </ul>
        <div ng-cloak id="mobile-menu-container" style="margin:1px 0px 0px 5px;position:relative;overflow: visible;" class="fr">
            <a ng-click="mobileMenuVisible=!mobileMenuVisible" href="javascript:void(0);" class="menu-icon mobile-menu-toggle"></a>
            <a href="#" class="sprite sound-on"></a>
            <ul ng-attr-class="@{{mobileMenuVisible?'mobile-menu submenu magictime boingInUp visible':'mobile-menu submenu magictime boingOutDown'}}">
                <span class="up-arrow"></span>

                <li ng-hide="user.access_token">
                    <a href="{{url('login')}}">Login</a>
                </li>
                <li ng-hide="user.access_token">
                    <a href="{{url('signup')}}">Signup</a>
                </li>
                <li>
                    <a href="{{url('profile-wheel')}}">Profile Wheel</a>
                </li>
                <li>
                    <a href="{{url('quest-bar')}}">Quest Bar</a>
                </li>
                <li>
                    <a href="{{url('videos')}}">Video Mingle</a>
                </li>
                <li>
                    <a href="{{url('letter-room')}}">Letter Room</a>
                </li>
                <li ng-show="user.access_token">
                    <a href="profile/build">Edit Profile</a>
                </li>
                <li ng-show="user.access_token">
                    <a ng-click="logout()">Logout</a>
                </li>
            </ul>
        </div>
        <div ng-cloak ng-show="user.access_token" class="user-header fr" id="current-user-header">
            <div ng-class="{'active':userMenuVisible}" class="toggle-btn" ng-click="userMenuVisible=!userMenuVisible">
                Welcome, @{{user.display_name|limit:15}}
                <span class="user-header-icon">
                <img ng-src="@{{user.profile.profile_pic}}">
                <span class="count">0</span>
            </span>
            </div>
            <ul ng-class="{'visible':userMenuVisible}" class="dropdown-menu">
                <li>
                    <a href="profile/upgrade">Upgrade Membership</a>
                </li>
                <li>
                    <a ng-href="users/@{{user.id}}">View Profile</a>
                </li>
                <li>
                    <a href="profile/build">Edit Profile</a>
                </li>
                <li>
                    <a ng-click="showInbox()" href="#">Messages(0)</a>
                </li>
                <li>
                    <a href="#">Notifications(0)</a>
                </li>
                <li>
                    <a ng-click="logout()">Logout</a>
                </li>
            </ul>
        </div>
        <ul ng-cloak no-anim ng-hide="user.access_token" class="top-main-menu fr">
            <li>
                <a href="{{url('login')}}">Login</a>
            </li>
            <li>/</li>
            <li>
                <a href="{{url('signup')}}">Signup</a>
            </li>
        </ul>
        <div class="cf"></div>
    </div>
</div>