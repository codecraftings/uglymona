<div class="row nav-bar">
    <div class="container">
        <h1 class="top-logo-container fl">
            <a href="{{url('/')}}" class="top-logo">Uglymona</a>
        </h1>
        <ul class="top-main-menu fl">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>/</li>
            <li class="has-submenu">
                <a class="submenu-toggle" href="javascript:void();">Mona's Rooms</a>
                <ul style="" class="submenu">
                    <span class="up-arrow"></span>
                    <li>
                        <a href="{{url('profile-wheel')}}">Profile Wheel</a>
                    </li>
                    <li>
                        <a href="{{url('quest-bar')}}">Quest Bar</a>
                    </li>
                    <li>
                        <a href="{{url('videos')}}">Video Mingle</a>
                    </li>
                    <li>
                        <a href="{{url('mona-room')}}">Letter Room</a>
                    </li>
                </ul>
            </li>
            <li>/</li>
            <li>
                <a href="{{url('blogs')}}">Blog</a>
            </li>
        </ul>
        <div style="margin:1px 0px 0px 5px;position:relative;overflow: visible;" class="fr">
            <a href="#" class="menu-icon mobile-menu-toggle"></a>
            <a href="#" class="sprite sound-on"></a>
            <ul class="mobile-menu submenu">
                <span class="up-arrow"></span>

                @if(!isset($current_user))
                    <li>
                        <a href="{{url('login')}}">Login</a>
                    </li>
                    <li>
                        <a href="{{url('signup')}}">Signup</a>
                    </li>
                @endif
                <li>
                    <a href="{{url('profile-wheel')}}">Profile Wheel</a>
                </li>
                <li>
                    <a href="{{url('quest-bar')}}">Quest Bar</a>
                </li>
                <li>
                    <a href="{{url('videos')}}">Video Mingle</a>
                </li>
                <li>
                    <a href="{{url('mona-room')}}">Letter Room</a>
                </li>
                @if(isset($current_user))
                    <li>
                        <a href="{{$current_user->profile_url}}">Edit Profile</a>
                    </li>
                    <li>
                        <a href="{{url('logout')}}">Logout</a>
                    </li>
                @endif
            </ul>
        </div>
        @if(isset($current_user))
            <div class="user-header fr">
                <div class="toggle-btn" onclick="app.toggle_user_header_menu()">
                    Welcome, {{$current_user->present()->name()}}
                    <span class="user-header-icon">
                <img src="{{$current_user->profile->profile_pic}}">
                <span class="count">0</span>
            </span>
                </div>
                <ul style=";" class="dropdown-menu">
                    <li>
                        <a href="#">Edit Profile</a>
                    </li>
                    <li>
                        <a href="#">Messages(0)</a>
                    </li>
                    <li>
                        <a href="#">Notifications(0)</a>
                    </li>
                    <li>
                        <a href="{{url('logout')}}">Logout</a>
                    </li>
                </ul>
            </div>
        @else
            <ul class="top-main-menu fr">
                <li>
                    <a href="{{url('login')}}">Login</a>
                </li>
                <li>/</li>
                <li>
                    <a href="{{url('signup')}}">Signup</a>
                </li>
            </ul>
        @endif
        <div class="cf"></div>
    </div>
</div>