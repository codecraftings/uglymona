(function (){
    angular.module('app.video').controller('VideoRoomController', VideoRoomController);

    VideoRoomController.$inject = ['latestVideos','singleVideo'];
    function VideoRoomController(latestVideos,singleVideo){
        var vm = this;
        vm.videos = [];
        vm.video;
        init();

        function init(){
            vm.videos = latestVideos;
            if(angular.isDefined(singleVideo)){
                vm.video = singleVideo;
            }
        }
    }
})();