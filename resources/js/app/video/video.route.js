(function (){
   angular.module('app.video')
      .config(RouteConfigProvider);

   RouteConfigProvider.$inject = [ '$routeProvider', 'authResolver' ];
   function RouteConfigProvider($routeProvider, authResolver){
      $routeProvider
         .when(
         '/videos',
         {
            title       : "Video Mingle",
            templateUrl : "templates/video-room.html",
            controller  : "VideoRoomController",
            controllerAs: "vm",
            accessLevel : "user",
            permissions: ['video_room'],
            resolve     : {
               permissions: authResolver,
               latestVideos: resolveLatestVideos,
               singleVideo: function(){}
            }
         }
      )
         .when(
         '/videos/add',
         {
            title       : "Add Your Video",
            templateUrl : "templates/add-video.html",
            controller  : "AddVideoController",
            controllerAs: "vm",
            accessLevel : 'user',
            permissions: ['video_room','add_video'],
            resolve     : {
               permissions: authResolver
            }
         }
      )
         .when(
         '/videos/:id/:title',
         {
            title: 'Video Mingle',
            templateUrl: "templates/single-video.html",
            controller: 'VideoRoomController',
            controllerAs: "vm",
            accessLevel: "user",
            permissions: ['video_room'],
            resolve: {
               permissions: authResolver,
               latestVideos: resolveLatestVideos,
               singleVideo: resolveVideoData
            }
         }
      );
   }
   resolveVideoData.$inject = ['videos', '$route'];
   function resolveVideoData(videos, $route){
      var video = videos.get($route.current.params.id);
      return video.$promise;
   }
   resolveLatestVideos.$inject = [ 'videos' ];
   function resolveLatestVideos(videos){
      var _list = videos.query();
      return _list.$promise;
   }
})();