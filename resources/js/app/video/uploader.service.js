(function (){
   angular.module('app.video').service('videoUploader', VideoUploader);
   
   VideoUploader.$inject = [ '$q', 'currentUser', '$log', 'urls', '$http' ];
   function VideoUploader($q, currentUser, $log, urls, $http){
      this.checkFileValidity = checkFileValidity;
      this.uploadFile = uploadFile;
      function uploadFile(filedata){
         var df = $q.defer();
         df.notify(0);
         var parts = getParts(filedata);
         var fileid = getFileId(filedata);
         uploadParts(parts, fileid)
            .then(function (){
               getAssembledFilesName(fileid, parts.length, filedata.size)
                  .then(df.resolve).catch(df.reject)
                  .finally(null, function (progress){
                     df.notify(map(progress, 0, 100, 80, 100));
                  });
            }).catch(df.reject)
            .finally(null, function (progress){
               df.notify(map(progress, 0, 100, 0, 80));
            });
         return df.promise;
      }

      function map(val, s1, s2, d1, d2){
         var dd = (val - s1) / (s1 - s2) * (d1 - d2) + d1;
         return dd;
      }

      function extraLarzeFile(filedata){
         var filesize = Math.floor(filedata.size / (1024 * 1024));
         if ( filesize > 100 ) {
            return true;
         }
         return false;
      }

      function getAssembledFilesName(fileid, total_chunks, size){
         var df = $q.defer();
         $http.post(urls.api("videos/upload/assemble"),
            {
               file_id: fileid,
               total_chunks: total_chunks,
               expected_filesize: size
            })
            .then(function (response){
               df.notify(100);
               df.resolve(response.filePath);
            }).catch(function (response){
               df.reject(response.data.error.message);
            });
         return df.promise;
      }

      function uploadParts(fileparts, fileid){
         var df = $q.defer();
         var totalChunks = fileparts.length;
         var current_chunk = 0;
         next();
         return df.promise;
         function next(){
            if ( current_chunk >= totalChunks ) {
               df.notify(100);
               df.resolve();
               return;
            }
            df.notify(current_chunk * (100 / totalChunks));
            testChunk(current_chunk, fileid).then(send)
               .catch(function (){
                  current_chunk += 1;
                  next();
               });
            function send(){
               sendChunk(fileparts[ current_chunk ], current_chunk, totalChunks, fileid)
                  .then(function (){
                     current_chunk += 1;
                     next();
                  })
                  .catch(function (response){
                     switch (response.data.status) {
                        case 405:
                           df.reject();
                           break;
                        case 402:
                           next();
                           break;
                        case 410:
                           current_chunk += 1;
                           next();
                           break;
                     }
                  });
            }
         }
      }

      function testChunk(chunkNumber, fileId){
         var dd = {};
         dd.chunk_number = chunkNumber;
         dd.file_id = fileId;
         return $http.post(urls.api('videos/test/chunk'), dd);
      }

      function sendChunk(chunk, chunkNumber, totalChunk, fileId){
         var data = new FormData();
         data.append("chunk_number", chunkNumber);
         data.append("total_chunk", totalChunk);
         data.append("file_id", fileId);
         data.append("filepart", chunk);
         return $http.post(urls.api("videos/upload/chunk"), data, {
            headers: {
               transformRequest: angular.identity,
               "Content-Type"  : undefined
            }
         });
      }

      function getFileId(file){
         var fileId = "";
         if ( currentUser.id ) {
            fileId += currentUser.id + "-";
         } else {
            fileId += Math.floor(Math.random() * 100) + "-";
         }
         if ( file.size ) {
            fileId += Math.floor(file.size / 1024) + "-";
         }
         var relativePath = file.webkitRelativePath || file.fileName || file.name;
         if ( relativePath ) {
            fileId += relativePath.replace(/[^0-9a-zA-Z_-]/img, '').substr(0, 30);
         }
         $log.info(fileId);
         return fileId;
      }

      function getParts(filedata){
         var chunkSize = 1024 * 1024;
         var parts = [];
         var totalChunks = Math.ceil(filedata.size / chunkSize);
         for ( var i = 0; i < totalChunks; i ++ ) {
            var blob = slice(filedata, i * chunkSize, (i + 1) * chunkSize);
            parts.push(blob);
         }
         $log.info(parts);
         return parts;
      }

      function slice(file, start, end){
         if ( typeof file.webkitSlice === "function" ) {
            return file.webkitSlice(start, end, file.type);
         } else if ( typeof file.mozSlice === "function" ) {
            return file.mozSlice(start, end, file.type);
         } else {
            return file.slice(start, end, file.type);
         }
      }

      function checkFileValidity(filedata){
         var df = $q.defer();
         if ( validMimeType(filedata) ) {
            if ( extraLarzeFile(filedata) ) {
               df.reject("File is too larze. Please select a video file smaller than 100MB");
            } else {
               df.resolve();
            }
         } else {
            df.reject("File format not valid");
         }
         return df.promise;
      }

      function validMimeType(filedata){
         var mime = getMime(filedata);
         var valid_mimes = [ 'video/mp4', 'video/webm' ];
         if ( valid_mimes.indexOf(mime) > - 1 ) {
            return mime;
         }
         return false;
      }

      function getMime(filedata){
         return filedata.type;
      }
   }
})();