(function (){
   angular.module('app.video').factory('Video', VideoFactory);
   
   VideoFactory.$inject = [ 'Model', 'User', 'Photo' ];
   function VideoFactory(Model, User, Photo){
      function Video(data){
         var _this = new Model;
         _this.setFields({
            user             : {},
            privacy          : "",
            status           : "",
            title            : "",
            description      : "",
            source_path      : "",
            thumb            : {},
            source           : {},
            allow_comments   : false,
            available_formats: []
         });
         _this.onDataUpdated(function (){
            _this.user = new User(_this.user);
            _this.thumb = new Photo(_this.thumb);
         });
         _this.setFieldsData(data);
         _this.getSource = getSource;
         return _this;
         function getSource(format){
            return _this.source[ format ];
         }
      }
      
      return Video;
   }
})();