(function (){
    angular.module('app.video').service('videos', VideoService);

    VideoService.$inject = ['Video', 'Repo'];
    function VideoService(Video, Repo){
        var _this = new Repo("videos", Video);

        return _this;
    }
})();