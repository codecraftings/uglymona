(function (){
   angular.module('app.video').controller('AddVideoController', AddVideoController);
   
   AddVideoController.$inject = [ 'Video', 'currentUser', '$log', '$scope', 'videoUploader', '$window' ];
   function AddVideoController(Video, currentUser, $log, $scope, videoUploader, $window){
      var vm = this;
      vm.video = new Video;
      vm.video.user_id = currentUser.id;
      vm.error = "";
      vm.success = "";
      vm.selectedFile = "";
      vm.submitDisabled = true;
      vm.privacyOptions = [
         {
            label: "Anyone",
            value: "anyone"
         },
         {
            label: "Registered Users",
            value: "users"
         },
         {
            label: "Only Connected",
            value: "friends"
         }
      ];
      vm.yesNoOptions = [
         {
            label: "Yes",
            value: 1
         },
         {
            label: "No",
            value: 0
         }
      ];
      vm.submit = submit;
      init();

      function init(){
         $scope.$watch("vm.selectedFile", function (){
            if ( vm.selectedFile ) {
               $log.info(vm.selectedFile.type);
               videoUploader.checkFileValidity(vm.selectedFile)
                  .then(function (){
                  vm.submitDisabled = false;
                  vm.error = "";
               }).catch(function (error){
                  vm.error = error;
                  vm.submitDisabled = true;
               });
            } else {
               vm.submitDisabled = true;
            }
         });
      }
      function submit(){
         videoUploader.uploadFile(vm.selectedFile);
      }
   }
})();