(function (){
   angular.module('app')
      .directive('monaPageLoader', monaPageLoader);

   monaPageLoader.$inject = [ '$rootScope', 'event' ];
   function monaPageLoader($rootScope, event){
      var dir = {
         restrict: 'A',
         link    : link
      };
      return dir;
      function link(scope, elm, attr){
         elm.addClass('mona-page-loader hidden');
         $rootScope.$on("$routeChangeStart", show);
         $rootScope.$on("$routeChangeSuccess", hide);
         $rootScope.$on("$routeChangeError", hide);

         $rootScope.$on(event.showLoader, show);
         $rootScope.$on(event.hideLoader, hide);
         var pendingRequests = 0;

         function show(){
            pendingRequests ++;
            if ( pendingRequests <= 1 ) {
               $rootScope.$broadcast("freeze:bg");
               elm.removeClass('hidden');
            }
         }

         function hide(force){
            force = force || false;
            if ( ! force ) {
               pendingRequests --;
               if ( pendingRequests > 0 ) {
                  return;
               }
            }
            pendingRequests = 0;
            elm.addClass('hidden');
            $rootScope.$broadcast("unfreeze:bg", force);
         }
      }
   }
})();