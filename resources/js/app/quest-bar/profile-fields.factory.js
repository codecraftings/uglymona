(function (){
    angular.module('app.quest-bar').factory('profileFields', profileFieldsFactory);

    profileFieldsFactory.$inject = [];
    function profileFieldsFactory(){
        return [
        {
            key: "gender",
            label: "Gender",
            options: [
            {
                label: "Man",
                value: "man"
            },
            {
                label: "Woman",
                value: "woman"
            }
            ]
        },
        {
            key: "looking_for",
            label: "Looking for",
            options: [
            {
                label: "Man",
                value: "man"
            },
            {
                label: "Woman",
                value: "woman"
            }
            ]
        },
        {
            key:"looking_for_age",
            label:"Age group looking for",
            options: [
            {
                label: "18-21",
                value: "18-21"
            },
            {
                label: "21-25",
                value: "21-25"
            },
            {
                label: "26-30",
                value: "26-30"
            },
            {
                label: "31-35",
                value: "31-35"
            }
            ]
        },
        {
            key:"zodiac_sign",
            label:"Zodiac sign",
            options: [
            {
                label: "Aries",
                value: "aries"
            },
            {
                label: "Taurus",
                value: "taurus"
            },
            {
                label: "Hemini",
                value: "hemini"
            },
            {
                label: "Cancer",
                value: "cancer"
            },
            {
                label: "Leo",
                value: "leo"
            },
            {
                label: "Virgo",
                value: "virgo"
            },
            {
                label: "Libra",
                value: "libra"
            },
            {
                label: "Scorpio",
                value: "scorpio"
            },
            {
                label: "Sagittarius",
                value: "sagittarius"
            },
            {
                label: "Capricorn",
                value: "capricorn"
            },
            {
                label: "Aquarius",
                value: "Aquarius"
            },
            {
                label: "Pisces",
                value: "pisces"
            }
            ]
        },
        {
            key: "relationship_status",
            label: "Relationship Status",
            options: [
            {
                label: "Single",
                value: "single"
            },
            {
                label: "Divorced",
                value: "divorced"
            }
            ]
        },
        {
            key:"want_children",
            label:"Want children?",
            options: [
            {
                label: "Yes",
                value: "1"
            },
            {
                label: "No",
                value: "0"
            }
            ]
        },
        {
            key: "education",
            label: "Education",
            options: [
            {
                label: "High School",
                value: "high school",
            },
            {
                label: "College",
                value: "college"
            },
            {
                label: "Graduated",
                value: "graduated"
            },
            {
                label: "PHD",
                value: "phd"
            }
            ]
        },
        {
            key: "work",
            label: "Profession",
            options: [
            {
                label: "Engineer",
                value: "engineer"
            },
            {
                label: "Doctor",
                value: "doctor"
            },
            {
                label: "Business",
                value: "business"
            },
            {
                label: "Creative Writer",
                value: "creative_writer"
            },
            {
                label: "Journalist",
                value: "journalist"
            },
            {
                label: "Corporate Job",
                value: "corporate_job"
            },
            {
                label: "Other",
                value: "other"
            }
            ]
        },
        {
            key:"personality",
            label:"Personality",
            options: [
            {
                label: 'Artist',
                value: 'Artist'
            },
            {
                label: 'Protector',
                value: 'Protector'
            },
            {
                label: 'Idealist',
                value: 'Idealist'
            },
            {
                label: 'Scientist',
                value: 'Scientist'
            },
            {
                label: 'Thinker',
                value: 'Thinker'
            },
            {
                label: 'Doer',
                value: 'Doer'
            },
            {
                label: 'Guardian',
                value: 'Guardian'
            },
            {
                label: 'Performer',
                value: 'Performer'
            },
            {
                label: 'Inspirer',
                value: 'Inspirer'
            }
            ]
        },
        {
            key:"ethnicity",
            label:"Ethnicity",
            options: [
            {
                value: 'indian',
                label: 'Indian'
            },
            {
                value: 'german',
                label: 'German'
            },
            {
                value: 'asian',
                label: 'Asian'
            }
            ]
        },
        {
            key:"build_type",
            label:"Build Type",
            options: [
            {
                label: "Fat",
                value: "fat"
            },
            {
                label: "Average",
                value: "average"
            },
            {
                label: "Slim",
                value: "slim"
            }
            ]
        },
        {
            key:"hair_color",
            label:"Hair Color",
            options: [
            {
                value: 'black',
                label: 'Black'
            },
            {
                value: 'golden',
                label: 'Golden'
            },
            {
                value: 'red',
                label: 'Red'
            },
            {
                value: 'white',
                label: 'White'
            },
            {
                value: 'brown',
                label: 'Brown'
            }
            ]
        },
        {
            key:"eye_color",
            label:"Eye Color",
            options: [
            {
               value: 'black',
               label: 'Black'
           },
           {
               value: 'red',
               label: 'Red'
           },
           {
               value: 'blue',
               label: 'Blue'
           },
           {
               value: 'Brown',
               label: 'Brown'
           }
           ]
       },
       {
            key: "do_smoke",
            label: "Do you smoke?",
            options: [
            {
                value: "1",
                label: "Yes"
            },
            {
                value: "0",
                label: "No"
            }
            ]
       }
       ];
   }
})();