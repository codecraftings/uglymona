(function (){
   angular.module('app.quest-bar').config(RouteConfigProvider);

   RouteConfigProvider.$inject = [ '$routeProvider', 'authResolver' ];
   function RouteConfigProvider($routeProvider, authResolver){
      $routeProvider.when('/quest-bar', {
         title       : "Quest Bar",
         templateUrl : "templates/quest-bar.html",
         controller  : "QuestBarController",
         controllerAs: "vm",
         resolve     : {
            permissions: authResolver
         }
      });
   }
})();