(function (){
    angular.module('app.quest-bar').controller('QuestBarController', QuestBarController);

    QuestBarController.$inject = [ '$rootScope', '$timeout', '$q','questBarService',"_" ];
    function QuestBarController($rootScope, $timeout, $q, questBarService, _){
        var vm = this;
        vm.loadingState = false;
        vm.searchButtonState = "normal";
        vm.searchString = questBarService.getSearchString();
        vm.searchFilters = questBarService.getFiltersObject();
        vm.filterFields = questBarService.getFilterFields();
        vm.search = search;
        vm.users = questBarService.getResult();
        vm.showOptionsPopup = showOptionsPopup;
        vm.optionsPopupVisible = false;
        vm.searchFiltersUpdated = searchFiltersUpdated;
        vm.showProfile = showProfile;
        vm.nextProfile = nextProfile;
        vm.userPopupVisible = false;
        vm.selectedUser = {};
        construct();

        function construct(){

        }
        function nextProfile(){
            var nextIndex = 0;
            var index = _.findIndex(vm.users,  function(u){
                return u.id===vm.selectedUser.id;
            });
            if(index<(vm.users.length-1)&&index>=0){
                nextIndex = index+1;
            }
            vm.userPopupVisible = false;
            $timeout(function(){
                vm.selectedUser = vm.users[nextIndex];
            }, 200);
            $timeout(function(){
                vm.userPopupVisible = true;
            }, 300);
        }
        function showProfile(user){
            vm.selectedUser = user;
            vm.userPopupVisible = true;
        }
        function searchFiltersUpdated(){
            vm.optionsPopupVisible = false;
            search();
        }
        function showOptionsPopup(){
            vm.optionsPopupVisible = true;

        }
        function search(){
            if(vm.loadingState){
                return;
            }
            var df = $q.defer();
            setLoadingState(true);
            vm.searchButtonState = "pending";
            callService();
            return df.promise;
            function callService(){
                var filters = questBarService.prepareFilters(vm.searchString, vm.searchFilters);
                questBarService.search(filters).then(success).catch(error).finally(cleanup);
            }
            function success(users){
                vm.users = questBarService.getResult();
            }
            function error(message){

            }
            function cleanup(){
                setLoadingState(false);
                vm.searchButtonState = "normal";
            }
        }

        function setLoadingState(flag){
            $rootScope.$applyAsync(function (){
                vm.loadingState = flag;
            });

        }
    }
})();