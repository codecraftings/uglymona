(function (){
   angular.module('app.quest-bar').service('questBarService', QuestBarService);

   QuestBarService.$inject = [ 'urls', '$q', 'users', 'profileFields' ];
   function QuestBarService(urls, $q, users, profileFields){
      var _search_string = "";
      this.getSearchString = getSearchString;
      var _filters = null;
      this.prepareFilters = prepareFilters;
      this.search = search;
      this.getFilterFields = getFilterFields;
      this.getFiltersObject = getFiltersObject;
      var _result = [];
      this.getResult = getResult;
      function getFiltersObject(){
         if ( _filters === null ) {
            _filters = {};
            angular.forEach(profileFields, function (value){
               _filters[ value.key ] = {
                  label: null,
                  value: null
               };
            });
         }
         return _filters;
      }

      function getResult(){
         return _result;
      }

      function getSearchString(){
         return _search_string;
      }

      function getFilterFields(){
         var fields = [];
         angular.copy(profileFields, fields);
         angular.forEach(fields, function (value){
            var options = [
               {
                  label: "Any",
                  value: ""
               }
            ];
            value.options = options.concat(value.options);
         });
         return fields;
      }

      function prepareFilters(searchString, searchFilters){
         var filters = {};
         filters.searchString = searchString;
         angular.forEach(searchFilters, function (selected, key){
            if ( selected.value ) {
               filters[ key ] = selected.value;
            }
         });
         return filters;
      }

      function search(filters){
         var df = $q.defer();
         var list = users.search(filters);
         list.$promise.then(function (){
            _result = list;
            df.resolve(_result);
         })
            .catch(function (error){
               df.reject(error.message);
            });
         return df.promise;
      }
   }
})();