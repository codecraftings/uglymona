(function (){
   angular.module('app').config([
      '$routeProvider', '$locationProvider', 'authResolver', function ($routeProvider, $locationProvider, authResolver){
         $routeProvider.when('/', {
            title       : 'Home',
            templateUrl : "templates/home.html",
            controller  : "HomeController",
            controllerAs: "vm",
            resolve: {
               permissions: authResolver
            }
         });
         $locationProvider.html5Mode(true);
      }
   ]);

})();