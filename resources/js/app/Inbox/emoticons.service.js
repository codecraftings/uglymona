(function (){
   angular.module('app.inbox').service('emoticons', EmoticonsFactory)
      .filter('emos', EmoticonsFilter);
   
   EmoticonsFactory.$inject = [];
   function EmoticonsFactory(){
      var _this = this;
      _this.getAvailableEmosArray = getAvailableEmosList;
      _this.parseEmoticons = parseEmoticons;
      _this.insertEmoticon = insertEmoticon;
      _this.convertTraditionalEmos = convertTraditionalEmos;
      var _startToken = "::";
      var _endToken = "::";

      function convertTraditionalEmos(msg){
         var pattern = /[\)\(\-\*\[\]\/\\"',<>~:_OxPD]+/g;
         var match;
         var list = mapWithTextEmos();
         while ( (match = pattern.exec(msg)) !== null ) {
            var e = match[ 0 ].replace(/\s/g, "");
            if ( angular.isDefined(list[ e ]) ) {
               msg = msg.replace(match[ 0 ], " " + _startToken + list[ e ] + _endToken + " ");
            }
         }
         return msg;
      }

      function getAvailableEmosList(){
         return [
            "smile",
            "sad",
            "wink",
            "laugh",
            "angry",
            "blushing",
            "surprised",
            "cheeky",
            "cool",
            "crying",
            "devil",
            "flower",
            "giggle",
            "inlove",
            "love",
            "makeup",
            "nerd",
            "party",
            "pirate",
            "rofl",
            "sealed",
            "sleepy",
            "smoking",
            "wait",
            "worried"
         ];
      }

      function mapWithTextEmos(){
         return {
            ":)": "smile",
            ":D": "laugh"
         }
      }

      function insertEmoticon(msg, emoName, position){
         var emoToken = " "+convertToken(emoName);
         if ( angular.isUndefined(position) || position === msg.length ) {
            msg +=  emoToken;
            return msg;
         } else {
            var str1 = msg.substr(0, position);
            var str2 = msg.substr(position, msg.length - position);
            msg = str1 + emoToken + str2;
            return msg;
         }
      }

      function convertToken(emoName){
         return _startToken + emoName + _endToken;
      }

      function parseEmoticons(msg){
         var rx = new RegExp(_startToken + "([A-z]+)" + _endToken, "gm");
         var token;
         while ( (token = rx.exec(msg)) !== null ) {
            var match = token[ 0 ];
            var emoName = token[ 1 ];
            if ( ! emoExists(emoName) ) {
               continue;
            }
            var replacement = convertHtml(emoName);
            msg = msg.replace(match, replacement);
         }
         return msg;
      }

      function emoExists(emoName){
         return true;
      }

      function convertHtml(emoName){
         return '<span class="icon-emo-' + emoName + '"></span>';
      }

   }

   EmoticonsFilter.$inject = [ 'emoticons' ];
   function EmoticonsFilter(emoticons){
      return function (input){
         input = emoticons.convertTraditionalEmos(input);
         return emoticons.parseEmoticons(input);
      }
   }
})();