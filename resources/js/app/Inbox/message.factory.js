(function (){
   angular.module('app.inbox').factory('Message', MessageFactory);
   
   MessageFactory.$inject = [ 'Model', "User", 'currentUser', 'emoticons' ];
   function MessageFactory(Model, User, currentUser, emoticons){
      function Message(data){
         var _this = new Model;
         _this.setFields({
            sender  : {},
            receiver: {},
            status  : "",
            age     : 0,
            message : ""
         });
         _this.onFieldsDataUpdated = function (){
            _this.sender = new User(_this.sender);
            _this.receiver = new User(_this.receiver);
            _this.isUnread = _this.sender.id !== currentUser.id && _this.status === "unread";
            if ( currentUser.id !== null && _this.sender.id === currentUser.id ) {
               _this.otherUser = _this.receiver;
            }
            if ( currentUser.id !== null && _this.receiver.id === currentUser.id ) {
               _this.otherUser = _this.sender;
            }

            //_this.message = parseMessage(_this.message);
         };
         _this.setFieldsData(data);
         return _this;

         function parseMessage(msg){
            msg = msg.replace(/\n/g, "<br>");
            msg = emoticons.convertTraditionalEmos(msg);
            msg = emoticons.parseEmoticons(msg);
            return msg;
         }
      }
      
      return Message;
   }
})();