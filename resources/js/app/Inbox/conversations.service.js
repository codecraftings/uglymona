(function (){
   angular.module('app.inbox').service('conversations', ConversationsRepo);
   
   ConversationsRepo.$inject = ['Repo','Conversation'];
   function ConversationsRepo(Repo, Conversation){
      var _this = new Repo("conversations", Conversation);
      return _this;
   }
})();