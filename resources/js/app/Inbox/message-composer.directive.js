(function (){
   angular.module('app.inbox').directive('messageComposer', registerDirective);
   
   registerDirective.$inject = ['$timeout'];
   function registerDirective($timeout){
      var directive = {
         link       : link,
         scope      : {
            receiver     : "=",
            quickMode: "=?",
            messageText: '=?',
            onMessageSent: "&"
         },
         restrict   : 'A',
         templateUrl: "components/message-composer.html",
         controller : Controller
      };
      return directive;
      function link($scope, elm, attr, api){
         angular.element(api.textbox()).on("keydown", handleKeyDown)

         var lastkey = 0;
         function handleKeyDown(e){
            if(angular.isUndefined(attr.quickMode)){
               return;
            }
            if(lastkey!==16&& e.keyCode===13){
               e.preventDefault();
               $timeout($scope.sendMessage);
            }
            lastkey = e.keyCode;
         }

         $scope.$on("$destroy", function (){
            angular.element(api.textbox()).off("keydown", handleKeyDown)
         })
      }
   }
   
   Controller.$inject = [ '$scope', '$attrs', '$element', 'inbox' ]
   function Controller(scope, attr, $element, inbox){
      var api = this;
      api.getCaretPosition = getCaretPosition;
      api.getWritterMessage = getWrittenMessage;
      api.setWritterMessage = setWrittenMessage;
      api.textbox = textbox;
      scope.sendMessage = sendMessage;
      scope.disableAll = false;
      function sendMessage(){
         var msg = getWrittenMessage();
         scope.disableAll = true;
         inbox.sendMessage(scope.receiver, msg)
            .then(function (){
               textbox().value = "";
               if(angular.isDefined(attr.onMessageSent)){
                  scope.onMessageSent();
               }
            })
            .finally(function (){
               scope.disableAll = false;
            });
      }

      function getWrittenMessage(){
         return textbox().value;
      }

      function setWrittenMessage(msg){
         textbox().value = msg;
      }

      function getCaretPosition(){
         return textbox().selectionStart;
      }

      var _textbox = null;

      return api;
      function textbox(){
         if ( ! _textbox ) {
            _textbox = $element.find('textarea')[ 0 ];
         }
         return _textbox;
      }
   }
})();