(function (){
   angular.module('app.inbox').factory('Conversation', ConversationFactory);
   
   ConversationFactory.$inject = [ 'Model', 'Message', 'Collection' ];
   function ConversationFactory(Model, Message, Collection){
      function Conversation(data){
         var _this = new Model;
         _this.setFields({
            user1_id:0,
            user2_id:0,
            last_message: {},
            messages: []
         });
         _this.onDataUpdated(function (){
            _this.lastMessage = new Message(_this.last_message);
            _this.otherUser = _this.lastMessage.otherUser;
            //_this.messages = new Collection(_this.messages, Message);
         });
         _this.setFieldsData(data);

         _this.set = function(data){
            _this.setFieldsData(data);
         }
         _this.belongsTo = belongsTo;
         return _this;

         function belongsTo(user){
            return user.id===_this.user1_id||user.id===_this.user2_id;
         }
      }
      return Conversation;
   }
})();