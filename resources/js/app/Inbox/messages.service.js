(function (){
   angular.module('app.inbox').service('messages', MessagesService);
   
   MessagesService.$inject = [ 'Message', 'Repo', 'api', '$q' ];
   function MessagesService(Message, Repo, api, $q){
      var _this = new Repo("messages", Message);
      _this.getUnreadMessageCount = getUnreadMessageCount;
      _this.getConversationMessages = getConversationMessages;
      _this.send = sendMessage;
      return _this;

      function sendMessage(message){
         return _this.action("/", "post", null, {
            "receiver_id": message.receiver.id,
            message      : message.message
         });
      }

      function getConversationMessages(conversation){
         return _this.action("conversation/" + conversation.id, 'get', null, null, true);
      }

      function getUnreadMessageCount(){
         var df = $q.defer();
         api.get(_this.path("count/unread"))
            .then(function (response){
               df.resolve(response.count);
            }).catch(df.reject);
         return df.promise;
      }
   }
})();