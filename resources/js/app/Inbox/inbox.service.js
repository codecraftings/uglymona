(function (){
   angular.module('app.inbox').service('inbox', InboxService);
   
   InboxService.$inject = [ 'messages', '$q', '$rootScope','event', 'Message', 'currentUser', 'conversations', 'Conversation' ];
   function InboxService(messages, $q, $rootScope, event, Message, currentUser, conversations, Conversation){
      var _this = this;
      var _conversations = [];
      var _active_conversation = new Conversation;
      this.latestConversations = _conversations;
      this.activeConversation = _active_conversation;
      this.fetchLatestConversations = fetchLatestConversations;
      this.fetchMessages = fetchMessages;
      this.sendMessage = sendMessage;
      this.unreadCount = 0;
      this.countUnreadMessages = countUnreadMessages;
      //$rootScope.$on(event.newMessageReceived, handleNewMessageReceived);

      function handleNewMessageReceived($event, data){
         var message = new Message(data.message);
         if(_active_conversation.belongsTo(message.receiver)){
            var msgs = _active_conversation.messages;
            var alreadyAdded = false;
            angular.forEach(msgs, function(msg){
               if(msg.id===message.id){
                  alreadyAdded = true;
               }
            });
            if(!alreadyAdded){
               addMessageToActiveConversation(message);
            }
         }
      }
      function countUnreadMessages(){
         return messages.getUnreadMessageCount().then(function(count){
            _this.unreadCount = count
         });
      }
      function sendMessage(otherUser, messageText){
         messageText = sanitaizeMessage(messageText);
         if ( messageText === "" ) {
            return $q.reject({});
         }
         var df = $q.defer();
         var msg = new Message({
            sender  : currentUser,
            receiver: otherUser,
            message : messageText,
            age     : "Just Now"
         });
         var _m = messages.send(msg)
         _m.$promise.then(function (){
            msg.setFieldsData(_m);
            addMessageToActiveConversation(msg)
            df.resolve();
         }).catch(df.reject);
         return df.promise;
      }

      function addMessageToActiveConversation(msg){
         if(angular.isUndefined(_active_conversation.otherUser)){
            return;
         }
         if ( _active_conversation.otherUser.id === msg.otherUser.id ) {
            _active_conversation.messages.push(msg);
         }
      }

      function sanitaizeMessage(msg){
         msg = msg.replace(/^[\s]*/, "");
         msg = msg.replace(/[\s\n]*$/, "");
         return msg;
      }

      function fetchMessages(conversation){
         _active_conversation.set(conversation);
         var msgs = messages.getConversationMessages(_active_conversation);
         msgs.$promise.then(function (){
            _active_conversation.messages.length = 0;
            msgs.reverse();
            angular.forEach(msgs, function (msg){
               _active_conversation.messages.push(msg);
            });
            countUnreadMessages();
         });
         return msgs.$promise;
      }

      function fetchLatestConversations(){
         var df = $q.defer();
         var convos = conversations.query();
         convos.$promise.then(function (){
            _conversations.length = 0;
            angular.forEach(convos, function (conversation){
               _conversations.push(conversation);
            });
            df.resolve(_conversations);
         }).catch(function (error){
            df.reject(error.message);
         });
         return df.promise;
      }
   }
})();