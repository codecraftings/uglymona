(function (){
   angular.module('app.inbox').directive('inbox', inbox);
   
   inbox.$inject = [ '$rootScope', 'inbox', 'event', 'Message' ];
   function inbox($rootScope, inbox, event, Message){
      var directive = {
         scope      : {},
         link       : link,
         restrict   : 'EA',
         templateUrl: "components/inbox.html"
      };
      return directive;
      
      function link(scope, elm, attr){
         scope.visible = false;
         scope.latestConversations = inbox.latestConversations;
         scope.activeConversation = inbox.activeConversation;
         scope.otherUser = {};
         scope.goToMessages = openLatestMessages;
         scope.openConversation = openFullConversation;
         scope.$on(event.showInbox, showInbox);
         scope.$on(event.hideInbox, hideInbox);
         scope.composeWindow = {};
         scope.composeWindow.visible = false;
         scope.$on(event.newMessage, openComposeWindow);
         scope.$on(event.newMessageReceived, handleNewMessageReceived);

         function handleNewMessageReceived($event, data){
            if ( scope.visible && scope.activeTab === "threads" ) {
               inbox.countUnreadMessages();
               inbox.fetchLatestConversations();
            }
            else if ( scope.visible && scope.activeTab === "messages" ) {
               var message = new Message(data.message);
               if ( scope.activeConversation.otherUser.id === message.sender.id ) {
                  inbox.fetchMessages(scope.activeConversation);
               }
            } else {
               inbox.countUnreadMessages();
            }
         }

         function showInbox($event){
            scope.visible = true;
            openLatestMessages();
         }

         function openLatestMessages(){
            showLoader();
            scope.popupTitle = "Messages";
            scope.activeTab = 'threads';
            inbox.fetchLatestConversations()
               .finally(function (){
                  hideLoader();
               })
         }

         function openComposeWindow($event, receiver, prefilledMessage){
            scope.composeWindow.visible = true;
            prefilledMessage = prefilledMessage || "";
            scope.composeWindow.messageInput = prefilledMessage;
            scope.otherUser = receiver;
         }

         function openFullConversation(conversation){
            showLoader();
            scope.activeTab = 'messages';
            scope.popupTitle = "Conversation With " + conversation.otherUser.display_name;
            inbox.fetchMessages(conversation)
               .finally(function (){
                  hideLoader();
               });
         }

         function showLoader(){
            scope.loaderVisible = true;
         }

         function hideLoader(){
            scope.loaderVisible = false;
         }

         function hideInbox(){
            scope.visible = false;
         }
      }
   }
})();