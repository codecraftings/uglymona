(function (){
   angular.module('app.inbox').directive('emoPicker', emoPicker);
   
   emoPicker.$inject = [ 'emoticons' ];
   function emoPicker(emoticons){
      var directive = {
         scope      : { pkDisabled: "=?" },
         require    : "^messageComposer",
         link       : link,
         restrict   : 'A',
         templateUrl: 'components/emo-picker.html'
      };
      return directive;
      
      function link($scope, elm, attr, composer){
         $scope.dropDownOpen = false;
         $scope.emos = emoticons.getAvailableEmosArray();
         $scope.insertEmo = insertEmo;

         function insertEmo(emoName){
            $scope.dropDownOpen = false
            var msg = composer.getWritterMessage();
            msg = emoticons.insertEmoticon(msg, emoName, composer.getCaretPosition());
            composer.setWritterMessage(msg);
         }

         $scope.$watch("dropDownOpen", function (newValue){
            if ( newValue ) {
               onDropDownVisible();
            } else {
               onDropDownHidden();
            }
         });
         $scope.$on("$destroy", function (){
            onDropDownHidden();
         })
         function onDropDownVisible(){
            angular.element('body').on('click', checkFocusLost);
         }

         function onDropDownHidden(){
            angular.element('body').off('click', checkFocusLost);
         }

         function checkFocusLost(e){
            if ( elm[ 0 ] !== e.target && ! elm[ 0 ].contains(e.target) ) {
               $scope.$applyAsync(function (){
                  $scope.dropDownOpen = false;
               });
            }
         }
      }
   }
})();