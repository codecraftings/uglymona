(function (){
   angular.module('app').config(configApp).run(bootApp);

   configApp.$inject = [ '$sceDelegateProvider', 'base_url' ];
   function configApp($sceDelegateProvider, base_url){
      $sceDelegateProvider.resourceUrlWhitelist([
         "self",
         base_url.asset + "**"
      ]);
   }

   bootApp.$inject = [
      '$rootScope',
      '$window',
      'event',
      'auth',
      '$location',
      '$route',
      'urls',
      'socket',
      'notifications',
      '_'
   ];
   function bootApp($rootScope, $window, event, auth, $location, $route, urls, socket, notifications, _){
      $rootScope.pageTitle = "Uglymona";
      $rootScope.$on(event.TokenInvalidated, _.debounce(handleTokenInvalid, 1000, true));
      $rootScope.$on(event.routeChangeStart, handleRouteChangeStart);
      $rootScope.$on(event.routeChangeSuccess, handleRouteChangeDone);
      $rootScope.$on(event.routeChangeError, handleRouteError);
      socket.connect();
      notifications.registerListeners();
      $window.videojs.options.flash.swf = urls.asset("js/video-js.swf");
      function handleRouteChangeStart($event){
         $rootScope.$broadcast(event.closeAllPopups);
      }

      function handleRouteChangeDone($event){
         $rootScope.pageTitle = $route.current.title + " | Uglymona";
      }

      function handleRouteError(){
         $window.history.back();
      }

      function handleTokenInvalid(){
         urls.pending_path = $location.path();
         auth.logout().finally(_.delay(function (){
            $location.path("login");
            $rootScope.$broadcast(event.hideLoader, true);
         }, 0));
      }
   }
})();