(function (){
   angular.module('app.wheel').controller('ProfileWheelController', ProfileWheelController);
   
   ProfileWheelController.$inject = ['$timeout'];
   function ProfileWheelController($timeout){
      var vm = this;
      //vm.tt = "hello";
      vm.startSpin = function(){
         vm.tt.startSpining();
         $timeout(function(){
            vm.tt.stopSpining();
         }, 4000);
      }
   }
})();