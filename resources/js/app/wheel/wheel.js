var wheel = {};
(function ($) {
    $.fn.circle_text = function (opt) {
        var css = {
            font: opt.fontSize + "px " + opt.fontFamily + ", MonoSpace",
            height: opt.radius + "px",
            position: "absolute",
            width: '1px',
            left: opt.radius + "px",
            top: 0,
            color: opt.fontColor,
            "font-weight": opt.fontWeight,
            display: "block",
            "transform-origin": "bottom center",
            transform: "rotate(6deg)"
        };
        var str = opt.str ? opt.str : $(this).html();
        $(this).css({
            position: "relative",
            width: opt.radius * 2 + "px",
            height: opt.radius * 2 + "px",
        });
        var container = $('<div class="circled-text" />');
        $(this).html("").append(container);
        container.css({
            position: 'absolute',
            "margin-left": 0 + "px",
            width: opt.radius * 2 + "px",
            height: opt.radius * 2 + "px",
            top: '0%',
            background: 'transparent',
            "transform-origin": "center center"
        });

        var p = opt.angle / str.length;
        var start = opt.angle / 2 * (-1);
        for (var i = 0; i < str.length; i++) {
            var elm = $('<span />');
            elm.html(str[i]);
            css.transform = "rotate(" + (start + p * (i + 1)) + "deg)";
            elm.css(css);
            $(container).append(elm);
        }
    };
    $.fn.spin_wheel = function (time, roundCount, callback) {
        var end = 180 * roundCount;
        $(this).css('transform', 'rotate(0deg)');
        var $this = $(this);
        animate({
            delay: 40,
            duration: time,
            delta: function (progress) {
                var d;
                if (progress < .5) {
                    d = Math.pow((2 * progress), 2) / 2;
                }
                else {
                    d = 2 - Math.pow(2 * (1 - progress), 2) / 2;
                }
                return d;
            },
            step: function (delta) {
                $this.css('transform', 'rotate(' + end * delta + 'deg)');
            },
            callback: function () {
                if (typeof callback != "undefined") {
                    callback();
                }
            }
        });
        function animate(opts) {

            var start = new Date

            var id = setInterval(function () {
                var timePassed = new Date - start
                var progress = timePassed / opts.duration

                if (progress > 1) progress = 1

                var delta = opts.delta(progress)
                opts.step(delta)

                if (progress == 1) {
                    clearInterval(id);
                    opts.callback();
                }
            }, opts.delay || 10)

        }
    };
    $(function () {
        window.SpiningView = Backbone.View.extend({
            initialize: function (elm) {
                this.setElement($(elm));
            },
            accl: 0,
            velocity: 0,
            rotation: 0,
            frameRate: 40,
            timer: null,
            render: function () {
                this.interpolate();
                this.$el.css('transform', 'rotate(' + this.rotation + 'deg)');
                if (this.velocity === 0) {
                    this.stopSpining();
                }
            },
            interpolate: function () {
                //this.accl = this.accl - 2;
                //this.accl = this.accl<0?0:this.accl;
                //this.velocity = this.velocity + this.accl;
                this.velocity = this.velocity < 0 ? 0 : this.velocity;
                this.rotation = this.rotation + this.velocity;
                this.rotation = this.rotation > 360 ? (this.rotation - 360) : this.rotation;
                this.rotation = this.rotation < 0 ? (360 - this.rotation) : this.rotation;
                console.log("accl: " + this.accl + ", val: " + this.velocity + ", rotatin: " + this.rotation);
            },
            startSpining: function () {
                this.velocity = 1;
                this.applyForce(0.4, 1000);
                this.timer = setInterval(_.bind(this.render, this), 1000 / this.frameRate);
            },
            applyForce: function (force, forMilisecs, callback) {
                var tt;
                var $this = this;
                var frame = 1000 / this.frameRate;
                var passed = 0;
                tt = setInterval(function () {
                    passed += frame;
                    $this.velocity += force;
                    if (passed > forMilisecs) {
                        if (typeof callback != "undefined") {
                            callback();
                        }
                        clearInterval(tt);
                    }
                }, frame);
            },
            tryStopSpining: function (callback) {
                this.applyForce(-0.6, 1000, callback);
            },
            stopSpining: function () {
                clearInterval(this.timer);
            }
        });
        var WheelView = Backbone.View.extend({
            el: $('.wheel-view'),
            events: {
                "click #wheel-start-spin-btn": "startSpining",
                "click .wheel-cross-btn": "reset",
                "click .wheel-icon.tick": "showDetail",
                "click .wheel-icon.cross": "reset",
                "click .wheel-nav-button.next": "showNextProfile",
                "click .wheel-nav-button.prev": "showPreviousProfile"
            },
            users: null,
            current_user_index: null,
            itemsWheel: null,
            width: 650,
            height: 650,
            initialize: function () {
                this.users = new Mona.Users();
                this.itemsWheel = new SpiningView($("#wheel-menu-items"));
                this.calculateSize();
                this.render();
                this.changeTitle("CLICK BELOW TO SPIN");
                $('#wheel-menu-items .circled-text').spin_wheel(1500, .5);
                this.toggleNavButtons();
                var $this = this;
                window.onresize = function(){
                    //_.throttle($this.handleResize(), 2000);
                }
            },
            calculateSize: function(){
            if(window.innerWidth<700) {
                this.width = window.innerWidth - 50;
                this.height = this.width;
            }
            },
            render: function(){

                $('#wheel-menu-items').circle_text({
                                                       radius: this.width/2-this.width*0.035,
                                                       angle: 360,
                                                       fontSize: this.width*0.035,
                                                       fontWeight: "bold",
                                                       fontFamily: "Anonymous Pro",
                                                       fontColor: "white"
                                                   });
                $('#wheel-menu-items').css("margin", (this.width*0.045)+"px 0px 0px "+(this.width*0.035)+"px");
                this.$el.css({
                                 'top': (this.height/2*(-1))+"px",
                                'width': this.width,
                                'left': ((window.innerWidth>1024?1024:(window.innerWidth-20))-this.width)/2,
                    'height': this.height
                             });
                $('.wheel-nav-buttons').css({
                    top: (this.height/2-10)+"px"
                                                 });
                this.$(".wheel-matching-text").css('font-size',this.width*0.107+"px");
                this.$('.wheel-profile-data').css('font-size', this.width*0.033+"px");
            },
            handleResize: function(){
                this.calculateSize();
                this.render();
            },
            toggleNavButtons: function () {
                if (this.users.length > 1) {
                    this.$('.wheel-nav-button').show();
                } else {
                    this.$('.wheel-nav-button').hide();
                }
            },
            setTitle: function (str) {
                $('#wheel-top-layer-title').circle_text({
                    str: str,
                    radius: this.width*0.34,
                    angle: 7 * str.length,
                    fontSize: this.width*0.05,
                    fontWeight: "normal",
                    fontFamily: "Anonymous Pro",
                    fontColor: "white"
                });
            },
            changeTitle: function (str) {
                $('#wheel-top-layer-title')
                    .removeClass("anim-flashIn")
                    .addClass("anim-flashOut");
                var $this = this;
                setTimeout(function () {
                    $this.setTitle(str);
                    _.defer(function () {
                        //return;
                        $('#wheel-top-layer-title')
                            .removeClass("anim-flashOut")
                            .addClass("anim-flashIn");
                    });
                }, 500);
            },
            isSpining: false,
            reset: function () {
                this.toggleNavButtons();
                this.changeTitle("CLICK BELOW TO SPIN");
                if (this.detailVisible) {
                    this.hideDetail();
                }
                if (this.profileVisible) {
                    this.hideProfile();
                }
                $('#wheel-start-spin-btn').removeClass("disabled");
                $('#wheel-start-spin-btn').fadeIn('300');
                $('.wheel-icon.tick, .wheel-icon.cross').addClass('disabled');
            },
            startSpining: function () {
                if (this.isSpining) {
                    return;
                }
                sounds.spin().play().fadeIn(100);
                this.isSpining = true;
                $('#wheel-start-spin-btn').addClass("disabled");
                this.changeTitle("Spinning..");
                this.itemsWheel.startSpining();
                var $this = this;
                this.fetchProfile();
                setTimeout(function(){
                    $this.stopSpining();
                    $this.showProfile();
                }, 12000);
            },
            stopSpining: function () {
                sounds.spin().fadeOut(400, function(){
                    sounds.spin().stop();
                });
                var $this = this;
                this.itemsWheel.tryStopSpining(function () {
                    $this.isSpining = false;
                });
            },
            fetchProfile: function () {
                var df = jQuery.Deferred();
                var $this = this;
                $.ajax({url: base_url + "/profile-wheel/find-match", dataType: 'json'}).done(function (res) {
                    var user = new Mona.User(res);
                    user = $this.users.add(user, {merge: true});
//                    $this.current_user_index = $this.users.length - 1;
                    _.defer(function () {
                        $this.current_user_index = $this.users.indexOf(user);
                        $this.toggleNavButtons();
                        df.resolve();
                    });
                    //$this.showProfile();
                }).fail(function () {
                    df.reject();
                });
                return df.promise();
            },
            detailVisible: false,
            showDetail: function (e) {
                this.detailVisible = true;
                $("#wheel-most-top-layer")
                    .removeClass("swashIn")
                    .addClass("magictime swashOut anim-duration1");
                _.delay(function () {
                    $("#wheel-user-details-layer")
                        .removeClass("swashOut hidden")
                        .addClass("magictime swashIn anim-duration1");
                }, 500);
            },
            hideDetail: function (e) {
                $("#wheel-user-details-layer")
                    .removeClass("swashIn")
                    .addClass("magictime swashOut anim-duration1");
                _.delay(function () {
                    $("#wheel-most-top-layer")
                        .removeClass("swashOut")
                        .addClass("magictime swashIn anim-duration1");
                }, 500);
                this.detailVisible = false;
            },
            profileVisible: false,
            showProfile: function (user) {
                sounds.pop().play();
                var $this = this;
                if (typeof user === "undefined") {
                    user = this.users.at(this.current_user_index);
                }
                this.profileVisible = true;
                this.changeTitle(user.get('full_name') + ", " + user.get('profile').age + " Y.O.");
                $("#wheel-user-pic-src-holder").attr('src', user.get('profile').profile_pic);
                $(".wheel-matching-text .count").html(user.get("matching") + "%");
                $('#wheel-start-spin-btn').fadeOut('300', function () {
                    $(".wheel-matching-text").removeClass('hidden');
                });
                $('.wheel-icon.tick, .wheel-icon.cross').removeClass('disabled');
                this.$('.wheel-profile-data h6').html(user.get('full_name'));
                this.$('.wheel-profile-data ul').html("");
                _.each(user.get('profile_fields'), function (value) {
                    $this.$('.wheel-profile-data ul').append("<li>" + value + "</li>");
                });
                $('.wheel-user-pic-container .angled-arrow').css({
                    top: "160px",
                    left: "0px"
                }).removeClass('hidden').animate({
                    top: "123px",
                    left: "41px"
                }, 100, function () {
                    $('.wheel-user-pic-container .pic-holder').css({
                        top: "40px",
                        left: "-80px"
                    }).removeClass('hidden').animate({
                        top: "0px",
                        left: "0px"
                    }, 400);
                });
            },
            hideProfile: function () {
                this.profileVisible = false;
                $(".wheel-matching-text").addClass('hidden');
                $('.wheel-user-pic-container .angled-arrow').addClass('hidden')
                $('.wheel-user-pic-container .pic-holder').animate({
                    top: "60px",
                    left: "-140px"
                }, 400, function () {
                    $('.wheel-user-pic-container .pic-holder').addClass('hidden');
                    $('.wheel-user-pic-container .angled-arrow').animate({
                        top: "160px",
                        left: "0px"
                    }, 200, function () {
                        $('.wheel-user-pic-container .angled-arrow').addClass('hidden')
                    });
                });
            },
            showPreviousProfile: function (e) {
                if (typeof e !== "undefined") {
                    if($(e.target).hasClass("disabled")){
                        return;
                    }
                    $(e.target).addClass("disabled");
                }
                var $this = this;
                if ($this.profileVisible) {
                    $this.current_user_index -= ($this.current_user_index <= 0 ? 0 : 1);
                }
                $this.reset();
                _.delay(function () {
                    $this.showProfile();
                    if(typeof e != "undefined"){
                        $(e.target).removeClass("disabled");
                    }
                }, 400);
            },
            showNextProfile: function (e) {
                if (typeof e !== "undefined") {
                    if($(e.target).hasClass("disabled")){
                        return;
                    }
                    $(e.target).addClass("disabled");
                }
                var $this = this;
                if ($this.profileVisible) {
                    $this.current_user_index += ($this.current_user_index >= ($this.users.length-1) ? 0 : 1);
                }
                $this.reset();
                _.delay(function () {
                    $this.showProfile();
                    if(typeof e != "undefined"){
                        $(e.target).removeClass("disabled");
                    }
                }, 400);
            }
        });
        if($('.wheel-view').length){
            wheel = new WheelView;
        }



    });
})(jQuery);