(function (){
   angular.module('app.wheel').factory('Match', MatchFactory);
   
   MatchFactory.$inject = [ 'User' ];
   function MatchFactory(User){
      function Match(data){
         var _this = new User();
         _this.setFields({
            percentage: "0%"
         });
         _this.onDataUpdated(function (){
         });
         _this.setFieldsData(data);
         return _this;
      }
      
      return Match;
   }
})();