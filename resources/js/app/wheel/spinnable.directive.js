(function (){
   angular.module('app.wheel').directive('spinnable', registerDirective);
   
   registerDirective.$inject = [];
   function registerDirective(){
      var directive = {
         link      : link,
         restrict  : 'A',
         controller: Controller
      };
      return directive;

      function link($scope, elm, attr){
         $scope.rotateView = function (angle){
            elm.css('transform', 'rotate(' + angle + 'deg)');
         }
      }
   }

   var frameRate = 40;

   function Spinnable($interval){
      var _this = this;
      var velocity = 0;
      var rotation = 0;
      _this.getRotation = getRotation;
      _this.getVelocity = getVelocity;
      _this.applyTorque = applyTorque;
      _this.update = update;

      function update(){
         rotation = getRotation() + getVelocity();
      }

      function applyTorque(force, forMilisecs, callback){
         var tt;
         var frame = 1000 / frameRate;
         var passed = frame;
         velocity = getVelocity() + force;
         tt = $interval(function (){
            passed += frame;
            velocity += force;
            if ( passed > forMilisecs ) {
               if ( typeof callback != "undefined" ) {
                  callback();
               }
               $interval.cancel(tt);
            }
         }, frame);
      }

      function getRotation(){
         if ( rotation < 0 ) {
            return 360 - rotation;
         } else if ( rotation > 360 ) {
            return rotation - 360;
         } else {
            return rotation;
         }
      }

      function getVelocity(){
         return velocity < 0 ? 0 : velocity;
      }
   }

   function AnimationLoop(spinnable, scope, $interval){
      var _this = this;
      var timer = null;
      _this.start = start;
      function start(){
         timer = $interval(loop, 1000 / frameRate);
      }

      function loop(){
         update();
         render();
         if ( spinnable.getVelocity() === 0 ) {
            stopLoop();
         }
      }

      function stopLoop(){
         $interval.cancel(timer);
      }

      function update(){
         spinnable.update();
      }

      function render(){
         scope.rotateView(spinnable.getRotation());
      }
   }

   Controller.$inject = [ '$scope','$attrs', '$interval','$q','alias' ]
   function Controller(scope,attr, $interval, $q,alias){
      var api = this;
      var spinnable = new Spinnable($interval);
      var loop = new AnimationLoop(spinnable, scope, $interval);
      api.startSpining = _startSpining;
      api.stopSpining = _stopSpining;
      function _startSpining(){
         spinnable.applyTorque(0.3, 1000);
         loop.start();
      }
      function _stopSpining(){
         var df = $q.defer();
         spinnable.applyTorque(-0.5, 1400, function(){
            df.resolve();
         });
         return df.promise;
      }
      return alias.bindApi(scope, attr.alias, api);
   }
})();