(function (){
   angular.module('app.wheel').service('wheel', WheelService);
   
   WheelService.$inject = [ 'api', 'urls', '$q','Match' ];
   function WheelService(api, urls, $q, Match){
      var _this = this;
      _this.getMatchHistory = _getMatchHistory;
      _this.findNewMatch = _findNewMatch;
      var matches = new MatchHistory();

      function _findNewMatch(){
         var df = $q.defer();
         api.get('profile-wheel/find-match').then(function (response){
            matches.push(new Match(response.data));
            df.resolve();
         }).catch(function (error){
            df.reject(error.message);
         });
         return df.promise;
      }

      function _getMatchHistory(){
         return matches;
      }
   }

   function MatchHistory(){
      var _this = this;
      var matches = [];
      _this.canGoBackward = _canGoBackward;
      _this.canGoForward = _canGoForward;

      _this.goBackward = _goBackward;
      _this.goForward = _goForward;

      _this.push = _push;
      _this.total = _total;
      _this.getCurrent = _getCurrent;
      _this.currentPosition = null;

      function _getCurrent(){
         if ( _this.currentPosition !== null ) {
            return matches[ _this.currentPosition ];
         } else {
            return null;
         }
      }

      function _push(match){
         matches.push(match);
         _this.currentPosition = matches.length - 1;
      }
      function _total(){
         return matches.length;
      }
      function _goForward(){
         if ( _canGoForward() ) {
            _this.currentPosition += 1;
         }
      }

      function _goBackward(){
         if ( _canGoBackward() ) {
            _this.currentPosition -= 1;
         }
      }

      function _canGoForward(){
         return _this.currentPosition < (matches.length - 1);
      }

      function _canGoBackward(){
         return _this.currentPosition > 0;
      }
   }
})();