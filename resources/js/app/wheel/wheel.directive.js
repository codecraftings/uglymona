(function (){
   angular.module('app.wheel').directive('monaWheel', registerDirective);
   
   registerDirective.$inject = [];
   function registerDirective(){
      var directive = {
         link        : link,
         restrict    : 'A',
         controller  : Controller,
         controllerAs: "wheel"
      };
      return directive;
      function link($scope, elm, attr, controller){
         elm.css('width', controller.width+"px");
         elm.css('height', controller.width+"px");
         elm.css('top',(controller.width/2*(-1))+"px");
         elm.css('font-size', controller.width*0.028+"px");
      }
   }
   
   Controller.$inject = [ '$scope', '$attrs', 'alias', '$timeout', 'wheel','$rootScope','$window' ]
   function Controller(scope, attr, alias, $timeout, service, $rootScope, $window){
      var wheel = this;
      wheel.bottomLayer = {};
      wheel.statusLabel = "Click Below to Spin";
      wheel.width = 650;
      if($window.innerWidth<650){
         wheel.width = $window.innerWidth - 25;
      }
      wheel.match = {};
      wheel.matchVisible = false;
      wheel.detailsVisible = false;
      wheel.matchHistory = service.getMatchHistory();
      wheel.navigator = new Navigator(wheel.matchHistory);
      wheel.showPrevMatch = _showPrevMatch;
      wheel.showNextMatch = _showNextMatch;

      wheel.spin = _spin;
      wheel.reset = _reset;
      return alias.bindApi(scope, attr.alias, wheel);

      function _spin(){
         _reset();
         wheel.statusLabel = "Spinning..";
         wheel.isSpining = true;
         wheel.bottomLayer.startSpining();
         $timeout(function (){
               stopSpining();
               showMatch();
            }
            , 2000);
         findMatch().then(function (){
            //showMatch();
         }).catch(function (error){
            console.log(error);
         });
      }

      function showMatch(){
         wheel.match = wheel.matchHistory.getCurrent();
         if ( wheel.match === null ) {
            return;
         }
         wheel.statusLabel = wheel.match.full_name;
         wheel.navigator.setOpen();
         wheel.matchVisible = true;
      }

      function findMatch(){
         return service.findNewMatch();
      }

      function stopSpining(){
         wheel.isSpining = false;
         wheel.bottomLayer.stopSpining();
      }
      function _reset(){
         wheel.matchVisible = false;
         wheel.detailsVisible = false;
         if ( wheel.isSpining ) {
            wheel.isSpining = false;
            stopSpining();
         }
         wheel.statusLabel = "Click Below to Spin";
         wheel.navigator.setClosed();
      }

      function _showPrevMatch(){
         wheel.navigator.goBackward();
         showMatch();
      }

      function _showNextMatch(){
         wheel.navigator.goForward();
         showMatch();
      }
   }

   function Navigator(matchHistory){
      var _this = this;
      var _closed = true;
      _this.setClosed = _setClosed;
      _this.setOpen = _setOpen;
      _this.canGoBackward = _canGoBackward;
      _this.canGoForward = _canGoForward;

      _this.goBackward = _goBackward;
      _this.goForward = _goForward;
      function _setClosed(){
         _closed = true;
      }

      function _setOpen(){
         _closed = false;
      }

      function _canGoBackward(){
         return matchHistory.canGoBackward() || (matchHistory.total() > 0 && _closed);
      }

      function _canGoForward(){
         return !_closed&&matchHistory.canGoForward();
      }

      function _goBackward(){
         if(_closed===false){
            matchHistory.goBackward();
         }
      }

      function _goForward(){
         if(_closed===false){
            matchHistory.goForward();
         }
      }
   }
})();