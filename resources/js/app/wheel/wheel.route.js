(function (){
   angular.module('app.wheel')
      .config(RouteConfigProvider);
   
   RouteConfigProvider.$inject = [ '$routeProvider', 'authResolver' ];
   function RouteConfigProvider($routeProvider, authResolver){
      $routeProvider.when('/profile-wheel', {
         title   : "Spin The Weel",
         templateUrl : "templates/profile-wheel.html",
         controller  : "ProfileWheelController",
         controllerAs: "vm",
         accessLevel : "user",
         permissions: ["wheel"],
         resolve     : {
            permissions: authResolver
         }
      });
   }
})();