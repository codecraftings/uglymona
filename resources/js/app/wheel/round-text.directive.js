(function (){
   angular.module('app.wheel').directive('roundText', registerDirective);
   
   registerDirective.$inject = ['$timeout'];
   function registerDirective($timeout){
      var directive = {
         link      : link,
         restrict  : 'A',
         controller: Controller
      };
      return directive;
      function link($scope, elm, attr, controller){
         if(angular.isDefined(attr.text)){
            bindTextChange(elm, attr, controller);
         }else{
            controller.setText(elm.text());
         }
      }
      function bindTextChange(elm, attr, controller){
         attr.$observe('text', function(value){
            elm.removeClass('anim-flashIn').addClass('anim-flashOut');
            $timeout(function(){
               controller.setText(value);
               elm.removeClass('anim-flashOut').addClass('anim-flashIn');
            }, 500);
         });
      }
   }


   Controller.$inject = [ '$scope','$element','$attrs','alias' ];
   function Controller($scope,elm, attr, alias){
      var api = this;
      api.setText = setText;
      return alias.bindApi($scope, attr.alias, api);
      function setText(text){
         render(elm, text, attr);
      }
   }

   function render(elm, text, opt){
      var container = angular.element('<div class="circled-text" />');
      elm.html("").append(container);
      opt = mergeOptions(opt);
      elm.css(getElementsCss(opt));
      container.css(getContainersCss(opt));
      var css = getItemsCss(opt);
      var rotationAngle = opt.angle / text.length;
      var initialAngle = opt.angle / 2 * (- 1);
      for ( var i = 0; i < text.length; i ++ ) {
         var item = $('<span />');
         item.html(text[ i ]);
         var angle = initialAngle + rotationAngle* (i + 1);
         css.transform = "rotate(" + angle + "deg)";
         item.css(css);
         container.append(item);
      }
   }

   function getElementsCss(opt){
      var css = {
         position: "relative",
         width   : opt.width + "px",
         height  : opt.width + "px",
      };
      return css;
   }

   function getContainersCss(opt){
      var css = {
         position          : 'absolute',
         "margin-left"     : 0 + "px",
         width             : opt.width + "px",
         height            : opt.width + "px",
         top               : '0%',
         background        : 'transparent',
         "transform-origin": "center center"
      };
      return css;
   }

   function getItemsCss(opt){
      var css = {
         font              : opt.fontSize + "px " + opt.fontFamily + ", MonoSpace",
         height            : opt.radius + "px",
         position          : "absolute",
         width             : '1px',
         left              : opt.radius + "px",
         top               : 0,
         color             : opt.fontColor,
         "font-weight"     : opt.fontWeight,
         display           : "block",
         "transform-origin": "bottom center",
         transform         : "rotate(6deg)"
      };
      return css;
   }

   function mergeOptions(opt){
      opt = angular.extend({
         width     : 600,
         fontSize  : 20,
         angle     : 360,
         fontWeight: "400",
         fontFamily: "Droid Sans Mono",
         fontColor : "white"
      }, opt);
      if ( angular.isUndefined(opt.radius) ) {
         opt.radius = opt.width / 2;
      }
      return opt;
   }
})();