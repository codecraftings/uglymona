(function(){
	angular.module('app.component')
		.directive('monaSlider', monaSlider);
	monaSlider.$inject = ['$','device'];
	function monaSlider($, device){
		var dir = {
			restrict: 'A',
			link: link
		};
		return dir;
		function link(scope, elm, attr){
		   var maxSlide = 3;
		   var slideMargin = 160;
		   if(device.width<1024){
			  maxSlide = 2;
		   }
		   if(device.width<840){
			  slideMargin = 100;
		   }
		   if(device.width<740){
			  slideMargin = 50;
		   }
		   if(device.width<680){
			  maxSlide = 1;
		   }
			var slider = elm.bxSlider({
				mode: 'horizontal',
				speed: 600,
				slideMargin: slideMargin,
				easing: 'easeOutElastic',
				useCss: true,
				pager: false,
				controls: false,
				auto: true,
				pause: 2500,
				autoHover: true,
				minSlides: 1,
				maxSlides: maxSlide,
				moveSlides: 3,
				slideWidth: 220
			});
			$('.slider-control-left').click(function(){
				slider.goToPrevSlide();
			})
			$('.slider-control-right').click(function(){
				slider.goToNextSlide();
			})
		}
	}
})();