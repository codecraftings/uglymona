(function (){
   angular.module('app.component').directive('scrollView', scrollView);

   scrollView.$inject = [ "_","device" ];
   function scrollView(_, device){
      var directive = {
         //scope: {},
         link       : link,
         //transclude: true,
         restrict   : 'EA',
         templateUrl: ''
      };
      return directive;

      function link(scope, elm, attr){
         if(device.width<800){
            return;
         }
         var config = {
            axis             : 'y',
            scrollbarPosition: "outside",
            autoHideScrollbar: true,
            snapAmount       : 20,
            snapOffset       : 0,
            scrollInertia    : 500,
            mouseWheel       : {
               deltaFactor: 218
            },
            callbacks: {
               onUpdate: scrollTo
            }
         };
         if ( angular.isDefined(attr.deltaFactor) ) {
            config.mouseWheel.deltaFactor = attr.deltaFactor;
         }
         if ( angular.isDefined(attr.scrollbarPosition) ) {
            config.scrollbarPosition = attr.scrollbarPosition;
         }
         if ( angular.isDefined(attr.maxHeight) ) {
            elm.css("height", attr.maxHeight);
         }
         _.defer(function (){
            elm.mCustomScrollbar(config);

         });

         function scrollTo(){
            if ( angular.isDefined(attr.scrollTo) ) {
               _.defer(function (){
                  elm.mCustomScrollbar('scrollTo', attr.scrollTo, {});
               });
            }
         }
      }
   }
})();