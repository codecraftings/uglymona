(function (){
   angular.module('app.component').directive('notiToast', notiToast);
   
   notiToast.$inject = ['$timeout', '$animate' ];
   function notiToast($timeout, $animate){
      var directive = {
         scope      : {
            noti: '=notiData'
         },
         link       : link,
         restrict   : 'A',
         templateUrl: "components/noti-toast.html"
      };
      return directive;

      function link(scope, elm, attr){
         elm.addClass("noti-toast-container");
         elm.on('click', handleNotiClicked);
         $animate.addClass(elm, 'visible');
         $timeout(hide, 5000);
         function hide(){
            $animate.removeClass(elm, "visible")
               .finally(function (){
                  elm.remove();
               })
         }

         function handleNotiClicked(){

         }
      }
   }
})();