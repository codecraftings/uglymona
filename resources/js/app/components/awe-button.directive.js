(function (){
    angular.module('app.component').directive('aweButton', aweButton);
    aweButton.$inject = [ '_', '$rootScope' ];
    function aweButton(_, $rootScope){
        var directive = {
            scope: {
                controlState: '=?',
                normalAction: '&',
                inverseAction: '&',
                pendingClass: '@',
                activeClass: '@',
                normalLabel: '@',
                activeLabel: '@',
                pendingLabel: '@'
            },
            link: link,
            restrict: 'A',
            templateUrl: ''
        };
        return directive;

        function link(scope, elm, attr){
            if ( ! angular.isDefined(scope.activeClass) ) {
                scope.activeClass = "active";
            }
            if ( ! angular.isDefined(scope.pendingClass) ) {
                scope.pendingClass = "pending";
            }
            if ( ! angular.isDefined(scope.controlState) ) {
                scope.controlState = "normal";
            }
            if(!angular.isDefined(attr.normalAction)){
                scope.normalAction = null;
            }
            if(!angular.isDefined(attr.inverseAction)){
                scope.inverseAction = null;
            }
            elm.addClass("awe-button");
            scope.$watch("controlState", manageStateChange);
            elm.on("click", _.debounce(manageClick,200, true));
            //$log.info(scope);
            function manageStateChange(newVal, oldVal){
                switch(scope.controlState){
                    case "active":
                        elm.addClass(scope.activeClass);
                        elm.removeClass(scope.pendingClass);
                        if(angular.isDefined(scope.activeLabel)){
                            elm.html(scope.activeLabel);
                        }
                        break;
                    case "pending":
                        elm.addClass(scope.pendingClass);
                        elm.removeClass(scope.activeClass);
                        if(angular.isDefined(scope.pendingLabel)){
                            elm.html(scope.pendingLabel);
                        }
                        break;
                    case "normal":
                    default:
                        elm.removeClass(scope.activeClass);
                        elm.removeClass(scope.pendingClass);
                        if(angular.isDefined(scope.normalLabel)){
                            elm.html(scope.normalLabel);
                        }
                        break;
                }
            }
            function setControlState(newState){
                scope.$applyAsync(function(){
                    scope.controlState = newState;
                });
            }
            function manageClick(e){
                e.preventDefault();
                switch (scope.controlState) {
                    case "pending":
                        return;
                        break;
                    case "active":
                        invokeInverseFn();
                        break;
                    case "normal":
                    default:
                        invokeMainFn();
                        break;
                }
            }

            function invokeMainFn(){
                if ( angular.isFunction(scope.normalAction) ) {
                    setControlState("pending");
                    var response = scope.normalAction();
                    if ( response&&angular.isDefined(response.then) ) {
                        response.then(function (){
                            setControlState("active");
                        }, function (){
                            setControlState("normal");
                        });
                    }
                }
            }

            function invokeInverseFn(){
                if ( angular.isFunction(scope.inverseAction) ) {
                    setControlState("pending");
                    var response = scope.inverseAction();
                    if ( response && angular.isDefined(response.then) ) {
                        response.then(function (){
                            setControlState("normal");
                        }, function (){
                            setControlState("active");
                        });
                    }
                }
            }
        }
    }
})();