(function (){
   angular.module('app.component').directive('alertBox', alertBox);
   
   alertBox.$inject = [ '$rootScope', 'event' ];
   function alertBox($rootScope, event){
      var directive = {
         scope      : {
            alertVisible: "="
         },
         templateUrl: 'components/alert-box.html',
         restrict   : 'A',
         controller: Controller,
         link       : link
      };
      Controller.$inject = ['$scope']
      return directive;
      
      function link(scope, elm, attr){

      }
      function Controller($scope){
         $scope.alertVisible = false;
         $rootScope.$on(event.showAlert, function (event, alert){
            console.log(alert);
            $scope.alertTitle = alert.title;
            $scope.alertMessage = alert.message;
            $scope.onAlertClosed = alert.onClose;
            $scope.alertVisible = true;
         });
      }
   }
})();