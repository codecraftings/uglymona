(function (){
   angular.module('app.component').directive('monaPopup', monaPopup);

   monaPopup.$inject = [ '$rootScope', 'event', '$animate', '_' ];
   function monaPopup($rootScope, event, $animate, _){
      var directive = {
         scope      : {
            title              : '@',
            visibilityCondition: '=',
            maxWidth           : '@',
            okButtonLabel      : '@',
            cancelButtonLabel  : '@',
            onDone             : '&',
            onClose            : '&'
         },
         link       : link,
         transclude : true,
         restrict   : 'A',
         templateUrl: 'components/popup.html'
      };
      var popupCounts = 0;
      var allPopupScopes = [];
      registerGlobalListener();
      return directive;

      function registerGlobalListener(){
         $rootScope.$on(event.closeAllPopups, function (){
            angular.forEach(allPopupScopes, function (scope){
               scope.visibilityCondition = false;
            });
         })
      }

      function link(scope, elm, attr){
         angular.element('body').append(elm);
         elm.addClass("mona-popup-container");
         scope.selfHide = selfHidePopup;
         watchForVisibitlitConditionChange();
         if ( angular.isUndefined(attr.locked) ) {
            checkFocusLost();
         }
         setDefaultValues();
         allPopupScopes.push(scope);
         scope.$on("$destroy", cleanUp);
         function setDefaultValues(){
            if(angular.isDefined(attr.noFooter)){
               scope.noFooter = true;
            }
            if ( ! angular.isDefined(attr.okButtonLabel) ) {
               attr.okButtonLabel = "Ok";
            }
            if ( ! angular.isDefined(attr.cancelButtonLabel) ) {
               attr.cancelButtonLabel = "Cancel";
            }
         }

         function checkFocusLost(){
            elm.on("click", function (e){
               if ( elm[ 0 ] === e.target || ! elm[ 0 ].contains(e.target) ) {
                  selfHidePopup();
               }
            });
         }

         function selfHidePopup(){
            scope.$applyAsync(function (){
               scope.visibilityCondition = false;
            });
         }

         function watchForVisibitlitConditionChange(){
            scope.$watch("visibilityCondition", function (newValue, oldValue){
               if ( newValue === true ) {
                  showPopup();
               } else if ( newValue == false && oldValue === true ) {
                  hidePopup();
               }
            });
         }

         function showPopup(){
            popupCounts ++;
            $rootScope.$broadcast("freeze:bg");
            scope.okButtonState = "normal";
            elm.css('z-index', popupCounts * 10000);
            $animate.addClass(elm, "popup-visible");
         }

         function hidePopup(){
            popupCounts = popupCounts < 1 ? 0 : (popupCounts - 1);
            $animate.removeClass(elm, "popup-visible");
            $rootScope.$broadcast("unfreeze:bg");
            if ( angular.isDefined(attr.onClose) ) {
               scope.onClose();
            }
         }

         function cleanUp(){
            allPopupScopes = _.filter(allPopupScopes, function (popup){
               return popup !== scope;
            });
         }
      }
   }
})();