(function (){
   angular.module('app.component').directive('monaFreezable', monaFreezable);

   monaFreezable.$inject = [ '$rootScope' ];
   function monaFreezable($rootScope){
      var directive = {
         link       : link,
         restrict   : 'A',
         templateUrl: ''
      };
      return directive;

      function link(scope, elm, attr){
         var label = attr.monaFreezable;
         var pendingRequests = 0;
         var overlay = angular.element("<div class='freeze-overlay'></div>");
         $rootScope.$on("freeze:" + label, function (){
            pendingRequests ++;
            if ( pendingRequests <= 1 ) {
               elm.addClass("freeze");
               //overlay = angular.element("<div class='freeze-overlay'></div>");
               elm.append(overlay);
            }
         });
         $rootScope.$on("unfreeze:" + label, function (force){
            force = force || false;
            if ( ! force ) {
               pendingRequests --;
               if ( pendingRequests > 0 ) {
                  return;
               }
            }
            pendingRequests = 0;
            elm.removeClass("freeze");
            overlay.remove();
         });
      }
   }
})();