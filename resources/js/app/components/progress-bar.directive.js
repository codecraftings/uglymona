(function(){
	angular.module("app.component")
		.directive("progressBar", progressBar);

	progressBar.$inject = [];
	function progressBar(){
		var dir = {
			restrict: 'E',
			scope: {
				value: '=valueMeter'
			},
			templateUrl: "components/progress-bar.html",
			link: link
		}
		return dir;
		function link(scope, elm, attr){

		}
	}
})();