(function (){
   angular.module('app.component').directive('ccExpDate', ccExpDate);
   
   ccExpDate.$inject = [ '$window','$timeout' ];
   function ccExpDate($window, $timeout){
      var directive = {
         scope      : {},
         link       : link,
         require: 'ngModel',
         restrict   : 'EA',
         templateUrl: ''
      };
      return directive;
      
      function link(scope, elm, attr, ngModel){
         elm.attr('maxlength', 9);
         elm.on('keyup', format);
         elm.on('change', formatYear);
         elm.on('keydown', limit);
         ngModel.$validators.validExpDate = function(date){
            if(typeof date == "undefined"){return false;}
            return date.length===9;
         }
         function formatYear(e){
            //format(e, true);
         }
         function limit(e){
            if ( e.keyCode > 64 && e.keyCode < 91 ) {
               e.preventDefault();
            }
         }
         function format(e, fullYear){
            if( e.keyCode===8|| e.keyCode===46){
               return;
            }
            var val = elm.val();
            val = val.replace(/[A-z\s\\\/\-_]/g, "");
            if( e.keyCode===191&&val.length<2){
               val = ("00" +val).slice(-2);
            }
            var m = val.substr(0, 2);
            m = getValidMonth(m);
            var y = val.substr(2, 4);
            y = getValidYear(y, fullYear);
            if ( val.length > 1 ) {
               elm.val(m + " / " + y);
            } else {
               elm.val(m);
            }
         }
         scope.$on('$destroy', function (){
            elm.off('keydown', limit);
            elm.off('keyup', format);
            elm.off('change', formatYear);
         });
      }
      function getValidMonth(m){
         if( m.length<2){
            return m;
         }
         m = parseInt(m);
         if(m>12){
            m = 12;
         }
         if(m<1){
            m = 1;
         }
         return ("00"+m).slice(-2);
      }
      function getValidYear(y, fullYear){
         if((y===""|| y.length<4)&&!fullYear){
            return y;
         }
         if(fullYear){
            y = "2"+("000"+y).slice(-3);
         }
         y = parseInt(y);
         var thisYear = new $window.Date().getFullYear();
         if(y<thisYear){
            y = thisYear;
         }
         return y;
      }
   }
})();