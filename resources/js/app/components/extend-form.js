(function (){
   angular.module('app.component').directive('extendForm', extendForm);
   
   extendForm.$inject = [];
   function extendForm(){
      var directive = {
         scope      : {},
         link       : link,
         require: 'FormController',
         restrict   : 'EA',
         templateUrl: ''
      };
      return directive;
      
      function link(scope, elm, attr, form){
         form.error = "";
         form.success = "";
         form.hasError = hasError;
         form.getError = getError;

         function hasError(){
            return form.error||form.$invalid;
         }
         function getError(){
            if(form.$invalid){

            }
         }
      }
   }
})();