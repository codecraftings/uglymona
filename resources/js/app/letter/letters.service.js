(function (){
   angular.module('app.letter').service('letters', LettersRepo);
   
   LettersRepo.$inject = ['Repo','Letter'];
   function LettersRepo(Repo, Letter){
      var _this = new Repo("letters", Letter);
      return _this;
   }
})();