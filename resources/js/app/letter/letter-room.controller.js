(function (){
    angular.module('app.letter').controller('LetterRoomController', LetterRoomController);

    LetterRoomController.$inject = ['letterRoom', '$rootScope'];
    function LetterRoomController(letterRoom, $rootScope){
        var vm = this;
        vm.letters = [];
        init();
        function init(){
            vm.letters = letterRoom.getLatest();
        }
    }
})();