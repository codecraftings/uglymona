(function (){
   angular.module('app.letter').factory('Letter', LetterFactory);
   
   LetterFactory.$inject = [ 'Model','User' ];
   function LetterFactory(Model, User){
      function Letter(data){
         var _this = new Model;
         _this.setFields({
            user: {},
            status: "",
            privacy: "",
            message: "",
            reply: "",
            replied_at: "",
            formatted_message: "",
            formatted_reply: ""
         });
         _this.onFieldsDataUpdated = function (){
            _this.user = new User(_this.user);
         };
         _this.setFieldsData(data);
         return _this;
      }
      
      return Letter;
   }
})();