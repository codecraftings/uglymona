(function (){
    angular.module('app.letter').controller('AddLetterController', AddLetterController);

    AddLetterController.$inject = ['Letter','event','$rootScope','currentUser','letters'];
    function AddLetterController(Letter,event, $rootScope, currentUser, letters){
        var vm = this;
        vm.error = "";
        vm.success = "";
        vm.letter = new Letter;
        vm.submit = submit;
        init();

        function init(){
            vm.letter.user_id = currentUser.id;
        }
        function submit(){
            $rootScope.$broadcast(event.showLoader);
            letters.create(vm.letter).$promise.then(function(){
                vm.error = "";
                vm.success = "success";
            }).catch(function(error){
                vm.error = error.message;
            }).finally(function(){
                $rootScope.$broadcast(event.hideLoader);
            })
        }
    }
})();