(function (){
   angular.module('app.letter').config(RouteConfigProvider);

   RouteConfigProvider.$inject = [ '$routeProvider', 'authResolver' ];
   function RouteConfigProvider($routeProvider, authResolver){
      $routeProvider.when('/letter-room', {
         templateUrl : "templates/letter-room.html",
         controller  : "LetterRoomController",
         title       : "Letter Room",
         accessLevel : "user",
         permissions : [ 'letter_room' ],
         resolve     : {
            permissions: authResolver,
            letters    : fetchLettersResolve
         },
         controllerAs: "vm"
      }).when('/letter/add', {
         title       : "Write A Letter",
         templateUrl : "templates/add-letter.html",
         controller  : "AddLetterController",
         controllerAs: "vm",
         accessLevel : "user",
         permissions : [ 'letter_room', 'add_letter' ],
         resolve     : {
            permissions: authResolver
         }
      })
   }

   fetchLettersResolve.$inject = [ 'letterRoom' ]
   function fetchLettersResolve(letterRoom){
      return letterRoom.fetchLatest();
   }
})();