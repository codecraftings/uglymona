(function (){
    angular.module('app.letter').service('letterRoom', LetterRoom);

    LetterRoom.$inject = ['letters'];
    function LetterRoom(letters){
        var _letters = [];
        this.getLatest = getLatest;
        this.fetchLatest = fetchLatest;

        function fetchLatest(){
            _letters = letters.query();
            return _letters.$promise;
        }
        function getLatest(){
            if(_letters.length<1){
                fetchLatest();
            }
            return _letters;
        }
    }
})();