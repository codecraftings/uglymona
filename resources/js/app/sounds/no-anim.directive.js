(function (){
    angular.module('app.component').directive('noAnim', noAnimation);

    noAnimation.$inject = ['$animate'];
    function noAnimation($animate){
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, elm, attr){
            $animate.enabled(elm, false);
        }
    }
})();