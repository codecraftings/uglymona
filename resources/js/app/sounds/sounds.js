var sounds = {};

var _spin;
var _pop;
var path = function(filename){
	return 'sounds/'+filename;
}
var settings = function(){
	return {
		preload: true,
		formats: ["ogg","mp3","aac"]
	}
}
sounds.spin = function(){
	if(typeof _spin=="undefined"){
		_spin = new buzz.sound(path('spin'), settings());
	}
	return _spin;
}
sounds.pop = function(){
	if(typeof _pop=="undefined"){
		_pop = new buzz.sound(path('pop'), settings());
	}
	return _pop;
}
function preload(){
	sounds.pop();
	sounds.spin();
}
preload();