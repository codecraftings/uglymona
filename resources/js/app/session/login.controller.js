(function (){
   angular.module("app.session")
      .controller("LoginController", LoginController);

   LoginController.$inject = [ '$rootScope', 'currentUser', 'api', 'urls', 'auth', '$location', '$route' ];
   function LoginController($rootScope, currentUser, api, urls, auth, $location, $route){
      var vm = this;
      vm.error = "";
      vm.email = "";
      vm.password = "";
      vm.shouldRemember = false;
      vm.submit = submitLogin;
      vm.emailVerifiedNotice = false;
      vm.passwordRetrievalWindow = false;
      vm.passwordResetWindow = false;
      vm.newPassRequest = {};
      vm.submitPassResetRequest = submitPassResetRequest;
      vm.submitConfirmPassReset = submitConfirmPassReset;
      vm.resetAlertNotices = resetAlertNotices;
      init();

      function init(){
         if ( currentUser.isAuthenticated() ) {
            $location.path('/');
         }
         if ( $route.current.params.verified ) {
            vm.emailVerifiedNotice = true;
         }
         if ( $route.current.params.reset_token ) {
            vm.passwordResetWindow = true;
         }
      }

      function resetAlertNotices(){
         console.log("ff");
         $location.path('/login').search("");
      }

      function submitLogin(e){
         e.preventDefault();
         $rootScope.$broadcast("showLoader");
         auth.login(vm.email, vm.password, vm.shouldRemember)
            .then(function (){
               resolveWhereToGoNext();
               vm.error = "";
            })
            .catch(function (error){
               vm.error = error.message;
            })
            .finally(function (){
               $rootScope.$broadcast("hideLoader");
            })
      }

      function resolveWhereToGoNext(){
         if ( currentUser.profile.profile_completion < 50 ) {
            $location.path('profile/build');
            return;
         }
         if ( urls.pending_path ) {
            $location.path(urls.pending_path);
            urls.pending_path = false;
         } else {
            $location.path('/');
         }
      }

      function submitPassResetRequest(){
         var promise = api.post("password/reset", {
            email: vm.passResetEmail
         })
         promise.then(function (response){
            vm.passResetInfo = response.message;
            vm.passResetError = "";
         })
         promise.catch(function (error){
            vm.passResetInfo = "";
            vm.passResetError = error.message;
         });
         return promise;
      }

      function submitConfirmPassReset(){
         vm.newPassRequest.token = $route.current.params.reset_token;
         var promise = api.post("password/reset/confirm", vm.newPassRequest)
         promise.then(function (response){
            vm.passResetInfo = response.message;
            vm.passResetError = "";
         })
         promise.catch(function (error){
            vm.passResetInfo = "";
            vm.passResetError = error.message;
         });
         return promise;
      }
   }
})();