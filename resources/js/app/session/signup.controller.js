(function (){
   angular.module("app.session")
      .controller("SignupController", SignupController);

   SignupController.$inject = [ '$log', '$rootScope', 'currentUser', 'auth', '$location' ];
   function SignupController($log, $rootScope, currentUser, auth, $location){
      var vm = this;
      vm.error = "";
      vm.success = "";
      vm.user = {};
      vm.submit = submit;
      init();

      function init(){
         if ( currentUser.isAuthenticated() ) {
            $location.path('/');
         }
      }

      function formError(form){
         $log.info(form.$error);
         if ( form.$error.required ) {
            if ( form.$error.required[ 0 ].$name == "terms" ) {
               return "You must read and accept terms and condition!";
            }
            return "All fields are required!";
         }
         if ( form.$error.email ) {
            return "Please enter a valid email!";
         }
      }

      function submit(form){
         if ( form.$invalid ) {
            vm.error = formError(form);
            return;
         }
         if ( vm.user.password !== vm.user.password_confirmation ) {
            vm.error = "Password mismatch";
            return;
         }
         $rootScope.$broadcast("showLoader");
         auth.register(vm.user)
            .then(function (){
               vm.error = "";
               vm.success = "Registration Done! You can login now!";
               vm.user = {};
            })
            .catch(function (error){
               vm.error = error.message;
            })
            .finally(function (){
               $rootScope.$broadcast("hideLoader");
            })
      }
   }
})();