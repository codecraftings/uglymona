(function (){
   angular.module('app.session').config(RouteConfigProvider);

   RouteConfigProvider.$inject = [ '$routeProvider', 'authResolver' ];
   function RouteConfigProvider($routeProvider, authResolver){
      $routeProvider.when('/signup', {
         title       : "Signup",
         templateUrl : "templates/signup.html",
         controller  : "SignupController",
         controllerAs: "vm",
         resolve     : {
            permissions: authResolver
         }
      }).when('/login', {
         title       : "Login",
         templateUrl : "templates/login.html",
         controller  : "LoginController",
         controllerAs: "vm",
         resolve     : {
            permissions: authResolver
         }
      }).when('/logout', {
         redirectTo: "/"
      });
   }
})();