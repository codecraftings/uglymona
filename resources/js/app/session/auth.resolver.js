(function (){
   angular.module('app.session')
      .constant('authResolver', authResolver);
   authResolver.$inject = [ '$route', '$q', 'event', 'currentUser', '$rootScope','membership' ];
   function authResolver($route, $q, event, currentUser, $rootScope, membership){
      var df = $q.defer();
      confirmAccessLevel($route.current.accessLevel)
         .then(function (){
            confirmHasPermissions($route.current.permissions)
               .then(df.resolve)
               .catch(df.reject);
         })
         .catch(df.reject);
      return df.promise;

      function confirmAccessLevel(accessLevel){
         var df = $q.defer();
         if ( angular.isUndefined(accessLevel) ) {
            df.resolve();
         }
         switch (accessLevel) {
            case 'user':
               if(currentUser.isAuthenticated()){
                  df.resolve();
               }else{
                  $rootScope.$broadcast(event.TokenInvalidated);
                  df.reject();
               }
               break;
            case 'any':
            case 'public':
            default:
               df.resolve();
               break;
         }
         return df.promise;
      }

      function confirmHasPermissions(permissions){
         var df = $q.defer();
         if ( angular.isUndefined(permissions) ) {
            df.resolve();
         }
         var flag = true;
         angular.forEach(permissions, function (value){
            if ( ! currentUser.hasPermission(value) ) {
               flag = false;
            }
         });
         if ( flag === false ) {
            membership.resolvePermission(permissions);
            df.reject();
         } else {
            df.resolve();
         }
         return df.promise;
      }
   }
})();