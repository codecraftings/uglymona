(function (){
    angular.module("app.session")
        .directive("monaNavbar", monaNavbar);
    monaNavbar.$inject = [ 'currentUser', 'auth', '$location','event', '$rootScope','inbox' ];
    function monaNavbar(currentUser, auth, $location,event, $rootScope, inbox){
        var dir = {
            restrict: 'A',
            link: link
        };
        return dir;

        function link(scope, elm, attr){
            scope.userMenuVisible = false;
            scope.roomsMenuVisible = false;
            scope.mobileMenuVisible = false;
            scope.user = currentUser;
            scope.logout = logout;
            scope.showInbox = showInbox;
            scope.inbox = inbox;
            if(currentUser.isAuthenticated()){
                inbox.countUnreadMessages();
            }
            angular.element('body').on('click', checkFocusLost);
            scope.$on("$destroy", destroyScope);
            $rootScope.$on("$routeChangeSuccess", hideAllMenu);
            var userHeader = angular.element("#current-user-header");
            var mobileMenu = angular.element("#mobile-menu-container");
            var roomsMenu = angular.element("#rooms-menu");

            function hideAllMenu(){
                scope.userMenuVisible = false;
                scope.roomsMenuVisible = false;
                scope.mobileMenuVisible = false;
            }

            function checkFocusLost(e){
                if ( userHeader[ 0 ] !== e.target && ! userHeader[ 0 ].contains(e.target) ) {
                    safeUpdate('userMenuVisible', false);
                }
                if ( roomsMenu[ 0 ] !== e.target && ! roomsMenu[ 0 ].contains(e.target) ) {
                    safeUpdate('roomsMenuVisible', false);
                }
                if ( mobileMenu[ 0 ] !== e.target && ! mobileMenu[ 0 ].contains(e.target) ) {
                    safeUpdate('mobileMenuVisible', false);
                }
            }

            function safeUpdate(key, value){
                scope.$applyAsync(function (){
                    scope[ key ] = value;
                });
            }

            function destroyScope(){
                angular.element('body').off('click', checkFocusLost);
            }
            function showInbox(){
                $rootScope.$broadcast(event.showInbox);
            }
            function logout(){
                $rootScope.$broadcast(event.showLoader);
                auth.logout().finally(function (){
                    $location.path("/");
                    $rootScope.$broadcast(event.hideLoader);
                })
            }
        }
    }
})();