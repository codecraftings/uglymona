(function (){
   angular.module('app.session').factory('currentUser', CurrentUserFactory);

   CurrentUserFactory.$inject = [ '$cookies', '$window', 'User' ];
   function CurrentUserFactory($cookies, $window, User){
      function CurrentUser(data){
         var _this = new User;
         _this.setFields({
            shouldRemember: false,
            permissions      : [],
            access_token     : null,
            token_expire_time: 0
         });
         _this.onDataUpdated(function (){

         });
         _this.setFieldsData(data);

         _this.hasPermission = hasPermission;
         _this.syncFromLocal = syncFromLocal;
         _this.syncToLocal = syncToLocal;
         _this.isAuthenticated = isAuthenticated;
         _this.tokenExpired = tokenExpired;
         _this.getToken = getToken;
         _this.setAccessToken = setAccessToken;
         _this.setData = setData;
         _this.logout = logout;
         _this.getDateOfBirth = getDateOfBirth;
         return _this;

         function isAuthenticated(){
            return _this.access_token !== null;
         }
         function getDateOfBirth(){
            if(_this.profile.date_of_birth.date){
               return _this.profile.date_of_birth.date;
            }else{
               return _this.profile.date_of_birth;
            }
         }
         function tokenExpired(){
            var now = Math.floor($window.Date.now() / 1000);
            if ( (now+2) >= _this.token_expire_time ) {
               return true;
            } else {
               return false;
            }
         }
         function getToken(){
            return _this.access_token;
         }
         function setAccessToken(token, expire_time){
            _this.access_token = token;
            _this.token_expire_time = expire_time;
         }
         function setData(data){
            //delete data['access_token'];
            //delete data['token_expire_time'];
            _this.setFieldsData(data);
         }
         function syncFromLocal(){
            var storedObj = $cookies.getObject("currentUser")
            if ( angular.isDefined(storedObj) ) {
               _this.setFieldsData(storedObj);
            }
         }

         function syncToLocal(shouldRemember){
            if(angular.isDefined(shouldRemember)){
               _this.shouldRemember = shouldRemember;
            }else{
               shouldRemember = _this.shouldRemember;
            }
            var expires = 0;
            if(shouldRemember){
               expires = new $window.Date();
               expires.setMinutes(60*24*14);
            }
            $cookies.putObject("currentUser", _this, {
               expires: expires
            });
         }

         function hasPermission(type){
            var hasPermission = false;
            angular.forEach(_this.permissions, function (value){
               if ( value === type ) {
                  hasPermission = true;
               }
            });
            return hasPermission;
         }

         function logout(){
            _this.setFieldsData({
               id               : null,
               username         : null,
               email            : null,
               permissions      : [],
               access_token     : null,
               token_expire_time: 0
            });
         }
      }

      var user = new CurrentUser();
      user.syncFromLocal();
      return user;
   }
})();