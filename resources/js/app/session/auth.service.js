(function (){
   angular.module("app.session")
      .service("auth", AuthService);

   AuthService.$inject = [ '$q', 'urls', 'currentUser', '$http', '$rootScope', 'event', '_parser', '$timeout' ];
   function AuthService($q, urls, currentUser, $http, $rootScope, event, _parser, $timeout){
      this.register = register;
      this.login = login;
      this.logout = logout;
      this.resolveToken = resolveToken;
      this.user = currentUser;

      function resolveToken(){
         var df = $q.defer();
         if ( currentUser.tokenExpired() ) {
            getRefreshTokenThrottled().then(df.resolve)
               .catch(function (error){
                  $rootScope.$broadcast(event.TokenInvalidated);
                  df.reject(error);
               });
         } else {
            df.resolve(currentUser.getToken());
         }
         return df.promise;
      }

      var refreshTokenPromise = null;

      function getRefreshTokenThrottled(){
         if ( refreshTokenPromise !== null ) {
            return refreshTokenPromise;
         }
         refreshTokenPromise = getRefreshToken();
         refreshTokenPromise.finally(function (){
            $timeout(function (){
               refreshTokenPromise = null;
            }, 1000);
         });
         return refreshTokenPromise;
      }

      function getRefreshToken(){
         var df = $q.defer();
         var token = currentUser.getToken();
         $http.post(urls.api('token/refresh'), { token: token })
            .success(function (response){
               if ( angular.isDefined(response.access_token) ) {
                  currentUser.setAccessToken(response.access_token, response.token_expire_time);
                  currentUser.syncToLocal();
                  df.resolve(currentUser.getToken());
               } else {
                  df.reject();
               }
            }).error(df.reject);
         return df.promise;
      }

      function logout(){
         var df = $q.defer();
         if ( ! currentUser.isAuthenticated() ) {
            return $q.resolve({});
         }
         $http.post(urls.api("logout"), { token: currentUser.getToken() }).finally(function (){
            currentUser.logout();
            currentUser.syncToLocal();
            $rootScope.$broadcast(event.UserLoggedOut);
            df.resolve();
         });
         return df.promise;
      }

      function register(user){
         var df = $q.defer();
         $http.post(urls.api("signup"), user)
            .success(function (response){
               $rootScope.$broadcast(event.UserRegistered);
               df.resolve();
            }).error(function (response){
               df.reject(_parser.error(response));
            });
         return df.promise;
      }

      function login(email, password, shouldRemember){
         var df = $q.defer();
         $http.post(urls.api("login"), {
            email   : email,
            password: password
         }).success(function (response){
            console.log(response);
            if ( angular.isDefined(response.access_token) ) {
               currentUser.setAccessToken(response.access_token, response.token_expire_time);
               if ( angular.isDefined(response.user) ) {
                  currentUser.setFieldsData(response.user);
               }
               currentUser.syncToLocal(shouldRemember);
               $rootScope.$broadcast(event.UserLoggedIn);
               df.resolve();
            } else {
               df.reject("Unknown error!");
            }
         }).error(function (response){
            df.reject(_parser.error(response));
         })
         return df.promise;
      }
   }
})();