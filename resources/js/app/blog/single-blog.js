(function (){
   angular.module('app.blog').controller('SingleBlogController', SingleBlogController);
   
   SingleBlogController.$inject = ['blogData','categories', '$rootScope'];
   function SingleBlogController(blogData,categories, $rootScope){
      var vm = this;
      vm.blog = blogData;
      vm.categories = categories;
      vm.currentCategory = vm.blog.category.slag;
      $rootScope.pageTitle = vm.blog.title + " | Uglymona";
   }
})();