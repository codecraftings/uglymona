(function (){
   angular.module('app.blog').config(RouteConfigProvider);

   RouteConfigProvider.$inject = [ '$routeProvider', 'authResolver' ];
   function RouteConfigProvider($routeProvider, authResolver){
      var blogListParams = {
         templateUrl : "templates/blogs.html",
         controller  : "BlogController",
         title       : "Blogs",
         controllerAs: "vm",
         accessLevel : "public",
         resolve     : {
            permissions: authResolver,
            latestBlogs: resolveLatestBlogs,
            categories : resolveBlogCategories
         }
      };
      var singleBlogParams = {
         templateUrl : "templates/single-blog.html",
         controller  : "SingleBlogController",
         title       : "Blogs",
         controllerAs: "vm",
         accessLevel : "public",
         resolve     : {
            permissions: authResolver,
            blogData   : resolveBlogData,
            categories : resolveBlogCategories
         }
      };
      $routeProvider
         .when('/blogs/category/:category', blogListParams)
         .when('/blogs/:id/:title', singleBlogParams)
         .when('/blogs', blogListParams);
   }

   resolveBlogCategories.$inject = [ 'blogs' ];
   function resolveBlogCategories(blogs){
      return blogs.getCategories();
   }

   resolveLatestBlogs.$inject = [ 'blogs', '$route' ]
   function resolveLatestBlogs(blogs, $route){
      var q = {};
      if ( $route.current.params.category ) {
         q.category = $route.current.params.category;
      }
      var _list = blogs.query(q);
      return _list.$promise;
   }

   resolveBlogData.$inject = [ 'blogs', '$route' ];
   function resolveBlogData(blogs, $route){
      var _blog = blogs.get($route.current.params.id);
      return _blog.$promise;
   }
})();