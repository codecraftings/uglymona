(function (){
   angular.module("app.blog")
      .controller("BlogController", BlogController);
   BlogController.$inject = [ 'latestBlogs', 'categories', '$routeParams' ];
   function BlogController(latestBlogs, categories, $routeParams){
      var vm = this;
      vm.blogs = [];
      vm.categories = [];
      init();

      function init(){
         vm.blogs = latestBlogs;
         vm.currentCategory = $routeParams.category;
         vm.categories = categories;
      }
   }
})();