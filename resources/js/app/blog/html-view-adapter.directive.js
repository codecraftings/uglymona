(function (){
   angular.module('app.blog').directive('htmlViewAdapter', htmlViewAdapter);
   
   htmlViewAdapter.$inject = [];
   function htmlViewAdapter(){
      var directive = {
         scope      : {},
         link       : link,
         restrict   : 'EA',
         templateUrl: ''
      };
      return directive;
      
      function link(scope, elm, attr){
         elm.on("load", function(){
            console.log("loaded!");
            elm.css("height", elm[0 ].contentWindow.document.body.scrollHeight+100+"px");
         });
         //elm.on("message", function(data){
         //   console.log(data);
         //});
      }
   }
})();