(function (){
   angular.module('app.blog').factory('Blog', BlogFactory);
   
   BlogFactory.$inject = [ 'Model' ];
   function BlogFactory(Model){
      function Blog(data){
         var _this = new Model;
         _this.setFields({
            title: "",
            content: "",
            author_name: "",
            post_data: "",
            is_pdf: false,
            pdf_link: "",
            permalink: ""
         });
         _this.onDataUpdated(function (){
         });
         _this.setFieldsData(data);
         return _this;
      }
      
      return Blog;
   }
})();