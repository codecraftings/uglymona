(function (){
   angular.module('app.blog').service('blogs', BlogsService);
   
   BlogsService.$inject = ['Repo',"Blog", 'api', '$q'];
   function BlogsService(Repo, Blog, api, $q){
      var _this = new Repo("blogs", Blog);
      _this.getCategories = getCategories;
      return _this;
      function getCategories(){
         var df = $q.defer();
         api.get('categories').then(function(response){
            df.resolve(response.data);
         }).catch(df.reject);
         return df.promise;
      }
   }
})();