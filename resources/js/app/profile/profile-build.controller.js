(function (){
    angular.module("app.profile")
        .controller("ProfileBuildController", ProfileBuildController);

    ProfileBuildController.$inject = [
        '$rootScope',
        '$scope',
        '$timeout',
        'currentUser',
        'profileFields',
        'profileService',
        '$q',
        '$location',
        'auth'
    ];
    function ProfileBuildController($rootScope, $scope, $timeout, currentUser, profileFields, profileService, $q, $location, auth){
        var vm = this;
        vm.error = "";
        vm.fields = getProfileFields();
        vm.user = angular.copy(currentUser);
        vm.user.profile.date_of_birth = currentUser.getDateOfBirth();
        vm.profileProgress = 0;
        vm.currentStep = 1;
        vm.resizeImagePopup = false;
        vm.selectedImage = "";
        vm.resizedImage = "";
        vm.finalImage = "";
        vm.newImageChosen = false;
        vm.handleCropingDone = handleCropingDone;
        vm.handleCropingCancel = handleCropingCancel;
        vm.gotoStep = gotoStep;
        vm.submit = submit;
        init();
        function init(){
            if ( vm.user.profile.profile_pic ) {
                vm.finalImage = vm.user.profile.profile_pic;
            }
            $scope.$watch("vm.selectedImage", function (){
                if ( angular.isDefined(vm.selectedImage) && vm.selectedImage ) {
                    vm.resizeImagePopup = true;
                }
            });
        }

        function submit(){
            var df = $q.defer();
            vm.error = "";
            $rootScope.$broadcast("showLoader");
            updateProfile().then(function (user){
                vm.user = user;
                currentUser.setData(user);
                currentUser.syncToLocal();
                vm.gotoStep(7);
                vm.profileProgress = 120;
                $timeout(function(){
                    $location.path("profile-wheel");
                }, 2000);
                df.resolve();
            }).catch(function (error){
                vm.error = error;
                df.reject();
            }).finally(function (){
                $rootScope.$broadcast("hideLoader");
            })
            return df.promise;
        }

        function updateProfile(){
            var df = $q.defer();
            if ( vm.newImageChosen ) {
                profileService.UpdateProfileWithNewProfilePic(vm.finalImage, vm.user)
                    .then(df.resolve)
                    .catch(df.reject);
            } else {
                profileService.updateProfile(vm.user)
                    .then(df.resolve)
                    .catch(df.reject);
            }
            return df.promise;
        }

        function hideStep(num){
            var panel = angular.element('.build-step-' + num);
            panel.css({ 'position': 'absolute' });
            panel.removeClass("magictime spaceInRight").addClass("magictime spaceOutLeft");
        }

        function showStep(num){
            var panel = angular.element('.build-step-' + num);
            panel.removeClass("hidden").addClass("magictime spaceInRight");
        }

        function gotoStep(stepNum){
            hideStep(stepNum - 1);
            showStep(stepNum);
            vm.currentStep = stepNum;
            vm.profileProgress = (vm.currentStep - 1) * 18;
        };

        function handleCropingDone(){
            vm.resizeImagePopup = false;
            vm.finalImage = vm.resizedImage;
            vm.newImageChosen = true;
        }

        function handleCropingCancel(){
            vm.selectedImage = "";
        }

        function getProfileFields(){
            var fields = {};
            angular.forEach(profileFields, function (field){
                fields[ field.key ] = field;
            });
            return fields;
        }
    }
})();