(function (){
   angular.module('app.profile').config(RouteConfigProvider);

   RouteConfigProvider.$inject = [ '$routeProvider', 'authResolver' ];
   function RouteConfigProvider($routeProvider, authResolver){
      $routeProvider.when('/profile/build', {
         title       : "Build Your Profile",
         templateUrl : "templates/profile-build.html",
         controller  : "ProfileBuildController",
         controllerAs: "vm",
         accessLevel : 'user',
         resolve     : {
            permisssions: authResolver
         }
      })
         .when('/profile/upgrade', {
            title       : "Unlock Full Access",
            templateUrl : "templates/upgrade-profile.html",
            controller  : "UpgradeProfileController",
            controllerAs: "vm",
            accessLevel : 'user',
            resolve     : {
               permissions: authResolver
            }
         })
   }
})();