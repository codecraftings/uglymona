(function(){
	angular.module("app.profile")
		.directive("monaSelect", monaSelect);
	monaSelect.$inject = [];
	function monaSelect(){
		var dir = {
			restrict: "E",
			scope: {
				label: '@monaLabel',
				model: '=monaModel',
				options: '=monaOptions'
			},
			templateUrl: "components/mona-select.html",
			link: link
		};
		return dir;

		function link(scope, elm, attr){
		   	scope.toggleDropdown = function(){
			   if(scope.dropdownOpen){
				  scope.hideDropdown();
			   }else{
				  scope.openDropdown();
			   }
			}
			scope.openDropdown = function(){
				scope.dropdownOpen = true;
				angular.element('body').on('click', checkFocusLost);
			}
			scope.hideDropdown = function(){
				scope.dropdownOpen = false;
				angular.element('body').off('click', checkFocusLost);
			}
			scope.setSelected = function(item){
				scope.model = item.value;
				scope.selected = item;
				scope.hideDropdown();
			}
			angular.forEach(scope.options, function(opt){
				if(opt.value===scope.model){
					scope.setSelected(opt)
				}
			});
			function checkFocusLost(e){
				if(elm[0]!==e.target&&!elm[0].contains(e.target)){
					scope.$applyAsync(function(){
						scope.hideDropdown();
					});
				}
			}
		}
	}
})();