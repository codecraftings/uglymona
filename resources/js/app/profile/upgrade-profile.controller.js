(function (){
   angular.module('app.profile').controller('UpgradeProfileController', UpgradeProfileController);
   
   UpgradeProfileController.$inject = [ 'stripe','config', '$q', 'api', 'currentUser','$rootScope','$location','event','membership' ];
   function UpgradeProfileController(stripe, config, $q, api, currentUser, $rootScope, $location, event, membership){
      var vm = this;
      vm.submit = submit;
      vm.card = {};
      vm.subscriptionFee = config.get("subscription_fee");
      console.log(currentUser);
      if(currentUser.membership.order>=membership.full.order){
         redirect();
      }
      function submit(){
         var df = $q.defer();
         stripe.getToken(vm.card.number, vm.card.cvc, vm.card.expDate.split(" / ")[ 0 ], vm.card.expDate.split(" / ")[ 1 ])
            .then(function (cardToken){
               api.post("profile/upgrade", {
                  card_token: cardToken,
                  last_four: vm.card.number.slice(-4)
               })
                  .then(function (response){
                     if ( response.user ) {
                        currentUser.setData(response.user);
                        currentUser.syncToLocal();
                        df.resolve("You have upgraded to full membership!");
                        showSuccessAlertBox();
                     }else{
                        df.reject("Something went wrong");
                     }
                  }).catch(function(error){
                     df.reject(error.message);
                  });
            })
            .catch(df.reject);
         return df.promise;
      }

      function redirect(){
         $location.path('/').search("").replace();
      }

      function showSuccessAlertBox(){
         $rootScope.$broadcast(event.showAlert, {
            title: "Payment Succeed!",
            message: "You are a full member now! you can access any pages!",
            onClose: function(){
               redirect();
            }
         });
      }
   }
})();