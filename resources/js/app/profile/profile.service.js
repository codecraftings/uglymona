(function (){
   angular.module('app.profile').service('profileService', ProfileService);

   ProfileService.$inject = [ 'photos', '$q', 'users' ];
   function ProfileService(photos, $q, users){
      this.UpdateProfileWithNewProfilePic = UpdateProfileWithNewProfilePic;
      this.updateProfile = updateProfile;

      function updateProfile(user){
         var df = $q.defer();
         var _user = users.update(user);
         _user.$promise.then(function (){
            df.resolve(_user);
         }).catch(function (error){
            df.reject(error.message);
         });
         return df.promise;
      }

      function UpdateProfileWithNewProfilePic(newImageData, user){
         var df = $q.defer();
         photos.upload(newImageData, user.id, "profile_pic").then(function (photo){
            user.profile.profile_photo_id = photo.id;
            var _user = users.update(user);
            _user.$promise.then(function (){
               df.resolve(_user);
            }).catch(function (error){
               df.reject(error.message);
            });
         }).catch(function (error){
            df.reject(error);
         })
         return df.promise;
      }
   }
})();