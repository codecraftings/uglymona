(function (){
   angular.module('app.profile')
      .factory('membership', membershipFactory);

   membershipFactory.$inject = [ '$location', 'currentUser', '$rootScope', 'event', '$timeout', 'urls' ]
   function membershipFactory($location, currentUser, $rootScope, event, $timeout, urls){
      var config = {
         none : {
            order: 0,
            label: 'public'
         },
         basic: {
            order: 1,
            label: "basic"
         },
         full : {
            order: 2,
            label: "full"
         },
         admin: {
            order: 3,
            label: "admin"
         }
      };
      var membership = angular.extend({}, config);
      membership.resolvePermission = resolvePermission;
      return membership;

      function resolvePermission(permissions){
         if ( currentUser.isAuthenticated() ) {
            urls.pending_path = $location.path();
            $timeout(function (){
               $location.path('profile/upgrade');
               $rootScope.$broadcast(event.hideLoader, true);
            }, 0)
         } else {
            $rootScope.$broadcast(event.TokenInvalidated);
         }
         //var order = getLowestMembershipOrder(permission);
      }

      //function getLowestMembershipOrder(permission){
      //   var m;
      //   for ( var membership in config ) {
      //      if ( in_array(membership.permissions, permission) ) {
      //         m = membership;
      //         break;
      //      }
      //   }
      //   if ( m ) {
      //      return m.order;
      //   } else {
      //      return 0;
      //   }
      //}
      //
      //function in_array(arr, item){
      //   for ( var i in arr ) {
      //      if ( i === item ) {
      //         return true;
      //      }
      //   }
      //}
   };
})();