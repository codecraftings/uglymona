(function (){
   angular.module("app.profile")
      .directive("bindToFileInput", bindToFileInput)
      .directive("bindToButton", bindToButton)
      .directive("bindFileData", bindFileData)

   bindToFileInput.$inject = [ '$rootScope' ];
   function bindToFileInput($rootScope){
      var dir = {
         restrict: 'A',
         link    : link
      };
      return dir;

      function link(scope, elm, attr){
         elm.on('click', manageClick);
         scope.$on("$destroy", function (){
            elm.off('click', manageClick);
         });
         function manageClick(event){
            $rootScope.$broadcast("triggerClick:" + attr.bindToFileInput);
         }
      }
   }

   bindToButton.$inject = [ '$rootScope' ];
   function bindToButton($rootScope){
      var dir = {
         restrict: 'A',
         link    : link
      };
      return dir;

      function link(scope, elm, attr){
         var listener = $rootScope.$on("triggerClick:" + attr.bindToButton, triggerClick);
         scope.$on("$destroy", function (){
            listener();
         });
         function triggerClick(){
            elm.click();
         }
      }
   }

   bindFileData.$inject = [ '$rootScope' ];
   function bindFileData($rootScope){
      var dir = {
         restrict: 'A',
         scope   : {
            model: '=bindFileData'
         },
         link    : link
      };
      return dir;

      function link(scope, elm, attr){
         elm.on("change", HandleFileSelected);

         scope.$on("$destroy", function (){
            elm.off("change", HandleFileSelected)
         });
         function HandleFileSelected(e){
            if ( e.currentTarget.files.length < 1 ) {
               return;
            }
            var file = e.currentTarget.files[ 0 ];
            if(angular.isDefined(attr.fileObject)){
               setModelValue(file);
               return;
            }
            var reader = new FileReader();
            reader.onload = function (event){
               setModelValue(event.target.result);
            }
            if ( angular.isDefined(attr.binaryData) ) {
               reader.readAsBinaryString(file);
            } else {
               reader.readAsDataURL(file);
            }

            function setModelValue(data){
               $rootScope.$apply(function (){
                  scope.model = data;
               });
            }
         }
      }
   }
})();