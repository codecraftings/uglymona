(function (){
    angular.module('app', ['app.core','app.blog','app.quest-bar', 'app.profile','app.letter', 'app.video', 'app.user', 'app.inbox', 'app.wheel']);
})();