(function(){
	angular.module("app.photo")
		.service("photos", PhotoService);

	PhotoService.$inject = ['Photo','Repo', '$q'];
	function PhotoService(Photo, Repo, $q){
	   	var _this = new Repo("photos", Photo);
		_this.upload = upload;

	   return _this;
		function upload(imageData, user_id, type){
			if(angular.isUndefined(type)){
				type = "album_photo";
			}
			var df = $q.defer();
			var photo = new Photo;
			photo.source = imageData;
			photo.user_id = user_id;
			photo.type = type;
			var _photo = _this.action("/","post",null,photo);
			_photo.$promise.then(function (){
				df.resolve(_photo);
			}).catch(function (error){
				df.reject(error.message);
			});
			return df.promise;
		}
	}
})();