(function (){
   angular.module('app.photo').factory('Photo', PhotoFactory);
   
   PhotoFactory.$inject = [ 'Model',"User" ];
   function PhotoFactory(Model, User){
      function Photo(data){
         var _this = new Model;
         _this.setFields({
            user_id : "",
            user: {},
            type: "",
            source: "",
            url: {original: ""}
         });
         _this.onFieldsDataUpdated = function (){
            _this.user = new User(_this.user);
         };
         _this.setFieldsData(data);
         _this.getUrl = getUrl;
         return _this;
         function getUrl(type){
            if(typeof type ==='undefined'){
               return _this.url.original;
            }else{
               return _this.url[type];
            }
         }
      }
      
      return Photo;
   }
})();