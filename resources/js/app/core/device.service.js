(function (){
   angular.module('app.core').service('device', DeviceService);
   
   DeviceService.$inject = ['$window','$location','$timeout', 'event','$rootScope'];
   function DeviceService($window, $location, $timeout, event, $rootScope){
      this.width = $window.innerWidth;
      var rotationEvent = ("onorientationchange" in $window)?"orientationchange":"resize";
      $window.addEventListener(rotationEvent, function(){
         //return;
         $location.url('/');
         $rootScope.$broadcast(event.showLoader);
         $timeout(function(){
            $window.location.reload();
            //$rootScope.$broadcast(event.hideLoader);
         }, 1000);
      }, false);
   }
})();