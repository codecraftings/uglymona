(function (){
   angular.module('app.core').service('stripe', StripeService);
   
   StripeService.$inject = [ 'loader', '$q', 'config' ];
   function StripeService(loader, $q, config){
      var _this = this;
      _this.getToken = wrap(getToken);
      var _initialized = false;

      function wrap(fn){
         var wrapper = function (){
            var args = arguments;
            var df = $q.defer();
            init().then(function (){
               fn.apply(this, args)
                  .then(df.resolve)
                  .catch(df.reject)
            }).catch(df.reject);
            return df.promise;
         }
         return wrapper;
      }

      function getToken(ccNumber, ccCVC, expMonth, expYear){
         var df = $q.defer();
         Stripe.card.createToken({
            number   : ccNumber,
            cvc      : ccCVC,
            exp_month: expMonth,
            exp_year : expYear
         }, function (status, response){
            if ( response.error ) {
               df.reject(response.error.message);
            } else {
               df.resolve(response.id);
            }
         });
         return df.promise;
      }

      function init(){
         if ( _initialized ) {
            return $q.resolve({});
         }
         return loadStripeScript()
            .then(function (){
               Stripe.setPublishableKey(config.get('strip_key'));
               _initialized = true;
            })
      }

      function loadStripeScript(){
         if ( typeof Stripe == "undefined" ) {
            return loader.loadScript('https://js.stripe.com/v2/');
         } else {
            return $q.resolve({});
         }
      }
   }
})();