(function (){
   angular.module('app.core')
      .run(registerHelpers);
   registerHelpers.$inject = [ '$rootScope', 'event' ];
   function registerHelpers($rootScope, event){
      var helpers = {};
      $rootScope.helper = helpers;
      helpers.longCall = longCall;

      function longCall(fn, vm){
         $rootScope.$broadcast(event.showLoader);
         var _this = this;
         if ( typeof vm !== "undefined" ) {
            _this = vm;
         }
         _this.success = "";
         _this.error = "";
         return fn.apply(_this, arguments)
            .then(function (response){
               if ( typeof response === "string" ) {
                  _this.error = "";
                  _this.success = response;
               }
            })
            .catch(function (error){
               if ( typeof error === "string" ) {
                  _this.success = "";
                  _this.error = error;
               }
            })
            .finally(function (){
               $rootScope.$broadcast(event.hideLoader);
            })
      }
   }
})();