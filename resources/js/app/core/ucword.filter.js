(function (){
   angular.module('app.core')
      .filter('ucword', ucWordFilter);

   ucWordFilter.$inject = ['$log'];

   function ucWordFilter($log){
      return function(input){
         var words = input.split(" ");
         var output = [];
         angular.forEach(words, function(val, i){
            val = val.split("");
            val[0] = val[0].toUpperCase();
            output[i] = val.join("");
         });
         return output.join(" ");
      }
   }
})();