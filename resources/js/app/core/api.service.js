(function (){
   angular.module('app.core').service('api', ApiService);
   
   ApiService.$inject = [ 'urls', '$q', '$http', '_parser', 'auth' ];
   function ApiService(urls, $q, $http, _parser, auth){
      var _this = this;
      _this.get = get;
      _this.post = post;
      _this.call = call;
      function get(path, params){
         return call(path, 'get', params, null);
      }

      function post(path, data){
         return call(path, 'post', null, data);
      }

      function call(path, method, params, data){
         var df = $q.defer();
         var request = {};
         request.headers = {};
         request.method = method;
         request.url = _getUrl(path);
         if ( typeof params !== "undefined" && params !== null ) {
            request.params = params;
         }
         if ( typeof data !== "undefined" && data !== null ) {
            request.data = data;
         }
         auth.resolveToken().then(function (access_token){
            request.headers.Authorization = "Bearer " + access_token;
            $http(request).success(function (response){
               df.resolve(_parser.response(response));
            }).error(function (response){
               df.reject(_parser.error(response));
            });
         })
            .catch(function (error){
               df.reject(error);
            })
         return df.promise;
      }

      function _getUrl(path){
         return urls.api(path);
      }
   }
})();