(function (){
   angular.module('app.core').config(['$cookiesProvider', function($cookiesProvider){
      $cookiesProvider.defaults.expires = 0;
   }]);
})();