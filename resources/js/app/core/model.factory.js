(function (){
   angular.module('app.core').factory('Model', ModelFactory);
   
   ModelFactory.$inject = [];
   function ModelFactory(){
      function Model(){
         var _this = this;
         _this.id = 0;
         _this.setFields = setFields;
         _this.setFieldsData = setFieldsData;
         _this.onFieldsDataUpdated = onFieldsDataUpdated;
         var _dataUpdatedCallbacks = [];
         _this.onDataUpdated = registerDataUpdatedCallback;
         return _this;

         function setFields(fields){
            merge(_this, fields);
         }

         function setFieldsData(attributes){
            merge(_this, attributes);
            _this.onFieldsDataUpdated();
         }

         function merge(dst, data){
            if ( ! angular.isObject(dst) || ! angular.isObject(data) ) {
               return;
            }
            var h = dst.$$hashKey;
            var keys = Object.keys(data);
            for ( var i = 0, n = keys.length; i < n; i ++ ) {
               var key = keys[ i ];
               var src = data[ key ];
               if ( angular.isFunction(src) ) {
                  continue;
               }
               dst[ key ] = src;
            }
            dst.$$hashKey = h;
         }

         function onFieldsDataUpdated(){
            var len = _dataUpdatedCallbacks.length;
            for ( var i = 0; i < len; i ++ ) {
               _dataUpdatedCallbacks[ i ].call();
            }
         }

         function registerDataUpdatedCallback(fn){
            if ( typeof fn === "function" ) {
               _dataUpdatedCallbacks.push(fn);
            }
         }
      }

      return Model;
   }
})();