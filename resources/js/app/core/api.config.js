(function () {
    angular.module('app.core').config(['$httpProvider', function($httpProvider){
        $httpProvider.defaults.useXDomain = true;
        $httpProvider.interceptors.push(httpServiceInterceptor);
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }]);

    httpServiceInterceptor.$inject = ['currentUser','$q','event', '$rootScope'];
    function httpServiceInterceptor(currentUser, $q, event,$rootScope){
        return {
            request: function(config){
                return config;
            },
            'responseError': function(response){
                if(response.status==401){
                    $rootScope.$broadcast(event.TokenInvalidated);
                }
                return $q.reject(response);
            }
        }
    }
})();