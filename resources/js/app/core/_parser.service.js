(function (){
   angular.module('app.core').service('_parser', ParserService);
   
   ParserService.$inject = [];
   function ParserService(){
      var _this = this;
      _this.response = parseResponse;
      _this.error = parseError;


      function parseResponse(response, ExpectedType, isArray){
         if ( typeof ExpectedType === "undefined" ) {
            return response;
         }
         var data = response.data;
         var parsedData;
         if ( isArray ) {
            parsedData = [];
            var len = data.length;
            for ( var i = 0; i < len; i ++ ) {
               parsedData[ i ] = new ExpectedType(data[ i ]);
            }
         } else {
            parsedData = new ExpectedType(data);
         }
         return parsedData;
      }

      function parseError(response){
         if ( typeof response === "undefined" || response === null || response === "" ) {
            return { code: 500, message: "No response from server" };
         }
         if ( typeof response.error !== "undefined" ) {
            return response.error;
         } else {
            return { code: response.status, message: response.statusText };
         }
      }
   }
})();