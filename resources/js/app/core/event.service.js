(function () {
    angular.module('app.core').factory('event', EventFactory);

    EventFactory.$inject = [];
    function EventFactory() {
        var event = {};
        event.TokenInvalidated = "Api.TokenInvalidated";
        event.UserLoggedIn = "Api.UserLoggedIn";
        event.UserLoggedOut = "Api.UserLoggedOut";
        event.UserRegistered = "Api.UserRegistered";

        event.routeChangeStart = "$routeChangeStart";
        event.routeChangeSuccess = "$routeChangeSuccess";
        event.routeChangeError = "$routeChangeError";

        event.showLoader = "showLoader";
        event.hideLoader = "hideLoader";

        event.showAlert = "showAlert";

        event.newNotification = "notification:new";
        event.newMessageReceived = "message:received";
        event.newMessage = "inbox:new";
        event.showInbox = "inbox:show";
        event.hideInbox = "inbox:hide";

        event.closeAllPopups = "popup:closeAll";
        return event;
    }
})();