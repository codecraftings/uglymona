(function (){
   angular.module('app.core').service('socket', SocketService);
   
   SocketService.$inject = [ 'urls', 'currentUser', '$q', '$rootScope', 'event', 'auth', 'loader' ];
   function SocketService(urls, currentUser, $q, $rootScope, event, auth, loader){
      var self = this;
      self.isConnected = false;
      self.connect = connect;
      self.disconnect = disconnect;
      var _pusher = null;
      var channel = null;
      self.connection = _pusher;
      self.channel = channel;
      $rootScope.$on(event.UserLoggedIn, self.connect);
      $rootScope.$on(event.UserLoggedOut, self.disconnect);

      function connect(){
         if ( ! currentUser.isAuthenticated() ) {
            return;
         }
         initPusher().then(function (){
            channel = _pusher.subscribe("private-user-" + currentUser.id);
            channel.bind("notification", handleNewNotification);
            channel.bind("Mona\\Core\\Inbox\\NewMessageReceived", handleNewMessage);
            self.isConnected = true;
         })
      }

      function handleNewMessage(data){
         console.log(data);
         $rootScope.$broadcast(event.newMessageReceived, data);
      }

      function handleNewNotification(data){
         console.log(data);
         $rootScope.$broadcast(event.newNotification, data);
      }

      function disconnect(){
         if ( _pusher !== null ) {
            _pusher.disconnect();
            _pusher = null;
         }
         self.isConnected = false;
      }

      function initPusher(){
         var df = $q.defer();
         if ( _pusher === null ) {
            loader.loadScript('http://js.pusher.com/3.0/pusher.min.js', 'Pusher').then(function (){
               auth.resolveToken().then(function (token){
                  _pusher = new Pusher('6d72fed798e9bbfdcb09', {
                     authEndpoint: urls.api("auth/pusher"),
                     auth        : {
                        headers: {
                           Authorization: "Bearer " + token
                        }
                     }
                  });
                  bindGlobalEvents();
                  df.resolve();
               })
            })
         } else {
            df.resolve();
         }
         return df.promise;
      }

      function bindGlobalEvents(){
         _pusher.bind("error", function (error){
            console.log(error);
         });
      }
   }
})();