(function (){
   angular.module('app.core').service('notifications', NotificationsService)

   NotificationsService.$inject = [ '$rootScope', 'event','$compile' ];
   function NotificationsService($rootScope, event, $compile){
      this.registerListeners = registerListeners;

      function registerListeners(){
         $rootScope.$on(event.newNotification, handleNewNotification);
      }

      function handleNewNotification(event, data){
         showToaster(data);
      }

      function showToaster(data){
         var scope = $rootScope.$new();
         scope.data = data;
         var elm = $compile('<div noti-toast noti-data="data"></div>')(scope);
         angular.element('#toaster').append(elm);
      }
   }
})();