(function (){
   angular.module('app.core').factory('Repo', RepoFactory);
   
   RepoFactory.$inject = [ 'api','$q' ];
   function RepoFactory(api, $q){
      function Repo(resourceId, Model){
         var _this = this;
         _this.resourceId = resourceId;
         _this.Model = Model;
         _this.get = get;
         _this.create = create;
         _this.update = update;
         _this.query = query;
         _this.path = path;
         _this.action = action;
         _this.parseArrayResponse = parseArrayResponse;
         return _this;

         function get(id){
            return action(id, "get", null, null);
         }
         function create(model){
            return action("/", "post", null, model);
         }
         function update(model){
            return action(model.id, "post", null, model);
         }

         function query(params){
            return action("/", "get", params, null, true);
         }

         function action(path, method, params, data, isArray){
            var result;
            if ( isArray ) {
               result = [];
            } else {
               result = new _this.Model();
            }
            var df = $q.defer();
            result.$promise = df.promise;
            api.call(_this.path(path), method, params, data)
               .then(function (response){
                  if ( isArray ) {
                     parseArrayResponse(response, result);
                  } else {
                     result.setFieldsData(response.data);
                  }
                  df.resolve(result);
               }).catch(df.reject);
            return result;
         }

         function path(path){
            if ( typeof path === "undefined" || path==="/" || path ==="" ) {
               return _this.resourceId;
            }
            return _this.resourceId + '/' + path;
         }

         function parseArrayResponse(response, arr){
            var data = response.data;
            var len = data.length;
            for ( var i = 0; i < len; i ++ ) {
               arr[ i ] = new _this.Model(data[ i ]);
            }
         }
      }
      
      return Repo;
   }
})();