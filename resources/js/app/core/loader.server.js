(function (){
   angular.module('app.core').service('loader', Loader);
   
   Loader.$inject = [ '$q', '$timeout' ];
   function Loader($q, $timeout){
      this.loadScript = loadScript;

      function loadScript(url, ObjectNeeded){
         if(typeof ObjectNeeded !== "undefined"&&typeof window[ObjectNeeded]!=="undefined"){
            return $q.resolve({});
         }
         var timerRef;
         var df = $q.defer();
         var loaded = false;
         var script = document.createElement('script');
         script.type = "text/javascript";
         script.src = url;
         script.onload = script.onreadystatechange = function (){
            if ( ! loaded && (! this.readyState || this.readyState == 'complete') ) {
               loaded = true;
               df.resolve();
               $timeout.cancel(timerRef)
            }
         }
         script.onerror = function (){
            $timeout.cancel(timerRef)
            df.reject();
         }
         var scripts = document.getElementsByTagName('script');
         if ( script_exists(scripts, script) ) {
            script_exists(scripts, script).remove();
         }
         $timeout(function (){
            scripts[ 0 ].parentNode.insertBefore(script, scripts[ 0 ]);
         }, 0);
         timerRef = $timeout(function (){
            console.log("Script loading timeout..");
            df.reject();
         }, 10000);
         return df.promise;
      }

      function script_exists(scripts, script){
         var exists = false;
         angular.forEach(scripts, function (s){
            if ( s.src === script.src ) {
               exists = s;
            }
         });
         return exists;
      }
   }
})();