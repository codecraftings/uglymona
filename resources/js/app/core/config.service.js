(function (){
   angular.module('app.core').service('config', ConfigService);
   
   ConfigService.$inject = ['$document'];
   function ConfigService($document){
      var _this = this;
      _this.get = get;
      function get(key){
         return $document[0 ].getElementsByName(key)[0 ].content;
      }
   }
})();