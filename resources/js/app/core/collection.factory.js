(function (){
   angular.module('app.core').factory('Collection', CollectionFactory);
   
   CollectionFactory.$inject = [ '$injector' ];
   function CollectionFactory($injector){
      function Collection(dataList, Type){
         var _list = [];
         Type = $injector.get(Type, "collection");
         for(var i=0;i<dataList.length; i++){
            _list[i] = new Type(dataList[i]);
         }
         return _list;
      }
      
      return Collection;
   }
})();