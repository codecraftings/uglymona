(function (){
   angular.module('app.user').factory('Profile', ProfileFactory);
   
   ProfileFactory.$inject = [ 'Model' ];
   function ProfileFactory(Model){
      function Profile(data){
         var _this = new Model;
         _this.setFields({
            likes_count: 0,
            profile_pic: "",
            date_of_birth: ""
         });
         _this.onFieldsDataUpdated = function (){

         };
         _this.setFieldsData(data);
         return _this;
      }

      return Profile;
   }
})();