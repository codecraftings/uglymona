(function (){
   angular.module('app.user')
      .config(RouteConfigProvider);
   
   RouteConfigProvider.$inject = [ '$routeProvider', 'authResolver' ];
   function RouteConfigProvider($routeProvider, authResolver){
      $routeProvider.when("/users/:id",{
         templateUrl: "templates/user-profile.html",
         controller: "UserProfileController",
         controllerAs: "vm",
         accessLevel: "user",
         resolve: {
            permissions: authResolver,
            profileData: resolveProfileData
         }
      });
   }

   resolveProfileData.$inject = ['userService','$route']
   function resolveProfileData(userService, $route){
      return userService.getProfile($route.current.params.id);
   }
})();