(function (){
   angular.module('app.user').service('users', usersRepo);
   
   usersRepo.$inject = [ 'User', 'Repo' ];
   function usersRepo(User, Repo){
      var _this = new Repo("users", User);
      _this.search = search;
      return _this;
      function search(filters){
         return _this.action("search", "post", null, filters, true);
      }
   }
})();