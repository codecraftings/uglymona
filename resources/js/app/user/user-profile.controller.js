(function (){
   angular.module('app.user').controller('UserProfileController', UserProfileController);
   
   UserProfileController.$inject = ['$rootScope','$route', 'profileData'];
   function UserProfileController($rootScope, $route, profileData){
      var vm = this;
      vm.visibleTab = 'photos';
      vm.user = profileData;
      vm.sendMessage = sendMessage;
      construct();
      function construct(){
         $rootScope.pageTitle = vm.user.display_name+" | Uglymona";
      }
      function sendMessage(){
         $rootScope.$broadcast("inbox:show", vm.user);
      }
   }
})();