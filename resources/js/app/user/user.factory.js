(function (){
   angular.module('app.user').factory('User', UserFactory);

   UserFactory.$inject = [ 'Model', 'Profile', '$location', 'event', '$rootScope', 'Collection' ];
   function UserFactory(Model, Profile, $location, event, $rootScope, Collection){
      //var User = $resource(urls.api('users/:id'),{id: '@id'});
      function User(data){
         var _this = new Model;
         _this.setFields({
            display_name: "",
            full_name   : "",
            username    : "",
            city        : "",
            state       : "",
            country     : "",
            permalink   : "",
            profile     : {},
            photos      : [],
            videos      : []
         });
         _this.onDataUpdated(function (){
            _this.profile = new Profile(_this.profile);
            _this.display_name = _this.full_name || _this.username;
            _this.photos = new Collection(_this.photos, 'Photo');
            _this.videos = new Collection(_this.videos, 'Video');
         });
         _this.setFieldsData(data);
         _this.showProfile = showProfile;
         _this.openChat = openChat;
         _this.requestPhoneNumber = requestPhoneNumber;
         return _this;
         function showProfile(){
            $location.path("users/" + _this.id);
         }
         function requestPhoneNumber(){
            $rootScope.$broadcast(event.newMessage, _this, "Hey, "+_this.name+" would like to know your phone number? Reply to this message if you want to share your number!");
         }
         function openChat(){
            $rootScope.$broadcast(event.newMessage, _this);
         }
      }

      return User;
   }
})();