(function (){
   angular.module('app.user').service('userService', UserService);

   UserService.$inject = [ 'users','$q' ];
   function UserService(users,$q){
      var _fetched_profile = null;
      this.fetchProfile = fetchProfile;
      this.getProfile = getProfile;
      function fetchProfile(id){
         var df = $q.defer();
         _fetched_profile = users.get(id);
         _fetched_profile.$promise.then(function(){
            df.resolve(_fetched_profile);
         }).catch(function(error){
            df.reject(error);
         });
         return df.promise;
      }

      function getProfile(id){
         if ( _fetched_profile === null || _fetched_profile.id !== id ) {
            return fetchProfile(id);
         }
         return _fetched_profile;
      }
   }
})();