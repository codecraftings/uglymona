<?php

//
// NOTE Migration Created: 2015-02-16 23:11:57
// --------------------------------------------------

use Mona\Core\User\Membership;

class CreateUglymonaDatabase
{
//
// NOTE - Make changes to the database.
// --------------------------------------------------

    public function up()
    {

//
// NOTE -- blogs
// --------------------------------------------------

        Schema::create('blogs', function ($table) {
            $table->increments('id')->unsigned();
            $table->text('title', 150)->nullable();
            $table->text('content')->nullable();
            $table->string('author_name')->nullable();
            $table->text('summary')->nullable();
            $table->boolean("is_pdf")->default(false);
            $table->text("pdf_link")->nullable();
            $table->unsignedInteger('author_id')->unsigned();
            $table->unsignedInteger('category_id')->unsigned();
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });

        Schema::create('categories', function ($table) {
            $table->increments('id');
            $table->string('label')->nullable();
            $table->string('slag')->nullable();
        });
//
// NOTE -- connections
// --------------------------------------------------

        Schema::create('connections', function ($table) {
            $table->increments('id')->unsigned();
            $table->unsignedInteger('user_id')->unsigned();
            $table->unsignedInteger('connected_id')->unsigned();
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->string('status', 20)->nullable();
        });


//
// NOTE -- inbox
// --------------------------------------------------

        Schema::create('inbox', function ($table) {
            $table->increments('id')->unsigned();
            $table->unsignedInteger('sender_id')->unsigned();
            $table->unsignedInteger('receiver_id')->unsigned();
            $table->text('message')->nullable();
            $table->char('conversation_id', 20);
            $table->string('status', 10)->nullable();
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });


//
// NOTE -- letter
// --------------------------------------------------

        Schema::create('letter', function ($table) {
            $table->increments('id')->unsigned();
            $table->unsignedInteger('user_id')->unsigned();
            $table->unsignedInteger('status')->nullable();
            $table->unsignedInteger('privacy')->nullable();
            $table->text('message')->nullable();
            $table->text('reply')->nullable();
            $table->dateTime('replied_at')->nullable();
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });

//
// NOTE -- photos
// --------------------------------------------------

        Schema::create('photos', function ($table) {
            $table->increments('id')->unsigned();
            $table->unsignedInteger('user_id')->unsigned();
            $table->string('caption', 200)->nullable();
            $table->string('type', 15)->nullable();
            $table->string('status', 15)->nullable();
            $table->string('source_path', 200)->nullable();
            $table->string('cdn_name', 20)->nullable();
            $table->json('available_sizes')->nullable();
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->unsignedInteger('filestore_id')->default(0);
        });


//
// NOTE -- user_profile
// --------------------------------------------------

        Schema::create('user_profile', function ($table) {
            $table->increments('id')->unsigned();
            $table->unsignedInteger('user_id')->unsigned()->unique();
            $table->unsignedInteger('profile_photo_id')->unsigned()->default(0);
            $table->integer("profile_completion")->default(20);
            $table->dateTime('date_of_birth')->nullable();
            $table->string('gender', 20)->nullable();
            $table->string('looking_for', 20)->nullable();
            $table->string('looking_for_age', 20)->nullable();
            $table->string('zodiac_sign', 30)->nullable();
            $table->string('relationship_status', 30)->nullable();
            $table->boolean('want_children')->nullable();
            $table->string('education', 50)->nullable();
            $table->string('work', 50)->nullable();
            $table->string('personality', 50)->nullable();
            $table->string('ethnicity', 30)->nullable();
            $table->string('build_type', 30)->nullable();
            $table->string('hair_color', 20)->nullable();
            $table->string('eye_color', 20)->nullable();
            $table->boolean('do_smoke')->nullable();
            $table->boolean('do_drink')->nullable();
            $table->text('about_text')->nullable();
            $table->unsignedInteger('likes_count')->unsigned()->default(0);
            $table->unsignedInteger('views_count')->unsigned()->default(0);
        });


//
// NOTE -- users
// --------------------------------------------------

        Schema::create('users', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('full_name', 80)->nullable();
            $table->string('username', 50)->nullable();
            $table->string('email', 80)->nullable();
            $table->string('password', 150)->nullable();
            $table->string('email_status', 150)->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('last_login')->nullable();
            $table->smallInteger('membership')->default(Membership::BASIC);
            $table->string('address', 200)->nullable();
            $table->string('city', 50)->nullable();
            $table->string('state', 50)->nullable();
            $table->string('zip', 20)->nullable();
            $table->string('country', 100)->nullable();
        });


//
// NOTE -- videos
// --------------------------------------------------

        Schema::create('videos', function ($table) {
            $table->increments('id')->unsigned();
            $table->unsignedInteger('user_id')->unsigned();
            $table->string('privacy', 20)->nullable();
            $table->string('status', 20)->nullable();
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->unsignedInteger('thumb_id')->unsigned();
            $table->string('source_path', 200)->nullable();
            $table->string('cdn_name', 200)->nullable();
            $table->text('available_formats')->nullable();
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
            $table->boolean('allow_comments')->default(true);
        });


    }

//
// NOTE - Revert the changes to the database.
// --------------------------------------------------

    public function down()
    {

        Schema::drop('blogs');
        Schema::drop('connections');
        Schema::drop('inbox');
        Schema::drop('letter');
        Schema::drop('photos');
        Schema::drop('user_profile');
        Schema::drop('users');
        Schema::drop('videos');
        Schema::drop('categories');
    }
}