<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class NewInboxDatabaseScheme extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("messages", function(Blueprint $table){
            $table->bigIncrements('id');
            $table->bigInteger('sender_id', false, true);
            $table->bigInteger('receiver_id', false, true);
            $table->bigInteger('conversation_id', false, true);
            $table->string("status");
            $table->text("message");
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
        Schema::create("conversations", function(Blueprint $table){
            $table->bigIncrements('id');
            $table->bigInteger('user1_id', false, true);
            $table->bigInteger('user2_id', false, true);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('messages');
        Schema::drop('conversations');
    }
}
