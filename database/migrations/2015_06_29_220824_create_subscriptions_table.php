<?php

use Carbon\Carbon;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("subscriptions", function(Blueprint $table){
            $table->bigIncrements("id");
            $table->bigInteger("user_id",false, true);
            $table->char("status", 30);
            $table->string("delegate_class");
            $table->json("coupons");
            $table->integer("payment_count")->default(0);
            $table->integer("failed_tries")->default(0);
            $table->dateTime("trial_ends_on");
            $table->dateTime("next_payment_due");
            $table->dateTime("last_payment");
            $table->dateTime("last_failed_try");
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("subscriptions");
    }
}
