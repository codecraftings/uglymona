<?php
use Illuminate\Support\Facades\Hash;
use Mona\Core\Inbox\Conversation;
use Mona\Core\User\Membership;
use Mona\Core\User\Subscription\FullMembershipSubscriptionDelegate;
use Mona\Core\User\Subscription\Subscription;
use Mona\Core\User\User;

if (!function_exists("randomID")) {
    function randomID($model)
    {
        return $model::query()->orderByRaw("rand()")->first()->id;
    }
}
$factory->define('Mona\Core\User\User', function ($faker) {
    return [
        'full_name'    => $faker->name,
        'username'     => $faker->username,
        'email'        => $faker->email,
        'address'      => $faker->address,
        'email_status' => 'verified',
        'city'         => $faker->city,
        'state'        => $faker->state,
        'zip'          => $faker->postcode,
        'password'     => Hash::make('odeskbangla'),
        'membership'   => $faker->randomElement([Membership::BASIC, Membership::FULL]),
        'country'      => $faker->country,
        'last_login' => $faker->dateTime(),
    ];
});
$factory->define('Mona\Core\User\Profile', function ($faker) {
    return [
        'user_id'             => 0,
        'profile_photo_id'    => '0',
        'profile_completion' => 80,
        'date_of_birth'       => $faker->dateTime('-16 years'),
        'gender'              => $faker->randomElement(['man', 'woman']),
        'looking_for'         => $faker->randomElement(['man', 'woman']),
        'looking_for_age'         => $faker->randomElement(['17-18', '20-22']),
        'zodiac_sign'         => $faker->randomElement(['Aquarius', 'Pisces']),
        'relationship_status' => $faker->randomElement(['single', 'married', 'divorced']),
        'want_children'       => $faker->randomElement(['0', '1']),
        'education'           => $faker->randomElement(['high school', 'college', 'phd']),
        'work'                => $faker->randomElement(['engineer', 'doctor', 'teacher']),
        'personality'         => 'good',
        'ethnicity'           => $faker->randomElement(['asian', 'american']),
        'build_type'          => $faker->randomElement(['skinny', 'average']),
        'hair_color'          => $faker->randomElement(['black', 'red', 'brown']),
        'eye_color'           => 'black',
        'do_smoke'            => $faker->randomElement(['1', '0']),
        'do_drink'            => $faker->randomElement(['1', '0']),
        'about_text'          => $faker->paragraph(),
        'likes_count' => 0,
        'views_count' => 0,
    ];
});

$factory->define('Mona\Core\Letter\Letter', function ($faker) {
    return [
        'user_id' => $faker->randomElement([1, 2, 3, 4, 5, 6]),
        'status'  => 2,
        'privacy' => 0,
        'message' => $faker->paragraph(15),
        'reply'   => $faker->paragraph(12)
    ];
});
$factory->define('Mona\Core\Video\Video', function ($faker) {
    return [
        //'user_id' => any('9 or 10 or 12 or 4 or 5 or 6'),
        'user_id'           => $faker->randomElement([1, 2, 3, 4, 5, 6]),
        'privacy'           => 'anyone',
        'title'             => $faker->sentence(),
        'description'       => $faker->paragraph(),
        'thumb_id'          => $faker->randomElement(['6', '8', '9']),
        'source_path'       => "uploads/demo/videos/",
        "available_formats" => ["mp4"]
    ];
});
$factory->define('Mona\Core\Photo\Photo', function ($faker) {
    return [
        'user_id'         => $faker->randomElement([1, 2, 3, 4, 5, 6]),
        'caption'         => $faker->paragraph(1),
        'source_path'     => "uploads/demo/photos/",
        'available_sizes' => ["original"]
    ];
});
$factory->define(Conversation::class, function($faker){
    return [
        "updated_at" => $faker->dateTime()
    ];
});
$factory->define('Mona\Core\Inbox\Message', function ($faker) {
    return [
        'status'=> $faker->randomElement(['read','unread']),
        'created_at' => $faker->dateTime(),
        'message' => $faker->paragraph(3)
    ];
});

$factory->define(Mona\Core\Blog\Blog::class, function ($faker) {
    return [
        'title'       => $faker->realText(80),
        'content'     => $faker->realText(500),
        'summary'     => $faker->realText(120),
        'author_name' => "Mona's Content Writer",
        'category_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7]),
        'author_id'   => 1,
        'created_at'  => $faker->dateTime()
    ];
});
$factory->define(Subscription::class, function($faker){
    return [
        "user_id" => randomID(User::class),
        "delegate" => FullMembershipSubscriptionDelegate::class,
        "coupons" => []
    ];
});