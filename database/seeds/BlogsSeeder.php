<?php


use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory;

class BlogsSeeder extends Seeder
{
    public function run()
    {
        for ($i = 0; $i < 30; $i ++)
            factory(Mona\Core\Blog\Blog::class)->create([]);
        factory(Mona\Core\Blog\Blog::class)->create([
            "title"       => "Knowing me, knowing you. Can meditation improve your relationship?",
            "is_pdf"      => true,
            'category_id' => 7,
            "pdf_link"    => "articles/meditation/index.html"
        ]);
    }
}