<?php


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Laracasts\TestDummy\Factory;
use Mona\Core\User\Membership;

class LoadDefaultData extends Seeder
{
    public function run()
    {
//        Factory::$factoriesPath = base_path("database/seeds/factories/ModelFactory.php");
        $this->createAdminUser();
        $this->createBlogCategories();
    }

    private function createAdminUser()
    {
        $user = factory('Mona\Core\User\User')->create(
            ['email'      => 'mahfuzkhanshovon@gmail.com',
             'username'   => 'mahfuz',
             'password'   => Hash::make("bangla"),
             'membership' => Membership::ADMIN
            ]);
        factory('Mona\Core\User\Profile')->create([
            'profile_completion' => 20,
            'user_id'            => $user->id
        ]);
    }

    private function createBlogCategories()
    {
        $this->call('BlogCategorySeeder');
    }
}