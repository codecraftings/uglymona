<?php


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Laracasts\TestDummy\Factory;
use Mona\Core\Photo\Photo;
use Mona\Core\User\Profile;
use Mona\Core\User\User;

class LoadDemoData extends Seeder
{
    public function run()
    {
        $this->createUsers();
        $this->createLetters();
        $this->createMessages();
        $this->createBlogs();
    }

    private function createUsers()
    {
        for ($i = 0; $i < 30; $i ++) {
            $user = factory(User::class)->create([]);
            $profile = factory('Mona\Core\User\Profile')->create([
                "user_id" => $user->id
            ]);
            $pp = $this->createPhoto("demo/pp/" . strtolower($profile->gender) . "/" . mt_rand(1, 9) . ".jpg", Photo::TYPE_PROFILE_PIC, $user->id);
            $profile->profile_photo_id = $pp->id;
            $profile->save();
            $n = mt_rand(3, 8);
            for ($j = 1; $j <= $n; $j ++) {
                $this->createPhoto("demo/casual/" . strtolower($profile->gender) . "/" . $j . ".jpg", Photo::TYPE_ALBUM_PHOTO, $user->id);
            }
            $n = mt_rand(0, 4);
            for ($j = 1; $j <= $n; $j ++) {
                $this->createVideo("demo/videos/1.mp4", $user->id);
            }
        }
    }

    private function createPhoto($path, $type, $user_id)
    {
        $photo = factory(Photo::class)->create([
            "type"            => $type,
            "user_id"         => $user_id,
            "available_sizes" => ["original"]
        ]);
        Storage::put("uploads/demo/photos/" . $photo->id . ".original.jpg", Storage::get($path));
        return $photo;
    }

    private function createVideo($path, $user_id)
    {
        $video = factory('Mona\Core\Video\Video')->create([
            'user_id'           => $user_id,
            'thumb_id'          => 1,
            'available_formats' => ["mp4"]
        ]);
        Storage::put("uploads/demo/videos/" . $video->id . ".mp4", Storage::get($path));
        $photo = $this->createPhoto("demo/videos/" . mt_rand(1, 3) . ".jpg", Photo::TYPE_VIDEO_THUMB, $video->user_id);
        $video->thumb_id = $photo->id;
        $video->save();
    }

    private function createLetters()
    {
        for ($i = 0; $i < 20; $i ++)
            factory('Mona\Core\Letter\Letter')->create([]);
    }

    private function createMessages()
    {
        $this->call("InboxSeeder");
    }

    private function createBlogs()
    {
        $this->call("BlogsSeeder");
    }
}