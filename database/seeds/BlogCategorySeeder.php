<?php


use Illuminate\Database\Seeder;
use Mona\Core\Blog\Category;

class BlogCategorySeeder extends Seeder{
    public function run(){
        $this->createCategory("Entertainment");
        $this->createCategory("Travels");
        $this->createCategory("Relationships");
        $this->createCategory("Fashion");
        $this->createCategory("Fitness");
        $this->createCategory("Health");
        $this->createCategory("Life");
    }
    private function createCategory($name){
        Category::create([
            "label" => $name,
            "slag" => $this->normalize($name)
        ]);
    }
    private function normalize($name){
        return strtolower(str_replace(" ", "_", $name));
    }
}