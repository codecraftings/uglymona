<?php


use Laracasts\TestDummy\Factory;
use Illuminate\Database\Seeder;
use Mona\Core\Inbox\Conversation;
use Mona\Core\Inbox\Message;

class InboxSeeder extends Seeder
{
    public function run()
    {
        $this->createMsg(1, 2);
        $this->createMsg(1, 3);
        $this->createMsg(1, 4);
        $this->createMsg(1, 5);
        $this->createMsg(1, 6);
        $this->createMsg(1, 7);
    }

    /**
     * @param $sender
     * @param $receiver
     */
    private function createMsg($sender, $receiver)
    {
        $time = Carbon\Carbon::now()->subMinutes(mt_rand(0, 10000));
        $convo = factory(Conversation::class)->create([
            'user1_id'   => $sender,
            'user2_id'   => $receiver,
            'updated_at' => $time
        ]);
        $l = mt_rand(5, 15);
        $a = [$sender, $receiver];
        for ($i = 0; $i < $l; $i ++) {
            $j = mt_rand(0, 1);
            $s = $a[$j];
            $r = $a[1 - $j];
            factory('Mona\Core\Inbox\Message')->create([
                'sender_id'       => $s,
                'receiver_id'     => $r,
                'conversation_id' => $convo->id,
            ]);
        }
    }
}