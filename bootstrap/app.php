<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new Laravel application instance
| which serves as the "glue" for all the components of Laravel, and is
| the IoC container for the system binding all of the various parts.
|
*/

use Monolog\Handler\LogglyHandler;
use Monolog\Handler\SyslogHandler;
use Monolog\Logger;

$app = new Illuminate\Foundation\Application(
    realpath(__DIR__ . '/../')
);

/*
|--------------------------------------------------------------------------
| Bind Important Interfaces
|--------------------------------------------------------------------------
|
| Next, we need to bind some important interfaces into the container so
| we will be able to resolve them when needed. The kernels serve the
| incoming requests to this application from both the web and CLI.
|
*/

$app->singleton(
    'Illuminate\Contracts\Http\Kernel',
    'Mona\Http\Kernel'
);

$app->singleton(
    'Illuminate\Contracts\Console\Kernel',
    'Mona\Console\Kernel'
);

$app->singleton(
    'Illuminate\Contracts\Debug\ExceptionHandler',
    'Mona\Exceptions\Handler'
);

/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/
$app->configureMonologUsing(function ($monolog) {
//    $loggly = new LogglyHandler('c9ddfdee-d682-45b9-8139-a0fe83a8f823', Logger::INFO);
//    $loggly->setTag("uglymona");
//    $monolog->pushHandler($loggly);
//    $papertail = new SyslogHandler("papertail", LOG_USER, Logger::INFO);
//    $formatter = new \Monolog\Formatter\LineFormatter('%channel%.%level_name%: %message% %extra%');
//    $papertail->setFormatter($formatter);
//    $monolog->pushHandler($papertail);
});

return $app;
