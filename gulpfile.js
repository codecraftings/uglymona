var elixir = require('laravel-elixir');
var gulp = require('gulp');
var exec = require('child_process').exec;
var merge = require("merge-stream");
elixir(function (mix){
   mix.styles(
      [
         'normalize.css',
         'plugins/*.css',
         'icons.css',
         'emoticons.css',
         'list-view.min.css',
         'style.css',
         'responsive.css'
      ],
      'public/assets/css/all.min.css'
   );
   mix.scripts(
      [
         "jquery/*.js",
         "underscore/*.js",
         "angular/*.js",
         "angular-plugins/*.js",
         "plugins/*.js",
         "app/**/*.module.js",
         "app/**/!(*.module.js|*.spec.js)"
      ],
      "public/assets/js/build/all.min.js"
   );
   mix.task('compile_web', "public/web/**/!(index.html)");
});

gulp.task('compile_web', function (){
   exec("php artisan web:compile");
});

var emoDir = "resources/assets/emoticons";
gulp.task("resize_emoticons", function (){
   var imageResize = require("gulp-image-resize");
   var changed = require("gulp-changed");
   var rename = require("gulp-rename");
   var s1 = resize(30, 30, false);
   var s2 = resize(60, 60, "@2x");
   return merge(s1, s2);
   function resize(w, h, appendName){
      console.log("resizing images...");
      return gulp.src(emoDir + "/*.png")
         //.pipe(changed(emoDir + "/resized"))
         .pipe(imageResize({
            width      : w,
            height     : h,
            imageMagick: true
         }))
         .pipe(rename(function (path){
            if ( appendName ) {
               path.basename += appendName;
            }
            console.log(path.basename)
         }))
         .pipe(gulp.dest(emoDir + "/resized"));
   }
});
gulp.task("emoticons", [ "resize_emoticons" ], function (){
   return compileSheet();

   function compileSheet(){
      console.log("compiling sprite sheet..");
      var generator = require("gulp.spritesmith");
      var sprite = gulp.src(emoDir + "/resized/*.png")
         .pipe(generator({
            retinaSrcFilter: [ emoDir + "/resized/*@2x.png" ],
            imgName        : "emoticons.png",
            retinaImgName  : "emoticons@2x.png",
            cssName        : "emoticons.css",
            imgPath        : "../images/emoticons.png",
            retinaImgPath  : "../images/emoticons@2x.png",
            cssVarMap: function(sprite){
               sprite.name = "emo-"+sprite.name;
            }
         }))
      var imgStream = sprite.img.pipe(gulp.dest("public/assets/images/"));
      var cssStream = sprite.css.pipe(gulp.dest("resources/css/"));
      return merge(imgStream, cssStream);
   }

});