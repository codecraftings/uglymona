'use strict';

module.exports = function(grunt){
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		watch: {
			css: {
				files: ['public/css/*.css'],
				tasks: ['cssmin']
			},
			js: {
				files: ['public/js/*.js'],
				tasks: ['uglify']
			}
		},
		cssmin: {
			all:{
				files: {
					'public/css/all.min.css':[
						'public/css/normalize.css',
						'public/css/magic.css',
						'public/css/jquery.bxslider.css',
						'bower_components/videojs/dist/video-js/video-js.css',
						'public/css/style.css',
						'public/css/responsive.css'
					],
					'public/css/list-view.min.css': [
						'bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css'
					]
				}
			}
		},
		uglify: {
			all:{
				files: {
					'public/js/build/plugins.min.js':[
						'bower_components/jquery-ui/ui/widget.js',
						'bower_components/jquery-ui/ui/autocomplete.js',
						'bower_components/jquery-ui/ui/datepicker.js',
						'bower_components/jquery-ui/ui/dialog.js',
						'bower_components/jquery-ui/ui/droppable.js',
						'bower_components/videojs/dist/video-js/video.js',
						'public/js/resumable.js'
					],
					'public/js/build/list-view.min.js': [
						'bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',
						'public/js/list-view.js'
					],
					'public/js/build/all.min.js':[
						'bower_components/jquery/dist/jquery.js',
						'bower_components/jquery-ui/ui/core.js',
						'bower_components/jquery-ui/ui/effect.js',
						'bower_components/jquery-ui/ui/effect-fade.js',
						'bower_components/underscore/underscore.js',
						'bower_components/backbone/backbone.js',
						'bower_components/bxslider-4/plugins/jquery.easing.1.3.js',
						'bower_components/bxslider-4/jquery.bxslider.js',
						'bower_components/buzz/dist/buzz.js',
						'public/js/sounds.js',
						'public/js/file_store.js',
						'public/js/main.js'
					]
				},
				options: {
					sourceMap: true
				}
			}
		}
	});
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.registerTask('default',['uglify']);
};