<?php

use Illuminate\Support\Facades\Artisan;
use Mockery\Mock;

class TestCase extends Illuminate\Foundation\Testing\TestCase
{

    protected $baseUrl = "http://um.api";

    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function setUp()
    {
        parent::setUp();
        Artisan::call('migrate');
        Artisan::call('db:seed',
            ['--class' => 'LoadDefaultData']);
    }
    public function mock($class){
        $mock = Mockery::mock($class);
        $this->app->instance($class, $mock);
        return $mock;
    }
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../bootstrap/app.php';

        $app->make('Illuminate\Contracts\Console\Kernel')->bootstrap();

        return $app;
    }

}
