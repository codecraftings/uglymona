<?php

class ExampleTest extends TestCase {

	protected $baseUrl = "http://um.api";
	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testBasicExample()
	{
		$response = $this->call('GET', '/test');

		$this->assertEquals(200, $response->getStatusCode());
	}

}
