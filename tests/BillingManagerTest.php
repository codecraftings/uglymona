<?php


use Illuminate\Support\Facades\App;
use Mona\Core\Mail\Composers\InvoiceEmail;
use Mona\Core\User\Billing\BillingManager;
use Mona\Core\User\Billing\BillingWasSuccessful;
use Mona\Core\User\Billing\Card;
use Mona\Core\User\Billing\StripeBillingManager;
use Mona\Core\User\User;
use Mona\Core\Mail\Mailer;

class BillingManagerTest extends TestCase
{
    /**
     * @var BillingManagerStub
     */
    protected $billing;
    protected $mailer;
    public function setUp()
    {
        parent::setUp();
        $this->mailer = Mockery::mock(Mailer::class);
        $this->app->instance(Mailer::class, $this->mailer);
        $this->billing = App::make(BillingManagerStub::class);
    }

    /**
     * @test
     */
    public function it_should_attach_cards_for_user()
    {
        $user = User::find(1);
        $card = $this->billing->attachCard($user, []);
        $card = $this->billing->attachCard($user, []);
        $cards = $this->billing->getAllCards($user);
        $this->assertCount(2, $cards);
    }
    /**
     * @test
     */
    public function it_should_return_users_last_added_card_as_active_card(){
        $user = User::find(1);
        $card1 = $this->billing->attachCard($user, []);
        $card2 = $this->billing->attachCard($user, []);
        $card = $this->billing->getActiveCard($user);
        $this->assertNotNull($card);
        $this->assertEquals($card->id, $card2->id);
    }
    /**
     * @test
     */
    public function it_should_successfully_charge_money_on_user_cards(){
        $this->expectsEvents(BillingWasSuccessful::class);
        $user = User::find(1);
        $card = $this->billing->attachCard($user, []);
        $transaction = $this->billing->charge($user, 10);
        $this->assertEquals(10, $transaction->amount);
    }

    /**
     * @test
     */
    public function it_should_send_invoice_email_upon_charge(){
        $this->mailer->shouldReceive("send")->once()->with(Mockery::type(InvoiceEmail::class));
        $user = User::find(1);
        $card = $this->billing->attachCard($user, []);
        $transaction = $this->billing->charge($user, 10);
        $this->assertEquals(10, $transaction->amount);
    }
}

class BillingManagerStub extends BillingManager
{

    function processCardData(array $cardData)
    {
        return [
            "billing_id" => mt_rand(1, 100),
            "last_four"  => '4222'
        ];
    }

    function processPayment(Card $card, $amount)
    {
        return [
            'amount' => $amount
        ];
    }
}