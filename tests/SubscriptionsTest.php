<?php


use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Mockery\Mock;
use Mona\Core\Mail\Composers\MembershipUpgradeEmail;
use Mona\Core\Mail\Mailer;
use Mona\Core\User\Billing\BillingInterface;
use Mona\Core\User\Billing\PaymentFailedException;
use Mona\Core\User\Membership;
use Mona\Core\User\Subscription\FullMembershipSubscriptionDelegate;
use Mona\Core\User\Subscription\Subscription;
use Mona\Core\User\Subscription\SubscriptionManagementJob;
use Mona\Core\User\Subscription\SubscriptionManager;
use Mona\Core\User\User;

class SubscriptionsTest extends TestCase
{
    /**
     * @var SubscriptionManager
     */
    protected $manager;
    protected $mailer;
    use DispatchesJobs;

    public function setUp()
    {
        parent::setUp();
        $this->manager = $this->app->make(SubscriptionManager::class);
    }

    /**
     * @test
     */
    public function it_should_create_a_subscription_successfully()
    {
        $user = User::find(1);
        $subscription = $this->manager->create($user, FullMembershipSubscriptionDelegate::class, []);
        $this->assertNotNull($subscription);
        $this->assertEquals($subscription->status, Subscription::STATUS_ACTIVE);
    }

    /**
     * @test
     */
    public function it_should_upgrade_user_to_full_membership()
    {
        /** @var User $user */
        $user = factory(User::class)->create([
            "membership" => Membership::BASIC
        ]);
        $this->assertEquals($user->membership->order, Membership::BASIC);
        $subscription = $this->manager->create($user, FullMembershipSubscriptionDelegate::class, []);
        $this->assertNotNull($subscription);
        $this->assertEquals(User::find($user->id)->membership->order, Membership::FULL);
    }

    /**
     * @test
     */
    public function user_should_receive_email_when_membership_subscription_created()
    {
        $mailer = Mockery::mock(Mailer::class)->makePartial();
        $this->app->instance(Mailer::class, $mailer);
        $mailer->shouldReceive("post")->once()->with(Mockery::type(MembershipUpgradeEmail::class));
        $user = factory(User::class)->create([
            "membership" => Membership::BASIC
        ]);
        $subscription = $this->manager->create($user, FullMembershipSubscriptionDelegate::class, []);
        $this->assertNotNull($subscription);
    }

    /**
     * @test
     */
    public function it_should_charge_fee_on_all_active_subscription_when_due_date_arrives()
    {
        $billing = $this->app->make(BillingManagerStub::class);
        $this->app->instance(BillingInterface::class, $billing);
        $users = factory(User::class, 3)->create();
        $users->each(function ($user) use ($billing) {
            $billing->attachCard($user, []);
        });
        $subscription1 = $this->manager->create($users[1], FullMembershipSubscriptionDelegate::class, []);
        $subscription2 = $this->manager->create($users[2], FullMembershipSubscriptionDelegate::class, []);
        $subscription0 = $this->manager->create($users[0], FullMembershipSubscriptionDelegate::class, []);
        $subscription0->next_payment_due = Carbon::now()->addDays(2);
        $subscription0->save();
        $subscription2->next_payment_due = Carbon::now()->subDays(2);
        $this->assertEquals(0, $subscription0->payment_count);
        $this->assertEquals(0, $subscription1->payment_count);
        $this->assertEquals(0, $subscription2->payment_count);
        $this->dispatch($this->app->make(SubscriptionManagementJob::class));
        $this->assertEquals(0, Subscription::find($subscription0->id)->payment_count);
        $this->assertEquals(1, Subscription::find($subscription1->id)->payment_count);
        $this->assertEquals(1, Subscription::find($subscription2->id)->payment_count);
    }

    /**
     * @test
     */
    public function it_should_deactivated_subscription_when_payment_fails_3_times()
    {
        $billing = Mockery::mock(BillingManagerStub::class);
        $billing->shouldReceive("charge")->times(3)->andThrow(PaymentFailedException::class);
        $this->app->instance(BillingInterface::class, $billing);
//        $delegate = $this->mock(FullMembershipSubscriptionDelegate::class)->shouldIgnoreMissing();
//        $delegate->shouldReceive("onPaymentFailed")->times(3);
        $user = factory(User::class)->create();
        /** @var SubscriptionManager $manager */
        $manager = $this->app->make(SubscriptionManager::class);
        $subscription = $manager->create($user, FullMembershipSubscriptionDelegate::class, []);
        $manager->processPayment($subscription);
        $this->assertEquals(Subscription::STATUS_ACTIVE, Subscription::find($subscription->id)->status);
        $manager->processPayment($subscription);
        $this->assertEquals(Subscription::STATUS_ACTIVE, Subscription::find($subscription->id)->status);
        $manager->processPayment($subscription);
        $this->assertEquals(Subscription::STATUS_DEACTIVATED, Subscription::find($subscription->id)->status);
    }

    /**
     * @test
     */
    public function it_should_change_status_to_ended_when_due_date_exceeds()
    {
        $billing = $this->app->make(BillingManagerStub::class);
        $this->app->instance(BillingInterface::class, $billing);
        $users = factory(User::class, 4)->create();
        $users->each(function ($user) use ($billing) {
            $billing->attachCard($user, []);
        });
        $subscription1 = $this->manager->create($users[1], FullMembershipSubscriptionDelegate::class, []);
        $subscription2 = $this->manager->create($users[2], FullMembershipSubscriptionDelegate::class, []);
        $subscription3 = $this->manager->create($users[3], FullMembershipSubscriptionDelegate::class, []);
        $subscription0 = $this->manager->create($users[0], FullMembershipSubscriptionDelegate::class, []);
        $subscription0->next_payment_due = Carbon::now()->addDays(2);
        $subscription0->setStatus(Subscription::STATUS_CANCELLED);
        $subscription0->save();
        $subscription1->next_payment_due = Carbon::now()->subHours(5);
        $subscription1->setStatus(Subscription::STATUS_DEACTIVATED);
        $subscription1->save();
        $subscription2->next_payment_due = Carbon::now()->subDays(2);
        $subscription2->save();
        $subscription3->next_payment_due = Carbon::now()->subDays(1);
        $subscription3->setStatus(Subscription::STATUS_CANCELLED);
        $subscription3->save();
        $this->dispatch($this->app->make(SubscriptionManagementJob::class));
        $this->assertEquals(Subscription::STATUS_CANCELLED, Subscription::find($subscription0->id)->status);
        $this->assertEquals(Subscription::STATUS_ENDED, Subscription::find($subscription1->id)->status);
        $this->assertEquals(Subscription::STATUS_ACTIVE, Subscription::find($subscription2->id)->status);
        $this->assertEquals(Subscription::STATUS_ENDED, Subscription::find($subscription3->id)->status);
    }
}