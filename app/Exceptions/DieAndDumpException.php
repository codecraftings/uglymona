<?php


namespace Mona\Exceptions;


class DieAndDumpException extends \Exception {
    /**
     * @var stdClass
     */
    protected $var;

    public function __construct($var)
    {
        $this->var = $var;
        parent::__construct();
    }
    public function getVar(){
        return $this->var;
    }
}