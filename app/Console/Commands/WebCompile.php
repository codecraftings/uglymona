<?php namespace Mona\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class WebCompile extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'web:compile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Compile bootstrap html file for web based app.';
    protected $storage;
    protected $pattern = "/<!\-\-!([^!]*)!\-\->/";

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->storage = Storage::disk('web');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $compiled = $this->compileHtml("web.boot.html");
        $compiled = $this->minifyHtml($compiled);
        $this->storage->put("index.html", $compiled);
    }

    private function compileHtml($filename)
    {
        $content = $this->storage->get($filename);
        $matches = [];
        preg_match_all($this->pattern, $content, $matches);
        foreach ($matches[0] as $key => $match) {
            $replacement = $this->executeDirective($matches[1][$key]);
            $content = str_replace($match, $replacement, $content);
        }
        return $content;
    }

    private function executeDirective($code)
    {
        echo "code: " . $code . "\n";
        return eval('return $this->' . $code . ";");
    }
    private function get_config($key){
        return Config::get($key);
    }
    private function web_url($path = "/")
    {
        return web_url($path);
    }

    private function asset_url($path = "/")
    {
        return asset_url($path);
    }

    private function api_url($path = "/")
    {
        return concat_url(Config::get('app.api_url'), $path);
    }

    private function attach($path)
    {
        return $this->compileHtml($path);
    }

    private function preloadTemplates($dir = "components")
    {
        $combined = "";
        $components = $this->storage->allFiles($dir);
        foreach ($components as $component) {
            echo $component . "\n";
            $combined .= $this->getPreloadedTemplateScript($component);
        }
        return $combined;
    }

    private function getPreloadedTemplateScript($filePath)
    {
        $tpl = '<script type="text/ng-template" id="' . $filePath . '">'."\n";
        $tpl .= $this->storage->get($filePath);
        $tpl .= "\n".'</script>'."\n";
        return $tpl;
    }
    private function minifyHtml($html){
        $replace = array(
            '/<!--[^\[](.*?)[^\]]-->/s' => '',
            "/<\?php/"                  => '<?php ',
            "/\n([\S])/"                => ' $1',
            "/\r/"                      => '',
            "/\n/"                      => '',
            "/\t/"                      => ' ',
            "/ +/"                      => ' ',
        );
        return preg_replace(
            array_keys($replace), array_values($replace), $html
        );
    }
    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [

        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

}
