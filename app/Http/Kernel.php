<?php namespace Mona\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	protected $middleware = [
		'Barryvdh\Cors\Middleware\HandleCors',
		'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
		'Illuminate\Cookie\Middleware\EncryptCookies',
		'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
		'Illuminate\Session\Middleware\StartSession',
		'Illuminate\View\Middleware\ShareErrorsFromSession',
		// 'Mona\Http\Middleware\VerifyCsrfToken',
	];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'csrf' => 'Mona\Http\Middleware\VerifyCsrfToken',
		'cors' => 'Barryvdh\Cors\Middleware\HandleCors',
		'auth' => 'Mona\Core\User\Auth\AuthMiddleware',
		'jwt.refresh' => 'Tymon\JWTAuth\Middleware\RefreshToken',
	];

}
