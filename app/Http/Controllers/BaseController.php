<?php namespace Mona\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;

class BaseController extends Controller {
	use DispatchesJobs, ValidatesRequests;
	public function __construct(){
	}
}
