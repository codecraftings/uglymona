<?php namespace Mona\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Mona\Core\Mail\Composers\EmailVerificationMail;
use Mona\Core\Mail\Composers\InvoiceEmail;
use Mona\Core\Mail\Composers\TestMail;
use Mona\Core\Mail\Mailer;
use Mona\Core\User\Matching\Match;
use Mona\Core\User\Registration\NewUserRegistration;
use Mona\Core\User\User;

class PagesController extends BaseController {

	public function __construct(){
		parent::__construct();
	}
	public function showLanding()
	{
		return redirect(web_url("/"));
	}
	public function test(Mailer $mailer){
//		Log::info("testing...");
		$mailer->post(new TestMail());
		echo "job posted";
//		throw new \Exception("testing....");
	}
}
