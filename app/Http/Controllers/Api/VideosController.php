<?php


namespace Mona\Http\Controllers\Api;


use Mona\Core\Video\VideoRepo;

class VideosController extends ApiController {
    /**
     * @var VideoRepo
     */
    private $repo;

    public function __construct(VideoRepo $repo)
    {

        $this->repo = $repo;
    }
    public function getAll()
    {
        $videos = $this->repo->search(null, []);
        return api_success($videos->toArray());
    }

    public function get($id)
    {
        /**
         * @var \Mona\Core\Video\Video $video
         */
        $video = $this->repo->get($id)->load('user','user.profile','thumb');
        return api_success($video);
    }

    public function create(ApiRequest $request, Authenticator $auth)
    {
        $auth->ensureItsMe($request->get("user_id"));
    }

    public function update($id, ApiRequest $request)
    {

    }

    public function delete($id)
    {

    }
}