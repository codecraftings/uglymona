<?php


namespace Mona\Http\Controllers\Api;


use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Mona\Core\User\Registration\NewUserRegistration;
use Mona\Core\User\Auth\LoginRequest;
use Mona\Core\User\UserRepo;
use Mona\Http\Controllers\BaseController;
use Mona\Http\Requests\ApiRequest;
use Mona\Http\Requests\AuthenticatedRequest;
use Mona\Http\Requests\SignupRequest;
use Mona\Core\User\Auth\AuthManager;

class AuthController extends ApiController
{
    protected $auth;

    public function __construct(AuthManager $auth)
    {
        $this->auth = $auth;
    }

    public function authPusher(AuthenticatedRequest $request, \Pusher $pusher)
    {
        $channel_name = $request->input("channel_name");
        $socket_id = $request->input("socket_id");
        $user = $request->user();
        if (ends_with($channel_name, "-" . $user->id)) {
            return $pusher->socket_auth($channel_name, $socket_id);
        } else {
            return api_error("Not Authorized", 403);
        }
    }

    public function logout(ApiRequest $request)
    {
        $this->auth->invalidate($request->input('token'));
        return api_success([]);
    }

    public function login(LoginRequest $request)
    {
        $data = $request->all();
        $token = $this->auth->createToken($data);
        if (!$token) {
            return api_error("Invalid Credentials");
        }
        $user = $this->auth->user($token)->load('profile');
        $expire_time = $this->auth->getExpireTime($token);
        return api_success(['access_token' => $token, 'token_expire_time' => $expire_time, 'user' => $user]);
    }

    /**
     * @param SignupRequest $request
     * @param UserRepo $userRepo
     * @return mixed
     */
    public function register(SignupRequest $request, UserRepo $userRepo)
    {
        $data = $request->all();
        $data['full_name'] = $data["username"];
        $user = $userRepo->create($data);
        if (!$user) {
            return api_error("Unknown Error!", 500);
        }
        //event(new NewUserRegistration($user));
        return api_success($user);
    }

    public function verifyEmail(ApiRequest $request, UserRepo $userRepo){
        try{
            $userRepo->verifyEmail($request->input('email'), $request->input('token'));
        }catch(\Exception $e){

        }
        return redirect(web_url("login")."?verified=1");
    }
    public function refreshToken(ApiRequest $request)
    {
        $oldToken = $request->input('token');
//        test($oldToken);
        $token = $this->auth->getNewToken($oldToken);
        $expireTime = $this->auth->getExpireTime($token);
        return api_success(['access_token' => $token, 'token_expire_time'=> $expireTime]);
    }

    public function me(ApiRequest $request)
    {
        return api_success($request->user());
    }
}