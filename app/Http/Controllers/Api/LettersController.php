<?php


namespace Mona\Http\Controllers\Api;


use Mona\Core\Letter\LetterCreateRequest;
use Mona\Core\Letter\LetterRepo;
use Mona\Http\Requests\ApiRequest;

class LettersController extends ApiController
{

    /**
     * @var LetterRepo
     */
    private $letter;

    public function __construct(LetterRepo $letter)
    {

        $this->letter = $letter;
    }

    public function getAll()
    {
        $letters = $this->letter->search(null, []);
        return api_success($letters->toArray());
    }

    public function get($id)
    {
        $letter = $this->letter->get($id);
        return api_success($letter);
    }

    public function create(LetterCreateRequest $request)
    {
        $letter = $this->letter->create($request->only(['user_id','message']));
        return api_success($letter);
    }

    public function update($id, ApiRequest $request)
    {

    }

    public function delete($id)
    {

    }
}