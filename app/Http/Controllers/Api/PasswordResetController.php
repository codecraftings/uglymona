<?php


namespace Mona\Http\Controllers\Api;


use Mona\Core\User\Auth\PasswordResetManager;
use Mona\Core\User\Auth\SetNewPasswordRequest;
use Mona\Http\Requests\ApiRequest;

class PasswordResetController extends ApiController {
    /**
     * @var PasswordResetManager
     */
    private $manager;

    public function __construct(PasswordResetManager $manager)
    {
        $this->manager = $manager;
    }
    public function requestResetLink(ApiRequest $request){
        if($this->manager->sendResetLink($request->input('email'))){
            return api_success("An email with reset link has been sent to your email");
        }
    }
    public function confirmPassReset(SetNewPasswordRequest $request){
        $data = $request->only(['email','token','password','password_confirmation']);
        if($this->manager->confirmPassReset($data)){
            return api_success("Password changed successfully. you can login now.");
        }
    }
    public function test(){
        throw new \Exception("Some thing went wront", 400);
    }
}