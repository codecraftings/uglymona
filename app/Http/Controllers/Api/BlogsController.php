<?php


namespace Mona\Http\Controllers\Api;


use Mona\Core\Blog\BlogsRepo;
use Mona\Http\Requests\ApiRequest;

class BlogsController extends ApiController
{
    /**
     * @var BlogsRepo
     */
    private $blogs;

    public function __construct(BlogsRepo $blogsRepo)
    {

        $this->blogs = $blogsRepo;
    }

    public function get($id){
        $blog = $this->blogs->get($id);
        $blog->load('category');
        return api_success($blog);
    }
    public function getAll(ApiRequest $request)
    {
        $filters = $request->only(["category"]);
        $blogs = $this->blogs->getAll($filters, $request->input("pagging", 20), $request->input("offset", 0));
        return api_success($blogs);
    }
    public function getCategories(){
        $cats = $this->blogs->getCategories();
        return api_success($cats);
    }
}