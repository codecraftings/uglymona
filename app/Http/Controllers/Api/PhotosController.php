<?php


namespace Mona\Http\Controllers\Api;


use Mona\Core\Photo\Photo;
use Mona\Core\Photo\PhotoCreateRequest;
use Mona\Core\Photo\PhotoRepo;
use Mona\Core\Photo\PhotoUpdateRequest;
use Mona\Core\User\Auth\AuthManager;

class PhotosController extends ApiController
{
    /**
     * @var PhotoRepo
     */
    private $photo;

    public function __construct(PhotoRepo $photo)
    {

        $this->photo = $photo;
    }

    public function getAll()
    {

    }

    public function get($id)
    {

    }

    public function create(PhotoCreateRequest $request, AuthManager $auth)
    {
        switch ($request->get("type")) {
            case Photo::TYPE_PROFILE_PIC:
                $photo = $this->photo->createProfilePic($request->get("source"), $request->get("user_id"));
                break;
            case Photo::TYPE_ALBUM_PHOTO:
            default:
                $photo = $this->photo->createAlbumPhoto($request->get("source"), $request->get("user_id"), $request->get("caption"));
                break;
        }
        return api_success($photo);
    }

    public function update($id, PhotoUpdateRequest $request)
    {

    }

    public function delete($id)
    {

    }
}