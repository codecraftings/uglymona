<?php


namespace Mona\Http\Controllers\Api;


use Mona\Core\Inbox\Inbox;
use Mona\Core\User\UserRepo;
use Mona\Http\Requests\ApiRequest;
use Mona\Core\User\Auth\AuthManager;
use Mona\Http\Requests\AuthenticatedRequest;

class InboxController extends ApiController
{
    /**
     * @var AuthManager
     */
    private $auth;
    /**
     * @var Inbox
     */
    private $inbox;

    public function __construct(AuthManager $auth, Inbox $inbox)
    {

        $this->auth = $auth;
        $this->inbox = $inbox;
    }

    public function getUnreadMessageCount(ApiRequest $request)
    {
        $user = $request->user();
        $count = $this->inbox->getUnreadMessageCount($user);
        return api_success(['count' => $count]);

    }

    public function getLatestConversations(ApiRequest $request)
    {
        $user = $request->user();
        $conversations = $this->inbox->getLatestConversations($user);
        return api_success($conversations);
    }

    public function getConversationMessages(ApiRequest $request, $id)
    {
        $user = $request->user();
        $conversation = $this->inbox->getConversation($id);
        $messages = $this->inbox->getConversationMessages($conversation);
        if (!$request->has('silent')) {
            $this->inbox->setConversationRead($conversation, $user);
        }
        return api_success($messages);
    }

    public function sendMessage(ApiRequest $request, UserRepo $userRepo)
    {
        $user = $request->user();
        $otherUser = $userRepo->get($request->input('receiver_id'));
        $msg = $request->input('message');
        $message = $this->inbox->createMessage($user, $otherUser, $msg);
        return api_success($message);
    }
}