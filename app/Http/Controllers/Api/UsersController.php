<?php


namespace Mona\Http\Controllers\Api;


use Mona\Core\Services\Search\StringParser;
use Mona\Core\User\Billing\BillingInterface;
use Mona\Core\User\Matching\MatchFinder;
use Mona\Core\User\MembershipUpgradRequest;
use Mona\Core\User\Subscription\FullMembershipSubscriptionDelegate;
use Mona\Core\User\Subscription\ProcessPaymentJob;
use Mona\Core\User\Subscription\SubscriptionManager;
use Mona\Core\User\UserRepo;
use Mona\Core\User\UserUpdateRequest;
use Mona\Http\Requests\ApiRequest;
use Mona\Core\User\Auth\AuthManager;

class UsersController extends ApiController
{
    /**
     * @var UserRepo
     */
    private $user;
    /**
     * @var AuthManager
     */
    private $auth;

    public function __construct(UserRepo $user, AuthManager $auth)
    {
        $this->user = $user;
        $this->auth = $auth;
    }

    public function search(ApiRequest $request, StringParser $parser)
    {
        $filters = $request->all();
        if ($request->user()) {
            $filters['exclude'] = [$request->user()->id];
        }
        if (isset($filters['searchString'])) {
            $arr = $parser->convertToArray($filters['searchString']);
            $filters = array_merge($filters, $arr);
            // return $filters;
        }
        $users = $this->user->search($filters, 20, $request->get('page', 1));
        return api_success($users->toArray());
    }

    public function getMatch(ApiRequest $request, MatchFinder $matchMaker)
    {
        $matchMaker->setUser($request->user());
        $match = $matchMaker->findOne();
        return api_success(['data' => $match->toArray()]);
    }

    public function get($id, UserRepo $userRepo)
    {
        $user = $userRepo->get($id)->load(['profile', 'photos', 'videos', 'videos.thumb']);
        return api_success($user);
    }

    public function update($id, UserUpdateRequest $request, UserRepo $userRepo)
    {
        $user = $userRepo->get($id);
        $user = $userRepo->update($user, $request->all());
        if (!$user) {
            return api_error("Unknown Error", 500);
        }
        return api_success($user);
    }

    public function upgradeMembership(MembershipUpgradRequest $request, BillingInterface $billing, SubscriptionManager $subscriptions, UserRepo $users)
    {
        $billing->attachCard($request->user(), [
            "card_token" => $request->input('card_token'),
            "last_four"  => $request->input('last_four'),
        ]);
        $subscription = $subscriptions->create($request->user(), FullMembershipSubscriptionDelegate::class, []);
        $this->dispatch(new ProcessPaymentJob($subscription));

        $user = $users->get($request->user()->id)->load('profile');
        return api_success(['user' => $user]);
    }
}