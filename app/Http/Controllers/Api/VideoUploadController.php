<?php


namespace Mona\Http\Controllers\Api;


use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Mona\Core\Video\Uploader;
use Mona\Http\Requests\ApiRequest;

class VideoUploadController extends ApiController
{
    /**
     * @var Uploader
     */
    private $uploader;

    public function __construct(Uploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public function testChunkExists(ApiRequest $request)
    {
        $chunk_number = $request->input("chunk_number");
        $file_id = $request->input("file_id");
        if ($this->uploader->chunkExists($file_id, $chunk_number)) {
            return api_error("chunk exists");
        }
        return api_success("chunk doesnot exists");
    }

    public function acceptChunk(ApiRequest $request)
    {
        $chunk_number = $request->input("chunk_number");
        $file_id = $request->get("file_id");
        $this->uploader->ensureChunkDoesNotExist($file_id, $chunk_number);
        $this->uploader->ensureChunkIsValid($request->file("filepart"));
        $this->uploader->acceptChunk($request->file("filepart"), $file_id, $chunk_number);
        return api_success("chunk accepted");
    }

    public function assembleFile(ApiRequest $request)
    {
        $file_id = $request->input("file_id");
        $total_chunks = $request->input("total_chunks");
        $expected_filesize = $request->input("expected_filesize");
        $this->uploader->ensureAllChunkReceived($file_id, $total_chunks);
        $filepath = $this->uploader->assembleChunks($file_id, $total_chunks, $expected_filesize);
        return api_success(['filePath' => $filepath]);
    }
}