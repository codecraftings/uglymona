<?php


namespace Mona\Http;


use Mona\Core\User\Membership;

class Route {
    /**
     * @var \Illuminate\Routing\Route
     */
    private $route;
    private $actions = [];
    public function __construct(\Illuminate\Routing\Route $route)
    {

        $this->route = $route;
        $this->actions = $route->getAction();
    }
    public function getPermissions(){
        if (isset($this->actions['permissions'])) {
            return $this->actions['permissions'];
        } else {
            return [];
        }
    }
    public function getAccessLevel(){
        if (isset($this->actions['access'])) {
            return $this->actions['access'];
        } else {
            return Membership::BASIC;
        }
    }
}