<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
/**/
use Illuminate\Support\Facades\Config;
use Mona\Core\User\Membership;

//$monolog = \Log::getMonolog();
//$syslog = new \Monolog\Handler\SyslogHandler('papertrail');
//$formatter = new \Monolog\Formatter\LineFormatter('%channel%.%level_name%: %message% %extra%');
//$syslog->setFormatter($formatter);
//$monolog->pushHandler($syslog);

Route::post('queue/receive', function () {
    return ;
});
Route::get('/', 'PagesController@showLanding');
Route::pattern('id', "[0-9]+");

Route::get('test', 'PagesController@test');
Route::group(["prefix" => "api/v1", "namespace" => "Api", "middleware" => ["auth"]], function () {
    Route::post("login", ['uses' => 'AuthController@login', 'access' => Membership::NONE]);
    Route::post("signup", ['uses' => 'AuthController@register', 'access' => Membership::NONE]);
    Route::get('verify/email',['uses'=>'AuthController@verifyEmail', 'access'=>Membership::NONE]);

    Route::post("token/refresh", ['uses' => 'AuthController@refreshToken', 'access' => Membership::NONE]);
    Route::post("logout", ['uses' => 'AuthController@logout', 'access' => Membership::NONE]);
    Route::post("password/reset",['uses'=>'PasswordResetController@requestResetLink','access'=>Membership::NONE]);
    Route::post("password/reset/confirm",['uses'=>'PasswordResetController@confirmPassReset','access'=>Membership::NONE]);
    Route::get("password/test",['uses'=>'PasswordResetController@test','access'=>Membership::NONE]);
    Route::get("me", 'AuthController@me');
    Route::post("auth/pusher", ['uses'=>'AuthController@authPusher', 'access'=>Membership::NONE]);

    Route::get("photos", 'PhotosController@getAll');
    Route::get("photos/{id}", 'PhotosController@get');
    Route::post("photos", 'PhotosController@create');
    Route::post("photos/{id}", 'PhotosController@update');
    Route::delete("photos/{id}", 'PhotosController@delete');

    Route::get("users", 'UsersController@getAll');
    Route::post("users/search", ['uses' => 'UsersController@search', 'access' => Membership::NONE]);
    Route::get("users/{id}", 'UsersController@get');
    Route::post("users/{id}", 'UsersController@update');

    Route::get("letters", 'LettersController@getAll');
    Route::get("letters/{id}", 'LettersController@get');
    Route::post("letters", 'LettersController@create');
    Route::post("letters/{id}", 'LettersController@update');
    Route::delete("letters/{id}", 'LettersController@delete');

    Route::get("videos", 'VideosController@getAll');
    Route::get("videos/{id}", 'VideosController@get');
    Route::post("videos", 'VideosController@create');
    Route::post("videos/{id}", 'VideosController@update');
    Route::delete("videos/{id}", 'VideosController@delete');
    Route::post('videos/upload/chunk', 'VideoUploadController@acceptChunk');
    Route::post('videos/test/chunk', 'VideoUploadController@testChunkExists');
    Route::post("videos/upload/assemble", 'VideoUploadController@assembleFile');

    Route::get("messages/count/unread", 'InboxController@getUnreadMessageCount');
    Route::post("messages", 'InboxController@sendMessage');
    Route::get("messages/conversation/{id}",'InboxController@getConversationMessages');

    Route::get("conversations",'InboxController@getLatestConversations');



    Route::get('profile-wheel/find-match', array('uses' => 'UsersController@getMatch'));
    Route::get('categories', 'BlogsController@getCategories');
    Route::get('blogs', 'BlogsController@getAll');
    Route::get('blogs/{id}', 'BlogsController@get');

    Route::post("profile/upgrade", array('uses'=>'UsersController@upgradeMembership'));
});
/**/