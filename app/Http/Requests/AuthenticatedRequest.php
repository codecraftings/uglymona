<?php


namespace Mona\Http\Requests;


use Mona\Core\User\Auth\AuthManager;

class AuthenticatedRequest extends ApiRequest {
    public function __construct(AuthManager $auth)
    {
        parent::__construct($auth);
    }
}