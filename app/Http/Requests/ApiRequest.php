<?php namespace Mona\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Mona\Core\User\Auth\AuthManager;
use Mona\Core\User\User;

class ApiRequest extends FormRequest
{
    /**
     * @var AuthManager
     */
    protected $auth;

    /**
     * @param User $user
     */
    protected $user;

    protected $access_token;

    public function __construct(AuthManager $auth)
    {
        $this->auth = $auth;
        $this->user = null;
        $this->access_token = null;
    }

    public function rules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function forbiddenResponse()
    {
        return api_error("You dont have permission", 403);
    }

    public function response(array $errors)
    {
        $errors = array_flatten($errors);
        if (count($errors) > 1) {
            $errors = implode(" #", $errors);
        } else {
            $errors = $errors[0];
        }
        return api_error($errors, 422);
    }

    public function user()
    {
        if ($this->user==null) {
            try{
                $this->user = $this->auth->user($this->access_token());
            }catch(\Exception $e){
                $this->user = false;
            }
        }
        return $this->user;
    }

    public function access_token()
    {
        if ($this->access_token==null) {
            try{
                $this->access_token = $this->auth->parseToken($this);
            }catch(\Exception $e){
                $this->access_token = false;
            }
        }
        return $this->access_token;
    }
}
