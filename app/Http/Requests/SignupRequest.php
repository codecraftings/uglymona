<?php


namespace Mona\Http\Requests;


class SignupRequest extends ApiRequest {
    public function rules(){
        return [
            'username' => 'required|unique:users,username',
            'email'    => 'sometimes|required|email|unique:users,email',
            'password' => 'required|confirmed',
            'terms'     => 'required|accepted'
        ];
    }
}