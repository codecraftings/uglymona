<?php namespace Mona\Services;

class UploadPathGenerator{
	private $baseUploadDir = "uploads";
    private $photosDir = "photos";
    private $videosDir = "videos";

    public function getPathForNewPhoto(){
        $basePath = $this->baseUploadDir."/".$this->photosDir;
        $dir1 = rand(1, 9);
        $dir2 = rand(1,9);
        return $basePath."/".$dir1."/".$dir2."/";
    }
    public function getPathForNewVideo(){
        $basePath = $this->baseUploadDir."/".$this->videosDir;
        $dir1 = rand(1, 9);
        $dir2 = rand(1,9);
        return $basePath."/".$dir1."/".$dir2."/";
    }
}