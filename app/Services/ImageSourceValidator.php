<?php


namespace Mona\Services;


use Illuminate\Validation\Validator;
use Intervention\Image\ImageManager;
use Symfony\Component\Translation\TranslatorInterface;

class ImageSourceValidator extends Validator {
    /**
     * @var ImageManager
     */
    private $imageManager;

    public function __construct(ImageManager $imageManager, TranslatorInterface $translator, $data, $rules, $messages){
        $this->imageManager = $imageManager;
        parent::__construct($translator, $data, $rules, $messages);
    }
    public function validateImageSource($attribute, $imageData, $params){
        try{
            $img = $this->imageManager->make($imageData);
        }catch (\Exception $e){
            return false;
        }
        if($img->height()>0&&$img->width()>0&&$img->mime()){
            return true;
        }
        return false;
    }
}