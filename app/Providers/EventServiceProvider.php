<?php namespace Mona\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Mona\Core\User\Billing\BillingWasSuccessful;
use Mona\Core\User\Billing\SendBillingInvoice;
use Mona\Core\User\Registration\SendVerificationEmail;
use Mona\Events\NewUserRegistration;

class EventServiceProvider extends ServiceProvider
{

    /**
     * The event handler mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Mona\Core\User\Registration\NewUserRegistration' => [
            SendVerificationEmail::class
        ],
        'Mona\Core\User\Billing\BillingWasSuccessful'         => [
            SendBillingInvoice::class
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }

}
