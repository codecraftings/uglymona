<?php namespace Mona\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Mona\Services\ImageSourceValidator;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		\Validator::resolver(function($translator, $data, $rules, $messages){
			return new ImageSourceValidator($this->app->make('Intervention\Image\ImageManager'),$translator, $data, $rules, $messages);
		});
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton("Pusher", function($app){
			$keys = $app["config"]->get("broadcasting.connections.pusher");
			return new \Pusher($keys["key"], $keys["secret"], $keys["app_id"]);
		});

		\Blade::setRawTags('{{', '}}');
		\Blade::setContentTags('{{{', '}}}');
		\Blade::setEscapedContentTags('{!!', '!!}');
	}

}
