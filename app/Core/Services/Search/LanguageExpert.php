<?php


namespace Mona\Core\Services\Search;


class LanguageExpert
{
    public function validGenderName($word)
    {
        $patterns = [
            "man"   => "%man%male%young%boy%men%boys%youngs%",
            "woman" => "%woman%girl%female%women%girls%females%"
        ];
        return $this->matchAgainst($patterns, "%" . $word . "%");
    }

    public function validAge($word)
    {
        if (is_numeric($word) && strlen($word) < 3) {
            return $word;
        }
    }

    public function validZipCode($word)
    {
        if (is_numeric($word) && strlen($word) > 3) {
            return $word;
        }
    }

    public function validCountryName($word)
    {
        $patterns = [
            "usa"    => "%america%american%usa%u.s.a%us%u.s.%",
            "uk"     => "%england%english%british%uk%",
            "canada" => "%canada%"
        ];
        return $this->matchAgainst($patterns, "%" . $word . "%");
    }

    public function validCityName($word)
    {
        $city_expert = new CityAndStateExpert;
        $cities = $city_expert->getPopularCities() . $city_expert->getUSACities();
        if (str_contains($cities, "%" . $word . "%")) {
            return $word;
        }
    }

    public function validStateName($word)
    {
        $state_expert = new CityAndStateExpert;
        $states = $state_expert->getStates();
        if (str_contains($states, "%" . $word . "%")) {
            return $word;
        }
    }

    public function validForName($word){
        $common_omit = "%years%more%than%anywhere%named%places%lived%near%world%country%";
        if(strlen($word)>3&&!str_contains($common_omit, "%".$word."%")){
            return true;
        }
    }
    private function matchAgainst($patterns, $word)
    {
        $match = false;
        foreach ($patterns as $key => $pattern) {
            if (str_contains($pattern, $word)) {
                $match = $key;
                break;
            }
        }
        return $match;
    }
}