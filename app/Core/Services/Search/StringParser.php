<?php


namespace Mona\Core\Services\Search;


class StringParser {

    /**
     * @var
     */
    private $expert;

    public function __construct(LanguageExpert $expert)
    {
        $this->expert = $expert;
    }
    public function convertToArray($str){
        $str = preg_replace('/[\(\)\-\[\]\_%,]/i', "", $str);
        $str = strtolower($str);
        $words = explode(" ", $str);
        $filters = ["full_name"=>""];
        foreach($words as $word){
            $result = $this->checkForPossibleMatch($word);
            if(false !=$result){
                $filters[$result['key']] = $result['value'];
            }else{
                if($this->validForName($word)){
                    $filters["full_name"] .= " ".$word;
                }
            }
        }
        $filters["full_name"] = trim($filters["full_name"]);
        return $filters;
    }
    private function validForName($word){
        return $this->expert->validForName($word);
    }
    private function checkForPossibleMatch($word){
        $key = "";
        $value = "";
        if($value = $this->expert->validGenderName($word)){
            $key = "gender";
        }
        elseif($value = $this->expert->validAge($word)){
            $key = "age";
        }
        elseif($value = $this->expert->validZipCode($word)){
            $key = "zip";
        }
        elseif($value = $this->expert->validCountryName($word)){
            $key = "country";
        }
        elseif($value = $this->expert->validCityName($word)){
            $key = "city";
        }
        elseif($value = $this->expert->validStateName($word)){
            $key = "state";
        }
        else{
            return false;
        }
        return compact("key","value");
    }
}