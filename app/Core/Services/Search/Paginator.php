<?php
namespace Mona\Core\Services\Search;

use Illuminate\Pagination\LengthAwarePaginator as LaravelPaginator;
class Paginator{
	public function __construct(){
	}
	public function make($query, $pagging=10, $page=1){
		$total = $query->count();
		$items = $query->skip(($page-1)*$pagging)->take($pagging)->get()->all();
		$paginated = new LaravelPaginator($items, $total, $pagging, $page);
		return $paginated;
	}
}