<?php
namespace Mona\Core\Services\Mail;

use Illuminate\Contracts\Mail\Mailer;
use League\Flysystem\Exception;
use Mona\Core\User\User;
use Illuminate\Support\MessageBag;
use Mona\Core\User\UserRepo;

class Mail
{
    protected $mailer;
    protected $admin_email;
    protected $no_reply_email;
    protected $user;
    protected $errors;

    public function __construct(Mailer $mailer, MessageBag $bag, UserRepo $user)
    {
        $this->user = $user;
        $this->mailer = $mailer;
        $this->admin_email = "contact@uglymona.com";
        $this->errors = $bag;
    }

    public function sendVerificationEmail(User $user)
    {
        \Log::info("Sending verification email to {$user->email}");
        $user = $this->user->get($user);
        if (!$user || $user->email_verified()) {
            $this->errors->add('verification', 'Invalid user!');
            return false;
        }
        \Log::info("here");
        try {
            $res = $this->mailer->send('emails.auth.email-verification', compact('user'), function ($message) use ($user) {
                \Log::info("inside closure");
                $message->to($user->email, $user->name);
                $message->subject('Confirm your email');
            });
        }catch(Exception $e){
            \Log::error($e);
        }
		\Log::info($res);
	}

    public function sendContactEmail($name, $email, $subject, $msg_body)
    {
        $data = compact('name', 'email', 'subject', 'msg_body');
        $this->mailer->send('emails.user-contact', $data, function ($message) use ($email, $name, $subject) {
            $message->to($this->admin_email, 'Uglymona Admin');
            $message->from($email, $name);
            $message->subject($subject);
        });
        return true;
    }

    public function errors()
    {
        return $this->errors;
    }
}