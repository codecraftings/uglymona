<?php

namespace Mona\Core\Inbox;
/*

*/
use Mona\Core\BaseModel;

/**
 * Class Message
 * @package Mona\Core\Inbox
 * @property integer id
 * @property integer sender_id*
 * @property interger receiver_id
 * @property string message
 * @property string status
 * @property integer conversation_id
 * @property DateTime updated_at
 * @property DateTime created_at
 */
class Message extends BaseModel{
	const STATUS_READ = "read";
	const STATUS_UNREAD = "unread";

	protected $table = "messages";
	protected $fillable = array('sender_id','receiver_id','conversation_id','message', 'status');
	protected $appends = ['age'];
	public function getAgeAttribute(){
		return $this->created_at->diffForHumans();
	}
	public function sender(){
		return $this->belongsTo('Mona\Core\User\User','sender_id','id');
	}
	public function receiver(){
		return $this->belongsTo('Mona\Core\User\User',"receiver_id","id");
	}
}