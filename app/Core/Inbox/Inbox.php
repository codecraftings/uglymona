<?php


namespace Mona\Core\Inbox;


use Mona\Core\User\User;

class Inbox
{
    /**
     * @var Message
     */
    private $message;
    /**
     * @var Conversation
     */
    private $conversation;

    /**
     * @param Message $message
     * @param Conversation $conversation
     */
    public function __construct(Message $message, Conversation $conversation)
    {
        $this->message = $message;
        $this->conversation = $conversation;
    }

    public function getUnreadMessageCount(User $user)
    {
        return $this->message->where('receiver_id', '=', $user->id)->where('status', '=', Message::STATUS_UNREAD)->count();
    }

    public function getLatestConversations(User $user)
    {
        $q = $this->conversation->where(function ($q) use ($user) {
            $q->where("user1_id", '=', $user->id);
            $q->orWhere("user2_id", '=', $user->id);
        })->orderBy("updated_at", "desc");
        return $q->take(100)->get();
    }

    public function getConversation($id)
    {
        return $this->conversation->findOrFail($id);
    }

    public function setConversationRead(Conversation $conversation, User $reader)
    {
        return $this->message->query()->where('conversation_id', '=', $conversation->id)
            ->where('receiver_id', '=', $reader->id)
            ->where('status', Message::STATUS_UNREAD)
            ->update(['status' => Message::STATUS_READ]);
    }

    public function getConversationMessages(Conversation $conversation)
    {
        $q = $this->message->where('conversation_id', '=', $conversation->id)->with('sender.profile', 'receiver.profile')->orderBy('created_at', 'desc');
        return $q->take(30)->get();
    }

    public function getConversationBetweenUsers(User $user1, User $user2)
    {
        $q = $this->conversation->query()->where(function ($q) use ($user1, $user2) {
            $q->where('user1_id', '=', $user1->id)
                ->where('user2_id', '=', $user2->id);
        })->orWhere(function ($q) use ($user1, $user2) {
            $q->where('user2_id', '=', $user1->id)
                ->where('user1_id', '=', $user2->id);
        });
        if($q->count()<1){
            return false;
        }
        return $q->first();
    }
    public function createConversation(User $user1, User $user2){
        return $this->conversation->create([
            "user1_id" => $user1->id,
            "user2_id" => $user2->id,
        ]);
    }
    public function createMessage(User $user, User $otherUser, $message)
    {
        if(!($conversation=$this->getConversationBetweenUsers($user, $otherUser))){
            $conversation = $this->createConversation($user, $otherUser);
        }else{
            $conversation->touch();
        }
        $message = $this->sanitaizeMessageText($message);
        $data = [
            'sender_id'       => $user->id,
            'receiver_id'     => $otherUser->id,
            'conversation_id' => $conversation->id,
            'message'         => $message,
            'status'          => Message::STATUS_UNREAD
        ];
        $message = $this->message->create($data);
        $message->load('sender.profile','receiver.profile');
        event(new NewMessageReceived($message));
        return $message;
    }

    public function getMessage($id)
    {
        return $this->message->findOrFail($id);
    }

    public function changeStatus(Message $message, User $user, $status)
    {
        if ($message->receiver_id != $user->id) {
            throw new \Exception("Cannot change status");
        }
        $message->status = $status;
        return $message->save();
    }

    public function setStatusRead(array $message_ids, User $user)
    {
        foreach ($message_ids as $message_id) {
            $message = $this->get($message_id);
            $this->changeStatus($message, $user, Message::STATUS_READ);
        }
        return true;
    }


    private function sanitaizeMessageText($message){
        return e($message);
    }
}