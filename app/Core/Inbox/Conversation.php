<?php


namespace Mona\Core\Inbox;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Conversation
 * @package Mona\Core\Inbox
 *
 */
class Conversation extends Model
{
    protected $table = "conversations";
    protected $appends = ['last_message'];
    protected $fillable = ['user1_id', 'user2_id'];

    public function getLastMessageAttribute()
    {
        return $this->messages()->orderBy('id', 'desc')->first()->load('sender.profile', 'receiver.profile');
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }
}