<?php


namespace Mona\Core\Inbox;


use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Mona\Events\Event;

class NewMessageReceived extends Event implements ShouldBroadcast
{

    /**
     * @var Message
     */
    public $message;

    public function __construct(Message $message)
    {

        $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [
            "private-user-" . $this->message->receiver_id,
            "private-user-" . $this->message->sender_id
        ];
    }
}