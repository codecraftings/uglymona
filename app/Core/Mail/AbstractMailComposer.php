<?php


namespace Mona\Core\Mail;


use Illuminate\Contracts\Queue\QueueableEntity;
use JsonSerializable;
use Serializable;

abstract class AbstractMailComposer implements MailComposerInterface, Serializable
{
    public function getSenderEmail()
    {
        return "admin@uglymona.com";
    }

    public function getSenderName()
    {
        return "Uglymona Support";
    }

    public abstract function getSubject();

    public abstract function getReceiverEmail();

    public abstract function getReceiverName();

    public abstract function getBodyView();

    public abstract function getBodyData();
}