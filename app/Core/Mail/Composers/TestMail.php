<?php


namespace Mona\Core\Mail\Composers;


use Mona\Core\Mail\AbstractMailComposer;

class TestMail extends AbstractMailComposer{

    public function getSubject()
    {
        "test mail";
    }

    public function getReceiverEmail()
    {
        return "makhan075@gmail.com";
    }

    public function getReceiverName()
    {
        return "manfuz";
    }

    public function getBodyView()
    {
        return "emails.user-contact";
    }

    public function getBodyData()
    {
        return [
            'msg_body'=> 'ddd'
        ];
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return json_encode($this);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        json_encode($serialized);
    }
}