<?php


namespace Mona\Core\Mail\Composers;


use Mona\Core\Mail\AbstractMailComposer;
use Mona\Core\User\Billing\Transaction;

class InvoiceEmail extends AbstractMailComposer
{

    /**
     * @var Transaction
     */
    public $transaction;

    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    public function getSubject()
    {
        return "Billing Receipt | Uglymona";
    }

    public function getReceiverEmail()
    {
        return $this->transaction->user->email;
    }

    public function getReceiverName()
    {
        return $this->transaction->user->name;
    }

    public function getBodyView()
    {
        return "emails.billing.invoice";
    }

    public function getBodyData()
    {
        return [
            'transaction' => $this->transaction
        ];
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return json_encode($this);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        $obj = json_decode($serialized);
        $this->transaction = $obj->transaction;
    }
}