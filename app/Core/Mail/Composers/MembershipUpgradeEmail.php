<?php


namespace Mona\Core\Mail\Composers;


use Mona\Core\Mail\AbstractMailComposer;
use Mona\Core\User\Subscription\Subscription;

class MembershipUpgradeEmail extends AbstractMailComposer {

    /**
     * @var Subscription
     */
    public $subscription;

    public function __construct(Subscription $subscription)
    {
        $this->subscription = $subscription;
    }
    public function getSubject()
    {
        return "You now have full access";
    }

    public function getReceiverEmail()
    {
        return $this->subscription->user->email;
    }

    public function getReceiverName()
    {
        return $this->subscription->user->name;
    }

    public function getBodyView()
    {
        return "emails.membership-upgrade";
    }

    public function getBodyData()
    {
        return ['subscription'=>$this->subscription];
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return json_encode($this);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        $obj = json_decode($serialized);
        $this->subscription = $obj->subscription;
    }
}