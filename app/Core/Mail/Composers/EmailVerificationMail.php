<?php


namespace Mona\Core\Mail\Composers;


use Mona\Core\Mail\AbstractMailComposer;
use Mona\Core\User\User;

class EmailVerificationMail extends AbstractMailComposer
{
    /**
     * @var User
     */
    public $user;

    /**
     * @param User $user
     * @throws \Exception
     */
    public function __construct(User $user)
    {
        if (!$user || $user->email_verified()) {
            throw new \Exception("User already verified!");
        }
        $this->user = $user;
    }

    public function getSubject()
    {
        return "Confirm Your Email";
    }

    public function getReceiverEmail()
    {
        return $this->user->email;
    }

    public function getReceiverName()
    {
        return $this->user->name;
    }

    public function getBodyView()
    {
        return 'emails.auth.email-verification';
    }

    public function getBodyData()
    {
        return [
            'user' => $this->user
        ];
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return json_encode($this);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        $o = json_decode($serialized);
        $this->user = $o->user;
    }
}