<?php


namespace Mona\Core\Mail;


use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Log;

class Mailer
{
    use DispatchesJobs;
    /**
     * @var \Illuminate\Contracts\Mail\Mailer
     */
    private $mailer;

    public function __construct(\Illuminate\Contracts\Mail\Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function send(MailComposerInterface $mail)
    {
        $this->mailer->send($mail->getBodyView(), $mail->getBodyData(), function ($message) use ($mail) {
            $message->from($mail->getSenderEmail(), $mail->getSenderName());
            $message->to($mail->getReceiverEmail(), $mail->getReceiverName());
            $message->subject($mail->getSubject());
        });
    }
    public function post(MailComposerInterface $mail){
        $this->dispatch(new PostMan($mail));
    }
}