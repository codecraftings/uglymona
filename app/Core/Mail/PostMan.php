<?php


namespace Mona\Core\Mail;


use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Mona\Jobs\Job;

class PostMan extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var MailComposerInterface
     */
    public $mail;

    public function __construct(MailComposerInterface $mail)
    {
        $this->mail = $mail;
    }

    public function handle(Mailer $mailer)
    {
        $mailer->send($this->mail);
    }
}