<?php


namespace Mona\Core\Mail;


interface MailComposerInterface {
    public function getSubject();
    public function getSenderEmail();
    public function getSenderName();
    public function getReceiverEmail();
    public function getReceiverName();
    public function getBodyView();
    public function getBodyData();
}