<?php
namespace Mona\Core\Photo;

use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Support\Facades\Config;
use Intervention\Image\ImageManager;
use Mona\Services\UploadPathGenerator;

class PhotoRepo
{
    protected $photo;
    /**
     * @var UploadPathGenerator
     */
    private $pathGenerator;
    /**
     * @var Factory
     */
    private $storage;
    /**
     * @var ImageManager
     */
    private $imageManager;

    public function __construct(Photo $photo, UploadPathGenerator $pathGenerator, Factory $storage, ImageManager $imageManager)
    {
        $this->photo = $photo;
        $this->pathGenerator = $pathGenerator;
        $this->storage = $storage;
        $this->imageManager = $imageManager;
    }

    public function get($id)
    {
        return $this->photo->findOrFail($id);
    }

    public function createVideoThumbPhoto($imageData, $user_id)
    {
        return $this->create($imageData, $user_id, Photo::TYPE_VIDEO_THUMB, "Video Thumbnail");
    }

    public function createAlbumPhoto($imageData, $user_id, $caption)
    {
        return $this->create($imageData, $user_id, Photo::TYPE_ALBUM_PHOTO, $caption);
    }

    public function createProfilePic($imageData, $user_id)
    {
        return $this->create($imageData, $user_id, Photo::TYPE_PROFILE_PIC, "Profile Pic");
    }

    private function create($imageData, $user_id, $type, $caption = "")
    {
        $path = $this->pathGenerator->getPathForNewPhoto();
        $data = [
            'user_id'         => $user_id,
            'type'            => $type,
            'caption'         => $caption,
            'status'          => Photo::STATUS_DRAFT,
            'available_sizes' => [],
            'source_path'     => $path,
            'cdn_name'        => Config::get("filesystems.default")
        ];
        $photo = $this->photo->create($data);
        if (!$photo) {
            throw new \Exception("Error creating photo");
        }
        $this->saveOriginalImage($photo, $path, $imageData);
        return $photo;
    }

    private function saveOriginalImage($photo, $path, $imageData)
    {
        $filename = PhotoSizes::getFileNameForSize("original", $photo->id);
        if (!$this->validImage($imageData)) {
            throw new \Exception("Image not valid");
        }
        $imageData = $this->decodeImage($imageData);
        $this->storage->put($path . $filename, $imageData);
        $photo->addAvailableSize("original");
        $photo->status = Photo::STATUS_PUBLISHED;
        $photo->save();
    }

    private function validImage($imageData)
    {
        try {
            $img = $this->imageManager->make($imageData);
        } catch (\Exception $e) {
            return false;
        }
        if ($img->height() > 0 && $img->width() > 0 && $img->mime()) {
            return true;
        }
        return false;
    }

    private function isValidImage($imageData)
    {
        $img = imagecreatefromstring($imageData);
        if (!$img) {
            return false;
        }
        $tempFileName = storage_path() . '/app/' . rand(0, 2000) . '.png';
        try {
            imagepng($img, $tempFileName);
        } catch (\Exception $e) {
            return false;
        }
        $info = getimagesize($tempFileName);
        unlink($tempFileName);
        if ($info[0] > 0 && $info[1] > 0 && $info['mime']) {
            return true;
        }
        return false;
    }

    private function decodeImage($imageData)
    {
        if ($pos = strpos($imageData, ",")) {
            $imageData = substr($imageData, $pos + 1);
        }
        $decoded = base64_decode($imageData);
        if (!$decoded)
            return $imageData;
        else
            return $decoded;
    }

    public function update($photo_id, $data)
    {
        $photo = $this->get($photo_id);
        if ($photo) {
            $photo->update($data);
            return $photo_id;
        }
    }
}