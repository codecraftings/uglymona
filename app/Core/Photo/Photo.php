<?php
namespace Mona\Core\Photo;

use Mona\Core\BaseModel;
/**
 * class Photo
 * @package Mona\Core\Photo
 * @property integer id
 * @property string user_id
 * @property string caption
 * @property string type
 * @property string status
 * @property string source_path
 * @property string cdn_name
 * @property array available_sizes
 * @property array url
 * @property DateTime created_at
 * @property DateTime updated_at
 */
class Photo extends BaseModel
{
    const TYPE_PROFILE_PIC = "profile_pic";
    const TYPE_VIDEO_THUMB = "video_thumb";
    const TYPE_ALBUM_PHOTO = "album_photo";
    const STATUS_DRAFT = "draft";
    const STATUS_PUBLISHED = "published";
    protected $table = "photos";
    protected $fillable = array('user_id', 'caption', 'type', 'status', 'source_path', 'cdn_name', 'available_sizes');
    protected $appends = array('url');
    protected $casts = [
        'available_sizes' => 'array'
    ];

    public function addAvailableSize($size)
    {
        $sizes = $this->available_sizes;
        if (false === array_search($size, $sizes)) {
            array_push($sizes, "original");
            $this->available_sizes = $sizes;
        }
    }

    public function getUrlAttribute()
    {
        $all_sizes = PhotoSizes::getSizeNames();
        $available_sizes = $this->available_sizes;
        $urls = [];
        $url = "";
        foreach ($all_sizes as $size) {
            if (false === array_search($size, $available_sizes)) {
                $urls[$size] = $url;
            } else {
                $name = PhotoSizes::getFileNameForSize($size, $this->id);
                $path = $this->source_path . $name;
                $url = asset_url($path, $this->cdn_name);
                $urls[$size] = $url;
            }
        }
        return $urls;
    }

}