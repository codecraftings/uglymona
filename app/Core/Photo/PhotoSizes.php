<?php
namespace Mona\Core\Photo;

class PhotoSizes{
	public static $array = [
	'original'	=> [0,0],
	'300x300' => [300,300],
	'200x200' => [200, 200]
	];
	public static function getSizeNames(){
		return array_keys(self::$array);
	}
	public static function getFileNameForSize($size, $id, $extension="jpg"){
		return $id.".".$size.".".$extension;
	}
}