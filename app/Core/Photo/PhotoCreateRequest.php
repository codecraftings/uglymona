<?php
namespace Mona\Core\Photo;


use Mona\Http\Requests\AuthenticatedRequest;

class PhotoCreateRequest extends AuthenticatedRequest
{
    public function rules()
    {
        return [
            "source"  => "required|image_source",
            "user_id" => "required",
            "type"    => "required|in:" . Photo::TYPE_ALBUM_PHOTO . "," . Photo::TYPE_PROFILE_PIC,
        ];
    }

    public function authorize()
    {
        return $this->user()->hasPermission('create_photo') && $this->user()->id == $this->input('user_id');
    }

}