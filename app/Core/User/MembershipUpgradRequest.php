<?php


namespace Mona\Core\User;


use Mona\Http\Requests\AuthenticatedRequest;

class MembershipUpgradRequest extends AuthenticatedRequest {
    public function rules(){
        return [
            "card_token" => 'required'
        ];
    }
}