<?php
namespace Mona\Core\User\Registration;

use Illuminate\Contracts\Queue\ShouldQueue;
use Mona\Core\Mail\Composers\EmailVerificationMail;
use Mona\Core\Mail\Mailer;

class SendVerificationEmail implements ShouldQueue
{

    protected $mailer;

    /**
     * Create the event handler.
     *
     * @param Mailer $mail
     */
    public function __construct(Mailer $mail)
    {
        $this->mailer = $mail;
    }

    /**
     * Handle the event.
     *
     * @param  NewUserRegistration $event
     * @return void
     */
    public function handle(NewUserRegistration $event)
    {
        $this->mailer->send(new EmailVerificationMail($event->user));
    }

}
