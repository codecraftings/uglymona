<?php
namespace Mona\Core\User\Registration;

use Mona\Events\Event;

use Illuminate\Queue\SerializesModels;
use Mona\Core\User\User;

class NewUserRegistration extends Event {

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public $user;
	public function __construct(User $user)
	{
		$this->user = $user;
	}

}
