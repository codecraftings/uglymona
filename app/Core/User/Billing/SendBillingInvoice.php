<?php


namespace Mona\Core\User\Billing;


use Illuminate\Contracts\Queue\ShouldQueue;
use Mona\Core\Mail\Composers\InvoiceEmail;
use Mona\Core\Mail\Composers\MembershipUpgradeEmail;
use Mona\Core\Mail\Mailer;
use Mona\Core\User\Subscription\Subscription;

class SendBillingInvoice implements ShouldQueue
{
    /**
     * @var Mailer
     */
    protected $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function handle(BillingWasSuccessful $event)
    {
        $this->mailer->send(new InvoiceEmail($event->transaction));
    }
}