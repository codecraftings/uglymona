<?php


namespace Mona\Core\User\Billing;


use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Event;
use Mona\Core\Mail\Composers\InvoiceEmail;
use Mona\Core\Mail\Mailer;
use Mona\Core\User\User;

abstract class BillingManager implements BillingInterface
{
    /**
     * @var Card
     */
    protected $cards;
    /**
     * @var Transaction
     */
    protected $transactions;

    public function __construct(Card $cards, Transaction $transactions)
    {
        $this->cards = $cards;
        $this->transactions = $transactions;
    }

    public function getAllCards(User $user)
    {
        return $user->cards;
    }

    public function getActiveCard(User $user)
    {
        $card = $this->cards->query()->orderBy('id','desc')->first();
        if (!$card) {
            throw new \Exception("User does not have any card associated!");
        }
        return $card;
    }

    abstract function processCardData(array $cardData);

    public function attachCard(User $user, array $cardData)
    {
        $cardData = $this->processCardData($cardData);
        $card = $this->cards->create([
            'user_id'    => $user->id,
            'last_four'  => $cardData['last_four'],
            'billing_id' => $cardData['billing_id']
        ]);
        return $card;
    }

    public function removeCard(User $user, Card $card)
    {
        return $user->cards()->detach($card);
    }

    public function logTransaction(User $user, Card $card, $amount, array $details = [])
    {
        $transaction = $this->transactions->create([
            "user_id" => $user->id,
            "card_id" => $card->id,
            "amount"  => $amount
        ]);
        event(new BillingWasSuccessful($transaction));
        return $transaction;
    }

    public function charge(User $user, $amount, Card $card = null)
    {
        if ($card == null) {
            $card = $this->getActiveCard($user);
        }
        $details = $this->processPayment($card, $amount);
        $transaction = $this->logTransaction($user, $card, $amount, $details);
        $card->save();
        return $transaction;
    }

    /**
     * @param Card $card
     * @param $amount
     * @throws PaymentFailedException
     * @return array
     */
    public abstract function processPayment(Card $card, $amount);
}