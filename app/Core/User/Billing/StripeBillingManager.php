<?php


namespace Mona\Core\User\Billing;


use Illuminate\Support\Facades\Config;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Error\Card as CardException;
use Stripe\Stripe;

class StripeBillingManager extends BillingManager
{
    public function __construct(Card $card, Transaction $transaction)
    {
        parent::__construct($card, $transaction);
        Stripe::setApiKey(Config::get("services.stripe.secret"));
        Stripe::setVerifySslCerts(false);
    }

    function processCardData(array $cardData)
    {
        $customer = Customer::create([
            "description" => "Uglymona full membership",
            "source"      => $cardData['card_token']
        ]);
        $cardData['billing_id'] = $customer->id;
        return $cardData;
    }

    /**
     * @param Card $card
     * @param $amount
     * @return array
     * @throws PaymentFailedException
     */
    public function processPayment(Card $card, $amount)
    {
        try {
            $charge = Charge::create([
                "amount"               => $amount * 100,
                "currency"             => 'usd',
                "customer"             => $card->billing_id,
                "statement_descriptor" => "Uglymona",
                "receipt_email"        => $card->user->email
            ]);
        } catch (CardException $e) {
            throw new PaymentFailedException();
        }
        return [
            "amount"         => $amount,
            "transaction_id" => $charge->balance_transaction
        ];
    }
}