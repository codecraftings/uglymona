<?php


namespace Mona\Core\User\Billing;


use Mona\Core\User\User;

interface BillingInterface {
    public function getAllCards(User $user);
    public function getActiveCard(User $user);
    public function attachCard(User $user, array $cardData);
    public function processCardData(array $cardData);
    public function removeCard(User $user, Card $card);
    public function charge(User $user, $amount, Card $card = null);
    public function processPayment(Card $card, $amount);
    public function logTransaction(User $user, Card $card, $amount, array $details = []);
}