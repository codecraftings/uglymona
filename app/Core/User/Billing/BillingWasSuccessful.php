<?php


namespace Mona\Core\User\Billing;


use Illuminate\Queue\SerializesModels;
use Mona\Events\Event;

class BillingWasSuccessful extends Event {
    use SerializesModels;

    /**
     * @var Transaction
     */
    public $transaction;

    public function __construct(Transaction $transaction)
    {

        $this->transaction = $transaction;
    }
}