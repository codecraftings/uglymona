<?php


namespace Mona\Core\User\Billing;


use Illuminate\Database\Eloquent\Model;
use Mona\Core\User\User;

/**
 * Class Transaction
 * @package Mona\Core\User\Billing
 * @property User user
 * @property Card card
 * @property mixed amount
 */
class Transaction extends Model
{
    protected $table = "transactions";
    protected $fillable = ['user_id', 'card_id', 'amount'];

    public function card()
    {
        return $this->belongsTo(Card::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}