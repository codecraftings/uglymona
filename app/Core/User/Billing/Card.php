<?php


namespace Mona\Core\User\Billing;


use Illuminate\Database\Eloquent\Model;
use Mona\Core\BaseModel;
use Mona\Core\User\User;

/**
 * Class Card
 * @package Mona\Core\User\Billing
 * @property integer id
 * @property \Mona\Core\User\User $user
 * @property integer user_id
 * @property string billing_id
 * @property string last_four
 */
class Card extends Model{
    protected $table = "cards";
    protected $fillable = ['user_id','billing_id','last_four'];
    public function user(){
        return $this->belongsTo(User::class);
    }
}