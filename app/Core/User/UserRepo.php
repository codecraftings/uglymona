<?php
namespace Mona\Core\User;

use League\Flysystem\Exception;
use Illuminate\Contracts\Hashing\Hasher as Hash;
use Mona\Core\Services\Search\Paginator;
use Carbon\Carbon;

class UserRepo
{
    protected $user;
    protected $profile;
    protected $hash;
    protected $connection;
    protected $paginator;

    public function __construct(Paginator $paginator, Connection $connection, Hash $hash, User $user, Profile $profile)
    {
        $this->user = $user;
        $this->paginator = $paginator;
        $this->connection = $connection;
        $this->profile = $profile;
        $this->hash = $hash;
    }

    public function get($user)
    {
        if ($user instanceof User) {
            return $user;
        }
        $user = $this->user->find($user);
        if (!$user) {
            throw new \Exception("User doesnot exists");
        }
        return $user;
    }

    public function getByEmail($email)
    {
        $user = $this->user->where('email', '=', $email);
        if ($user->count() < 1) {
            return false;
        }
        return $user->first();
    }

    public function search($filters, $pagging = 10, $page = 1, $withProfile = true)
    {
        $q = $this->user->query();
        foreach ($filters as $key => &$value) {
            if (empty($value)) {
                unset($filters[$key]);
            }
        }
        if (isset($filters['full_name'])) {
            $parts = explode(" ", $filters['full_name']);
            $q->where(function ($q) use ($filters, $parts) {
                $q->where('full_name', 'like', '%' . $filters['full_name'] . '%');
                foreach ($parts as $part) {
                    $q->orWhere('full_name', 'like', '%' . $part . '%');
                }
            });
        }
        if (isset($filters['exclude'])) {
            $q->whereNotIn('id', $filters['exclude']);
        }
        $addressFilters = array_only($filters, ['address', 'city', 'state', 'zip', 'country']);
        $profileFilters = array_only($filters, ["gender",
                                                "looking_for",
                                                "looking_for_age",
                                                "zodiac_sign",
                                                "relationship_status",
                                                "want_children",
                                                "education",
                                                "work",
                                                "personality",
                                                "ethnicity",
                                                "build_type",
                                                "hair_color",
                                                "eye_color",
                                                "do_smoke",
                                                "do_drink"
        ]);
        if (count($addressFilters) > 0) {
            foreach ($addressFilters as $key => $value) {
                $q->where($key, "like", "%" . $value . "%");
            }
        }
        $q->whereHas('profile', function ($q) use ($profileFilters, $filters) {
            $q->where('profile_completion', '>', 50);
            if (count($profileFilters) > 0) {
                $q->where($profileFilters);
            }
            $q->where(function ($q) use ($filters) {
                if (isset($filters["age"])) {
                    $dt = Carbon::now()->subYears($filters['age']);
                    $q->whereBetween("date_of_birth", [$dt->copy()->subYears(3), $dt->copy()->addYears(2)]);
                    return;
                }
                if (isset($filters["lessThanAge"])) {
                    $q->where("date_of_birth", "<=", Carbon::now()->subYears($filters["lessThanAge"]));
                }
                if (isset($filters["greaterThanAge"])) {
                    $q->where("date_of_birth", ">=", Carbon::now()->subYears($filters["greaterThanAge"]));
                }
            });
        });
        if (isset($filters['sort'])) {
            switch ($filters['sort']) {
                case 'updated':
                    $q->orderBy('updated_at', 'desc');
                    break;
                case 'old':
                    $q->orderBy('created_at', 'asc');
                    break;
                case 'random':
                    $q->orderBy(\DB::Raw('rand()'));
                    break;
                default:
                    $q->orderBy('created_at', 'desc');
                    break;
            }
        } else {
            $q->orderBy("updated_at", "desc");
        }
        if ($withProfile) {
            $q->with('profile');
        }
        return $this->paginator->make($q, $pagging, $page);
    }

    public function sendConnection($user, $other)
    {
        $this->connection->create(array(
            "user_id"      => $other,
            "connected_id" => $user,
            "status"       => Connection::STATUS_PENDING
        ));
    }

    public function connectionRequests($user)
    {
        $q = $this->connection->where("user_id", $user)->where("status", Connection::STATUS_PENDING);
        return $q->get();
    }

    public function acceptConnection($user, $other)
    {
        $q = $this->connection->where("user_id", $other)->where("connected_id", $user)->where("status", Connection::STATUS_PENDING);
        $q->update(array("status" => Connection::STATUS_ACTIVE));
        $this->connection->create(array(
            "user_id"      => $user,
            "connected_id" => $other,
            "status"       => Connection::STATUS_ACTIVE
        ));
        return true;
    }

    public function isBothConnected($user, $other)
    {
        $q = $this->connection->where("user_id", $other)->where("connected_id", $user)->where("status", Connection::STATUS_ACTIVE);
        if ($q->count() < 1) {
            return false;
        }
        return true;
    }

    public function create(array $data)
    {
        if ($this->hash->needsRehash($data['password'])) {
            $data['password'] = $this->hash->make($data['password']);
        }
        $data["membership"] = Membership::FULL;
        $data["email_status"] = str_random(30);
        $user = $this->user->create(array_except($data, []));
        //$user->password = $data['password'];
        //$user->membership = Membership::BASIC;
        //$user->email_status = str_random(30);
        //$user->save();
        $data['user_id'] = $user->id;
        $profile = $this->profile->create($data);
        return $user;
    }

    public function setUserMembership(User $user, $membership)
    {
        if (!Membership::validMembership($membership)) {
            throw new \Exception("Membership type not valid!");
        }
        $user->membership = $membership;
        $user->save();
        return $user;
    }

    public function update($user, array $data)
    {
        $user = $this->get($user);
        if (isset($data['email'])) {
            if ($data['email'] == $user->email) {
                unset($data['email']);
            }
        }
        if (isset($data['username'])) {
            if ($data['username'] == $user->username) {
                unset($data['username']);
            }
        }
        if (isset($data['password']) && !$data['password']) {
            unset($data['password']);
        }
        if (isset($data['password']) && $data['password']) {
            $data['password'] = $this->hash->make($data['password']);
        }
        if(isset($data['membership'])){
            unset($data['membership']);
        }
        $user->update($data);
        if (isset($data['password'])) {
            $user->password = $data['password'];
            $user->save();
        }
        if (isset($data['profile'])) {
            if(isset($data['profile']['date_of_birth'])){
                $data['profile']['date_of_birth'] = Carbon::parse($data['profile']['date_of_birth']);
            }
            $user->profile->update($data['profile']);
            $user->profile->profile_completion = $this->calculateProfileCompletion($user);
            $user->profile->save();
        }
        return $user;
    }

    private function calculateProfileCompletion($user)
    {
        $c = 20;
        if ($user->city) {
            $c += 10;
        }
        if ($user->state) {
            $c += 5;
        }
        if ($user->zip) {
            $c += 5;
        }
        if ($user->country) {
            $c += 10;
        }
        if ($user->profile->age > 5 && $user->profile->age < 150) {
            $c += 10;
        }
        if($user->profile->relationship_status){
            $c += 10;
        }
        if($user->profile->education){
            $c += 10;
        }
        return $c;
    }

    public function verifyEmail($email, $token)
    {
        /** @var \Mona\Core\User\User $user */
        $user = $this->getByEmail($email);
        if (!$user) {
            throw new \Exception('No account associated with this email');
        }
        if ($user->email_verified()) {
            throw new \Exception('Email already verified');
        }
        if ($user->email_status !== $token) {
            throw new Exception("Invalid token");
        } else {
            $user->email_status = User::EMAIL_VERIFIED;
            $user->save();
            return $user;
        }
    }
}