<?php


namespace Mona\Core\User\Subscription;


use Carbon\Carbon;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mona\Jobs\Job;

class SubscriptionManagementJob extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels, DispatchesJobs;

    /**
     * @var SubscriptionManager
     */
    private $subscriptions;
    private $specialQueue = "managementqueue";

    public function __construct(SubscriptionManager $subscription)
    {
        $this->subscriptions = $subscription;
    }

    public function handle(Carbon $carbon)
    {
        $currentTime = $carbon->now();
        $this->checkTrialSubscriptions();
        $this->checkActiveSubscriptions($currentTime);
        $this->checkEndedSubscriptions($currentTime);
    }

    private function checkTrialSubscriptions()
    {
        $subscriptions = $this->subscriptions->getSubscriptionsOnTrial();
        /** @var Subscription $subscription */
        foreach ($subscriptions as $subscription) {
            if ($subscription->trial_ends_on->isPast()) {
                $subscription->setStatus(Subscription::STATUS_ACTIVE);
                $subscription->save();
            }
        }
    }

    private function checkActiveSubscriptions(Carbon $currentTime)
    {
        $subscriptions = $this->subscriptions->getActiveSubscriptions();
        /** @var Subscription $subscription */
        foreach ($subscriptions as $subscription) {
            if ($subscription->next_payment_due->lt($currentTime->addMinutes(10))) {
                $this->dispatch((new ProcessPaymentJob($subscription))->onQueue($this->specialQueue));
            }
        }
    }

    private function checkEndedSubscriptions(Carbon $currentTime)
    {
        $this->checkIfEnd($this->subscriptions->getDeactivatedSubscriptions());
        $this->checkIfEnd($this->subscriptions->getCancelledSubscriptions());
    }

    private function checkIfEnd($subscriptions)
    {
        /** @var Subscription $subscription */
        foreach ($subscriptions as $subscription) {
            if (($subscription->status == Subscription::STATUS_DEACTIVATED || $subscription->status == Subscription::STATUS_CANCELLED) && $subscription->next_payment_due->isPast()) {
                $subscription->setStatus(Subscription::STATUS_ENDED);
                $subscription->save();
            }
        }
    }
}