<?php


namespace Mona\Core\User\Subscription;


use Carbon\Carbon;

interface SubscriptionDelegateInterface {
    public function getPaymentAmount(Subscription $subscription);
    public function getNextPaymentDate(Subscription $subscription, Carbon $lastPayment);
    public function getTrialPeriod(Subscription $subscription);
    public function onSubscriptionCreated(Subscription $subscription);
    public function onSubscriptionTrialStarted(Subscription $subscription);
    public function onSubscriptionActivated(Subscription $subscription);
    public function onPaymentFailed(Subscription $subscription);
    public function onSubscriptionDeactivated(Subscription $subscription);
    public function onSubscriptionCancelled(Subscription $subscription);
    public function onSubscriptionEnded(Subscription $subscription);
}