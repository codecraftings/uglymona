<?php


namespace Mona\Core\User\Subscription;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Mona\Core\Mail\Mailer;
use Mona\Core\User\User;
use Mona\Core\User\UserRepo;

/**
 * Class Subscription
 * @package Mona\Core\User\Subscription
 * @property string delegate_class
 * @property User user
 * @property string status
 * @property array coupons
 * @property integer payment_count
 * @property integer failed_tries
 * @property Carbon trial_ends_on
 * @property Carbon next_payment_due
 * @property Carbon last_payment
 * @property Carbon last_failed_try
 */
class Subscription extends Model
{
    const STATUS_CREATED = "created";
    const STATUS_TRIAL = "trial";
    const STATUS_PROCESSING = "processing";
    const STATUS_ACTIVE = "active";
    const STATUS_DEACTIVATED = "deactivated";
    const STATUS_CANCELLED = "cancelled";
    const STATUS_ENDED = "ended";
    protected $table = "subscriptions";
    protected $casts = [
        'coupons' => 'array'
    ];
    private $_delegate;
    protected $fillable = [
        "user_id",
        "delegate_class",
        "coupons"
    ];

    public function getDates()
    {
        return ['next_payment_due', 'last_payment', 'last_failed_try'];
    }

    public function setStatus($newStatus)
    {
        switch ($newStatus) {
            case self::STATUS_CREATED:
                $this->onCreated();
                break;
            case self::STATUS_TRIAL:
                $this->onTrialStarted();
            case self::STATUS_ACTIVE:
                $this->onActivated();
                break;
            case self::STATUS_CANCELLED:
                $this->onCancelled();
                break;
            case self::STATUS_DEACTIVATED:
                $this->onDeactivated();
                break;
            case self::STATUS_ENDED:
                $this->onEnded();
                break;
            default:
                break;
        }
        $this->status = $newStatus;
        return $this;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return SubscriptionDelegateInterface|object
     * @throws \Exception
     */
    public function delegate()
    {
        if (!$this->_delegate) {
            $delegate = $this->attributes['delegate_class'];
            $ref = new \ReflectionClass($delegate);
            $this->_delegate = $ref->newInstanceArgs([App::make(UserRepo::class), App::make(Mailer::class)]);
        }
        return $this->_delegate;
    }

    public function getTrialPeriod()
    {
        return $this->delegate()->getTrialPeriod($this);
    }

    public function getPaymentAmount()
    {
        return $this->delegate()->getPaymentAmount($this);
    }

    public function getNextPaymentDate($lastPayment)
    {
        return $this->delegate()->getNextPaymentDate($this, $lastPayment);
    }

    public function onPaymentSuccess(Carbon $lastPayment)
    {
        $this->next_payment_due = $this->getNextPaymentDate($this->next_payment_due);
        $this->last_payment = $lastPayment;
        $this->payment_count += 1;
        $this->failed_tries = 0;
        $this->save();
    }

    public function onPaymentFailed(Carbon $triedTime)
    {
        $this->last_failed_try = $triedTime;
        $this->failed_tries += 1;
        if ($this->failed_tries > 2) {
            $this->setStatus(Subscription::STATUS_DEACTIVATED);
        }
        $this->save();
        $this->delegate()->onPaymentFailed($this);
    }

    public function onCreated()
    {
        return $this->delegate()->onSubscriptionCreated($this);
    }

    public function onTrialStarted()
    {
        return $this->delegate()->onSubscriptionTrialStarted($this);
    }

    public function onCancelled()
    {
        return $this->delegate()->onSubscriptionCancelled($this);
    }

    public function onActivated()
    {
        return $this->delegate()->onSubscriptionActivated($this);
    }

    public function onDeactivated()
    {
        return $this->delegate()->onSubscriptionDeactivated($this);
    }

    public function onEnded()
    {
        return $this->delegate()->onSubscriptionEnded($this);
    }


}