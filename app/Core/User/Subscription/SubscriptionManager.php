<?php


namespace Mona\Core\User\Subscription;


use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Mona\Core\User\Billing\BillingInterface;
use Mona\Core\User\Billing\PaymentFailedException;
use Mona\Core\User\User;

class SubscriptionManager
{
    /**
     * @var Subscription
     */
    private $subscriptions;
    /**
     * @var Carbon
     */
    private $carbon;
    /**
     * @var BillingInterface
     */
    private $billing;

    /**
     * @param Subscription $subscriptions
     * @param Carbon $carbon
     * @param BillingInterface $billing
     */
    public function __construct(Subscription $subscriptions, Carbon $carbon, BillingInterface $billing)
    {
        $this->subscriptions = $subscriptions;
        $this->carbon = $carbon;
        $this->billing = $billing;
    }

    public function create(User $user, $delegateClass, array $coupons = [])
    {
        $subscription = $this->subscriptions->newInstance([
            "user_id"  => $user->id,
            "delegate_class" => $delegateClass,
            "coupons"  => $coupons
        ]);
        $subscription->trial_ends_on = $this->carbon->now()->addDays($subscription->getTrialPeriod());
        $subscription->next_payment_due = $subscription->trial_ends_on;
        $subscription->setStatus(Subscription::STATUS_CREATED);
        if ($subscription->getTrialPeriod() > 0) {
            $subscription->setStatus(Subscription::STATUS_TRIAL);
        } else {
            $subscription->setStatus(Subscription::STATUS_ACTIVE);
        }
        $subscription->save();
        return $subscription;
    }

    public function processPayment(Subscription $subscription)
    {
        $now = $this->carbon->now();
        $previousStatus = $subscription->status;
        $this->setStatus($subscription, Subscription::STATUS_PROCESSING);
        try {
            $this->billing->charge($subscription->user, $subscription->getPaymentAmount());
            $subscription->onPaymentSuccess($now);
        } catch (PaymentFailedException $e) {
            $subscription->onPaymentFailed($now);
        } catch (\Exception $e) {
            throw $e;
        } finally {
            if ($subscription->status == Subscription::STATUS_PROCESSING) {
                $this->setStatus($subscription, $previousStatus);
            }
        }
    }

    public function setStatus(Subscription $subscription, $status)
    {
        $subscription->setStatus($status);
        $subscription->save();
    }

    public function get($id)
    {
        if ($id instanceof Subscription) {
            return $id;
        }
        return $this->subscriptions->findOrFail($id);
    }

    public function getSubscriptionsOnTrial()
    {
        return $this->getByStatus(Subscription::STATUS_TRIAL);
    }

    public function getActiveSubscriptions()
    {
        return $this->getByStatus(Subscription::STATUS_ACTIVE);
    }

    public function getDeactivatedSubscriptions()
    {
        return $this->getByStatus(Subscription::STATUS_DEACTIVATED);
    }

    public function getCancelledSubscriptions()
    {
        return $this->getByStatus(Subscription::STATUS_CANCELLED);
    }

    public function getEndedSubscriptions()
    {
        return $this->getByStatus(Subscription::STATUS_ENDED);
    }

    /**
     * @param $status
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function getByStatus($status)
    {
        $q = $this->subscriptions->query()->where('status', '=', $status);
        return $q->get();
    }
}