<?php


namespace Mona\Core\User\Subscription;


use Carbon\Carbon;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mona\Core\User\Billing\BillingInterface;
use Mona\Core\User\Billing\PaymentFailedException;
use Mona\Jobs\Job;

class ProcessPaymentJob extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    /**
     * @var Subscription
     */
    private $subscription;
    private $previousStatus;

    public function __construct(Subscription $subscription)
    {
        $this->subscription = $subscription;
    }

    public function handle(SubscriptionManager $manager)
    {
        $manager->processPayment($this->subscription);
    }

}