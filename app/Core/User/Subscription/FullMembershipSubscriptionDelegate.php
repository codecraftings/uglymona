<?php


namespace Mona\Core\User\Subscription;


use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Config;
use Mona\Core\Mail\Composers\MembershipUpgradeEmail;
use Mona\Core\Mail\Mailer;
use Mona\Core\Mail\PostMan;
use Mona\Core\User\Membership;
use Mona\Core\User\UserRepo;

class FullMembershipSubscriptionDelegate implements SubscriptionDelegateInterface
{

    /**
     * @var UserRepo
     */
    private $users;
    /**
     * @var Mailer
     */
    private $mailer;

    public function __construct(UserRepo $users, Mailer $mailer)
    {
        $this->users = $users;
        $this->mailer = $mailer;
    }

    public function getPaymentAmount(Subscription $subscription)
    {
        return Config::get("billing.fee.annual");
    }

    public function getNextPaymentDate(Subscription $subscription, Carbon $lastPayment)
    {
        return $lastPayment->copy()->addYears(1);
    }

    public function getTrialPeriod(Subscription $subscription)
    {
        return 0;
    }

    public function onSubscriptionCreated(Subscription $subscription)
    {
        return;
    }

    public function onSubscriptionTrialStarted(Subscription $subscription)
    {
        return $this->activateFullMembership($subscription);
    }

    public function onSubscriptionActivated(Subscription $subscription)
    {
        return $this->activateFullMembership($subscription);
    }

    public function onPaymentFailed(Subscription $subscription)
    {

    }

    public function onSubscriptionDeactivated(Subscription $subscription)
    {
    }

    public function onSubscriptionCancelled(Subscription $subscription)
    {
    }

    public function onSubscriptionEnded(Subscription $subscription)
    {
        return $this->deactivateFullMembership($subscription);
    }

    /**
     * @param Subscription $subscription
     * @throws \Exception
     */
    protected function activateFullMembership(Subscription $subscription)
    {
        if ($subscription->user->membership->order < Membership::FULL) {
            $this->users->setUserMembership($subscription->user, Membership::FULL);
            $this->mailer->post(new MembershipUpgradeEmail($subscription));
        }
    }

    /**
     * @param Subscription $subscription
     * @throws \Exception
     */
    protected function deactivateFullMembership(Subscription $subscription)
    {
        if ($subscription->user->membership->order == Membership::FULL) {
            $this->users->setUserMembership($subscription->user, Membership::BASIC);
        }
    }
}