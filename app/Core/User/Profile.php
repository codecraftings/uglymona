<?php
namespace Mona\Core\User;
use Carbon\Carbon;
use Mona\Core\BaseModel;

/**
DB Fields
-----------
id
user_id
profile_photo_id
date_of_birth
gender
looking_for
looking_for_age
zodiac_sign
relationship_status
want_children
education
work
personality
ethnicity
build_type
hair_color
eye_color
do_smoke
do_drink
about_text
likes_count
views_count
*/

/**
 * Class Profile
 * @package Mona\Core\User
 * @property Carbon date_of_birth
 */
class Profile extends BaseModel{
	const GENDER_MALE = "man";
	const GENDER_FEMALE = "woman";
	protected $table = "user_profile";
	protected $fillable = array(
			'user_id',
			'profile_photo_id',
			'date_of_birth',
			'gender',
			'looking_for',
			'looking_for_age',
			'zodiac_sign',
			'relationship_status',
			'want_children',
			'education',
			'work',
			'personality',
			'ethnicity',
			'build_type',
			'hair_color',
			'eye_color',
			'do_smoke',
			'do_drink',
			'about_text'
		);
	protected $casts = [
		"date_of_birth"
	];
	protected $appends = ['age','profile_pic'];
	public  function getProfilePicAttribute(){
		if($this->profile_photo_id==0){
			return asset_url('images/avater.png', "local");
		}else{
			return $this->photo->url['original'];
		}
	}
	public function getAgeAttribute(){
		try{
			return $this->date_of_birth->age;
		}catch(\Exception $e){
			return 0;
		}
	}
	public $timestamps = false;
	public function user(){
		return $this->belongsTo('Mona\Core\User\User');
	}
	public function photo(){
		return $this->belongsTo('Mona\Core\Photo\Photo','profile_photo_id');
	}
	public function getDates(){
		return ['date_of_birth'];
	}
	public function allFieldsArray(){
		return [
			ucfirst($this->work),
			ucfirst($this->education),
			ucfirst($this->want_children?"Want children":"Don't want children"),
			ucfirst($this->relationship_status),
			ucfirst($this->do_smoke?"Smoker":"Non-smoker")
		];
	}
	public function getDateOfBirthAttribute(){
		$d = Carbon::parse($this->attributes['date_of_birth']);
		if($d->timestamp<0){
			return false;
		}
		return $d;
	}
}