<?php
namespace Mona\Core\User;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Illuminate\Database\Eloquent\Model;
use Mona\Core\BaseModel as Base;
use Mona\Core\Photo\Photo;
use Mona\Core\User\Billing\Card;
use Mona\Core\Video\Video;

/**
 * class User
 * @package \Mona\Core\User
 * @property integer id
 * @property string full_name
 * @property string username
 * @property string email
 * @property string password
 * @property string email_status
 * @property string remember_token
 * @property DateTime created_at
 * @property DateTime updated_at
 * @property DateTime last_login
 * @property Membership membership
 * @property string address
 * @property string city
 * @property string state
 * @property string zip
 * @property string country
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{

    use Authenticatable, CanResetPassword;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    const EMAIL_VERIFIED = "verified";
    const GENDER_MALE = "man";
    const GENDER_FEMALE = "woman";
    //protected $hidden = array('password', 'email_status');
    protected $fillable = array('full_name', 'username', 'email', "password", "email_status", 'address', 'city', 'state', 'zip', 'country');
    protected $appends = array('permalink','name','location', 'permissions');

    public function getLocationAttribute(){
        return $this->city.", ".$this->state.", ".$this->country;
    }
    public function getNameAttribute(){
        if(!$this->full_name){
            return $this->username;
        }
        return $this->full_name;
    }
    public function hasPermission($type)
    {
        return $this->membership->hasPermission($type);
    }

    public function getPermissionsAttribute()
    {
        return $this->membership->availablePermissions();
    }

    public function getPermalinkAttribute()
    {
        return web_url('users/' . $this->id);
    }

    public function getMembershipAttribute()
    {
        //return new Membership($this->attributes['membership']);
        return new Membership(Membership::FULL);
    }
    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    public function videos()
    {
        return $this->hasMany(Video::class);
    }
    public function cards(){
        return $this->hasMany(Card::class);
    }

    public function email_verified()
    {
        return $this->email_status == User::EMAIL_VERIFIED;
    }
    public function getDates()
    {
        return ['last_login', 'created_at', 'updated_at'];
    }
}
