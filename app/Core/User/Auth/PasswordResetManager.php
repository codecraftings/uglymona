<?php


namespace Mona\Core\User\Auth;


use Exception;
use Illuminate\Contracts\Auth\PasswordBroker;
use Mona\Core\User\UserRepo;

class PasswordResetManager
{
    /**
     * @var UserRepo
     */
    private $users;
    /**
     * @var PasswordBroker
     */
    private $broker;

    public function __construct(UserRepo $users, PasswordBroker $broker)
    {

        $this->users = $users;
        $this->broker = $broker;
    }

    public function sendResetLink($email)
    {
        $flag = $this->broker->sendResetLink(['email' => $email]);
        if ($flag === PasswordBroker::INVALID_USER) {
            throw new Exception("This email doesn't exist in our record", 422);
        } else {
            return true;
        }
    }

    public function confirmPassReset(array $credentials)
    {
        $flag = $this->broker->reset($credentials, function ($user, $pass) {
            $this->users->update($user, [
                "password" => $pass
            ]);
        });
        switch ($flag) {
            case PasswordBroker::INVALID_USER:
                throw new \Exception("Invalid email address", 422);
                break;
            case PasswordBroker::INVALID_TOKEN:
                throw new \Exception("Invalid token provided", 422);
                break;
            case PasswordBroker::INVALID_PASSWORD:
                throw new Exception("Password mismatch", 422);
                break;
            case PasswordBroker::PASSWORD_RESET:
            default:
                break;
        }
        return true;
    }
}