<?php


namespace Mona\Core\User\Auth;


use Mona\Http\Requests\ApiRequest;

class SetNewPasswordRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'email'                 => 'required|email',
            'token'                 => 'required',
            'password'              => 'required|min:6',
            'password_confirmation' => 'required'
        ];
    }
}