<?php

namespace Mona\Core\User\Auth;


use Exception;
use Illuminate\Http\Request;
use Mona\Core\User\Auth\AuthManager;
use Mona\Core\User\Membership;
use Mona\Core\User\User;
use Mona\Http\Route;
use Tymon\JWTAuth\JWTAuth;

class AuthMiddleware
{
    /**
     * @var AuthManager
     */
    private $auth;
    /**
     * @var Route
     */
    private $route;

    public function __construct(AuthManager $auth, Route $route)
    {

        $this->auth = $auth;
        $this->route = $route;
    }

    public function handle(Request $request, \Closure $next)
    {
        $accessLevel = $this->route->getAccessLevel();
        if ($accessLevel > Membership::NONE) {
            try {
                /** @var \Mona\Core\User\User $user */
                $user = $this->getUser($request);
            } catch (Exception $e) {
                return api_error($e->getMessage(), 401);
            }
            if ($user->membership->order < $accessLevel) {
                return api_error("You are not authorized to access this", 401);
            }
            $permissions = $this->route->getPermissions();
            if (count($permissions) > 0) {
                if (false == $user->membership->hasPermissions($permissions)) {
                    return api_error("You don't have necessary permissions", 401);
                }
            }
        }
        return $next($request);
    }

    private function getUser($request)
    {
        if (!$token = $this->auth->parseToken($request)) {
            throw new Exception("No Token Provided", 401);
        }
        $user = $this->auth->user($token);
        if (!$user) {
            throw new Exception("Token not valid", 401);
        }
        return $user;
    }
}