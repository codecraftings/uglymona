<?php
namespace Mona\Core\User\Auth;


use Mona\Http\Requests\ApiRequest;

class LoginRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'email'    => 'email|required',
            'password' => 'required'
        ];
    }
}