<?php
namespace Mona\Core\User\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Mona\Core\User\User;
use Tymon\JWTAuth\JWTAuth;

class AuthManager
{
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    public function createToken(array $credentials)
    {
        return $this->jwt->attempt($credentials);
    }

    public function createTokenForUser(User $user)
    {
        return $this->jwt->fromUser($user);
    }

    public function parseToken(Request $request)
    {
        return $this->jwt->setRequest($request)->parseToken()->getToken();
    }

    public function isTokenValid($token)
    {
        return $this->jwt->authenticate($token);
    }

    public function invalidate($token)
    {
        try {
            $this->jwt->invalidate($token);
        } catch (\Exception $e) {
            return;
        }
    }

    public function user($token)
    {
        return $this->jwt->toUser($token);
    }
    public function getExpireTime($token){
        return time() + Config::get('jwt.ttl') * 60;
    }
    public function getNewToken($oldToken)
    {
        return $this->jwt->refresh($oldToken);
    }
}