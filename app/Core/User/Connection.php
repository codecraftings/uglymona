<?php
namespace Mona\Core\User;
/*
id
user_id
connected_id
status
created_at
updated_at
*/
class Connection extends \Mona\Core\BaseModel{
	const STATUS_PENDING = "pending";
	const STATUS_ACTIVE = "active";
	protected $table = "connections";
	protected $fillable = array("user_id","connected_id", "status");
}