<?php


namespace Mona\Core\User\Matching;


use Mona\Core\User\User;

class Match implements \Serializable
{
    /**
     * @var User
     */
    private $matchedUser;

    public function __construct(User $matchedUser, $currentUser)
    {
        $this->matchedUser = $matchedUser;
        $this->matchedUser->load('profile');
    }

    public function matchPercentage()
    {
        return mt_rand(60, 80);
    }

    public function toArray()
    {
        $data = $this->matchedUser->toArray();
        $data['percentage'] = $this->matchPercentage();
        $data['profile_fields'] = $this->matchedUser->profile->allFieldsArray();
        return $data;
    }

    public function __toString()
    {
        return $this->serialize();
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return json_encode($this->toArray());
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        // TODO: Implement unserialize() method.
    }
}