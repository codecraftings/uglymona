<?php
namespace Mona\Core\User\Matching;

use Mona\Core\User\UserRepo;
use Mona\Core\User\User;

class MatchFinder
{
    protected $userRepo;
    protected $current_user;

    public function __construct(UserRepo $user)
    {
        $this->userRepo = $user;
    }

    public function setUser($user)
    {
        $this->current_user = $user;
    }

    public function findOne()
    {
        $match = $this->findMatches(1)[0];
        if (!$match) {
            throw new \Exception("No Match Found!");
        }
        return $match;
    }

    public function findMatches($num = 1)
    {
        $filters = array();
        $this->addFiltersForUser($filters);
        $filters['sort'] = "random";
        $matches = $this->userRepo->search($filters, $num, 1);
        if ($matches->count() < 1) {
            return false;
        }
        $arr = [];
        foreach($matches as $match){
            array_push($arr, new Match($match, $this->current_user));
        }
        return $arr;
    }

    private function addFiltersForUser(array &$filters)
    {
        if (false == $this->current_user) {
            return;
        }
        $filters['exclude'] = [$this->current_user->id];
        $age = $this->current_user->profile->age;
        if ($this->current_user->profile->gender == User::GENDER_MALE) {
            $filters['lessThanAge'] = $age;
            $filters['gender'] = User::GENDER_FEMALE;
        } else {
            $filters['greaterThanAge'] = $age;
            $filters['gender'] = User::GENDER_MALE;
        }
    }
}	