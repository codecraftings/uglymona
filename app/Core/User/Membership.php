<?php
namespace Mona\Core\User;

/*
DB_Fields
-----------
id
label
permissions
*/
use Illuminate\Support\Facades\Config;
use Serializable;

class Membership implements Serializable
{
    const NONE = 0;
    const BASIC = 1;
    const FULL = 2;
    const ADMIN = 3;
    public $permissions = [];
    public $order;
    public $label;

    public static function validMembership($membership){
        return in_array($membership, [self::BASIC, self::FULL, self::ADMIN]);
    }
    public function __construct($id)
    {
        $config = Config::get('permissions');
        $this->order = $id;
        $this->label = $config[$id]['label'];
        for ($i = $id; $i >= Membership::NONE; $i --) {
            $this->permissions = array_merge($this->permissions, $config[$i]['permissions']);
        }
    }

    public function availablePermissions()
    {
        return $this->permissions;
    }

    public function hasPermissions(array $permissions)
    {
        $flag = true;
        foreach ($permissions as $permission) {
            if (!$this->hasPermission($permission)) {
                $flag = false;
                break;
            }
        }
        return $flag;
    }

    public function hasPermission($type)
    {
        $permissions = Config::get('permissions');
        $permissions = array_keys($permissions);
        if (!in_array($type, $permissions)) {
            throw new \Exception("Permission does not exist");
        }
        return in_array($type, $this->availablePermissions());
    }

    public function __toString()
    {
        return $this->serialize();
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return json_encode($this);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        $o = json_decode($serialized);
        $this->order = $o->order;
        $this->label = $o->label;
        $this->permissions = $o->permissions;
    }
}