<?php
namespace Mona\Core\Blog;
/*
id
title
content
author_id
author_name
is_pdf
pdf_link
summary
created_at
updated_at
*/
use Mona\Core\BaseModel;

class Blog extends BaseModel{
	protected $table = "blogs";
	protected $fillable = array('title','content','author_id');
	protected $appends = ['post_date', 'permalink'];
	protected $casts = [
		"is_pdf" => "boolean"
	];
	public function getPermalinkAttribute(){
		return web_url("blogs/".$this->id."/".urlencode(substr($this->title, 0, 50)));
	}
	public function getPostDateAttribute(){
		return $this->created_at->format("M D, Y");
	}
	public function getPdfLinkAttribute(){
		return web_url($this->attributes['pdf_link']);
	}
	public function getContentAttribute(){
		return nl2br($this->attributes['content']);
	}
	public function category(){
		return $this->belongsTo(Category::class);
	}
}