<?php


namespace Mona\Core\Blog;


class BlogsRepo
{
    /**
     * @var Blog
     */
    private $blog;
    /**
     * @var Category
     */
    private $category;

    public function __construct(Blog $blog, Category $category)
    {
        $this->blog = $blog;
        $this->category = $category;
    }

    public function get($id)
    {
        return $this->blog->findOrFail($id);
    }

    public function getAll($filters = [], $pagging = 10, $offset = 0)
    {
        $q = $this->blog->query();
        if (isset($filters['category'])) {
            $category = $this->getCategory($filters['category']);
            $q->where('category_id', $category->id);
        }
        $q->orderBy("id", "desc");
        return $q->skip($offset)->take($pagging)->get();
    }

    public function getCategory($id)
    {
        if (is_numeric($id)) {
            return $this->category->findOrFail($id);
        } else if (is_string($id)) {
            return $this->category->where('slag', '=', $id)->first();
        } else if ($id instanceof Category) {
            return $id;
        } else {
            throw new \Exception("Category Not Found!");
        }
    }

    public function getCategories()
    {
        return $this->category->query()->orderBy('id','desc')->get();
    }
}