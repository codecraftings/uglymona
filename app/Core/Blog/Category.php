<?php


namespace Mona\Core\Blog;


use Mona\Core\BaseModel;

class Category extends BaseModel {
    protected $table = "categories";
    protected $fillable = ['label','slag'];
    public $timestamps = false;
    public function blogs(){
        return $this->hasMany(Blog::class);
    }
}