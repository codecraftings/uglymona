<?php
namespace Mona\Core\Video;
/*
id
user_id
privacy
status
title
description
source_path
thumb_id
cdn_name
allow_comments
available_formats
created_at
updated_at
*/

use Mona\Core\BaseModel;

class Video extends BaseModel{
	const PRIVACY_PUBLIC = "anyone";
	const PRIVACY_USERS = "users";
	const PRIVACY_CONNECTIONS = "friends";
	const STATUS_PUBLISHED = "published";
	const STATUS_DRAFT = "draft";
	protected $table = "videos";
	protected $fillable = array("user_id","privacy", "title","description","source_path","thumb_id","cdn_name","allow_comments", "available_formats");
	protected $appends = array('permalink', 'source', 'thumb_img');
	protected $casts = [
		"available_formats" => 'array'
	];
	public function getPermalinkAttribute(){
		return web_url('videos/'.$this->id."/".urlencode($this->title));
	}
	public function getSourceAttribute(){
		$urls = [];
		foreach($this->available_formats as $format){
			$path = $this->source_path.$this->id.".".$format;
			$urls[$format] = asset_url($path, $this->cdn_name);
		}
		return $urls;
	}
	public function thumb(){
		return $this->belongsTo('Mona\Core\Photo\Photo','thumb_id');
	}
	public function getThumbImgAttribute(){
//		return $this->thumb->url["original"];
	}
	public function user(){
		return $this->belongsTo('Mona\Core\User\User');
	}
}