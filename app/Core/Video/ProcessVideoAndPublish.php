<?php namespace Mona\Core\Video;

use Illuminate\Support\Facades\Log;
use Mona\Commands\Command;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Mona\Jobs\Job;

class ProcessVideoAndPublish extends Job implements SelfHandling {

	use InteractsWithQueue, SerializesModels;
	/**
	 * @var
	 */
	private $temp_file;
	/**
	 * @var Video
	 */
	private $video;

	public function __construct($temp_file, Video $video)
	{
		$this->temp_file = $temp_file;
		$this->video = $video;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$this->delete();
		try{
			Log::info("heelo");
		}catch (\Exception $e){
			$this->release(40);
		}
	}

}
