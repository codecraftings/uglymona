<?php
namespace Mona\Core\Video;
use Mona\Core\Video\Video;
use Mona\Core\User\UserRepo;
use Illuminate\Support\MessageBag;
use Mona\Core\Services\Search\Paginator;
class VideoRepo{
	protected $video;
	protected $user;
	protected $errors;
	protected $paginator;
	public function __construct(Paginator $paginator, Video $video, UserRepo $user, MessageBag $errors){
		$this->video = $video;
		$this->user = $user;
		$this->paginator = $paginator;
		$this->errors = $errors;
	}
	public function errors(){
		return $this->errors;
	}
	public function get($id){
		if($id instanceof Video){
			return $id;
		}
		$video = $this->video->find($id);
		if(!$video){
			$this->errors()->add("video","Video Not Found!");
			return false;
		}
		return $video;
	}
	public function canAccess($video, $user){
		$video = $this->get($video);
		switch ($video->privacy) {
			case Video::PRIVACY_PUBLIC:
				return true;
				break;
			case Video::PRIVACY_USERS:
				return $user!=false;
				break;
			case Video::PRIVACY_CONNECTIONS:
				if(!$user)
					return false;
				elseif($user->id==$video->user->id)
					return true;
				else
					return $this->user->isBothConnected($video->user, $user);
				break;
			default:
				return false;
				break;
		}
	}
	public function search($str, $filters, $pagging = 10, $page = 1, $onlyPublic = true){
		$q = $this->video->query()->with("user","thumb");
		if($onlyPublic){
			$q->where('privacy','=',Video::PRIVACY_PUBLIC);
		}
		else {
			$q->where(function($query){
				$query->where('privacy','=',Video::PRIVACY_PUBLIC)
					->orWhere('privacy','=', Video::PRIVACY_USERS);
			});
		}
		if($str){
			$q->where('title','like','%'.$str.'%');
		}
		if(isset($filters['sort'])){
			switch ($filters['sort']) {
				case 'updated':
					$q->orderBy('updated_at','desc');
					break;
				case 'old':
					$q->orderBy('created_at','asc');
					break;
				default:
					$q->orderBy('created_at','desc');
					break;
			}
		}else{
			$q->orderBy("updated_at","desc");
		}
		return $this->paginator->make($q, $pagging, $page);
	}
	public function createDraftEntry($user_id, $privacy, $title = "", $description = "", $allow_comments = false){
		$data = [
			"user_id" => $user_id,
			"privacy"	=> $privacy,
			'title' => $title,
			'description' => $description,
			'status'  => Video::STATUS_DRAFT,
			'allow_comments' => $allow_comments
		];
		$video = $this->video->create($data);
		return $video;
	}
	public function update($video, array $data){
		$video = $this->get($video);
		return $video->update($video);
	}
}