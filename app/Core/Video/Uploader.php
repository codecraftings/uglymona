<?php


namespace Mona\Core\Video;


use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Uploader
{
    /**
     * @var Storage
     */
    private $storage;
    private $temp_dir = "uploads/temp/";
    private $accepted_mimes = ["video/mp4", "video/webm"];
    const UPLOAD_ABORT_CODE = "405";
    const UPLOAD_RETRY_CODE = "402";
    const UPLOAD_CONTINUE_CODE = "410";

    public function __construct(Factory $storage)
    {
        $this->storage = $storage;
    }

    private function getChunkName($file_id, $chunk_number)
    {
        return $file_id . "--" . $chunk_number;
    }

    public function ensureChunkDoesNotExist($file_id, $chunk_number)
    {
        if ($this->chunkExists($file_id, $chunk_number)) {
            throw new \Exception("Chunk already exits!", self::UPLOAD_CONTINUE_CODE);
        }
    }

    public function chunkExists($file_id, $chunk_number)
    {
        $filename = $this->getChunkName($file_id, $chunk_number);
        if ($this->storage->exists($this->temp_dir . $filename)) {
            return true;
        }
        return false;
    }

    public function ensureChunkIsValid($filepart)
    {
        if (!$filepart instanceof UploadedFile) {
            throw new \Exception("No chunk data provided", self::UPLOAD_ABORT_CODE);
        }
        if (!$filepart->isValid()) {
            throw new \Exception("File chunk not valid", self::UPLOAD_RETRY_CODE);
        }
        $mime = $filepart->getMimeType();
        if (array_search($mime, $this->accepted_mimes) < 0) {
            throw new \Exception("File format not supported", self::UPLOAD_ABORT_CODE);
        }
    }

    public function acceptChunk($filepart, $file_id, $chunk_number)
    {
        $contents = file_get_contents($filepart->getRealPath());
        $chunk_name = $this->getChunkName($file_id, $chunk_number);
        $this->storage->put($this->temp_dir . $chunk_name, $contents);
    }

    public function ensureAllChunkReceived($file_id, $total_chunks)
    {
        for ($i = 0; $i < $total_chunks; $i ++) {
            $chunk_name = $this->getChunkName($file_id, $i);
            if (!$this->storage->exists($this->temp_dir . $chunk_name)) {
                throw new \Exception("Not all parts were uploaded", self::UPLOAD_RETRY_CODE);
            }
        }
    }

    public function assembleChunks($file_id, $total_chunks, $expected_filesize)
    {
        $file_name = $file_id . "." . $this->detectExtension($file_id);
        $combind_file = $this->temp_dir . $file_name;
        if ($this->storage->exists($combind_file)) {
            $this->storage->delete($combind_file);
        }
        $contents = "";
        $chunks = [];
        for ($i = 0; $i < $total_chunks; $i ++) {
            $chunk = $this->temp_dir . $this->getChunkName($file_id, $i);
            if ($this->storage->exists($chunk)) {
                $contents .= $this->storage->get($chunk);
                array_push($chunks, $chunk);
            } else {
                throw new \Exception("All parts not found", self::UPLOAD_RETRY_CODE);
            }
        }
        $this->storage->put($combind_file, $contents);
        if ($this->storage->size($combind_file) != $expected_filesize) {
            throw new \Exception("Filesize mismatch", self::UPLOAD_RETRY_CODE);
        }
        $this->storage->delete($chunks);
        return $combind_file;
    }

    private function detectExtension($file_id)
    {
        $chunkName = $this->getChunkName($file_id, 0);
        $first_chunk = $this->temp_dir . $chunkName;
        $content = $this->storage->get($first_chunk);
        $this->storage->disk("storage")->put("temp/" . $chunkName, $content);
        $file = new File(storage_path() . "/app/temp/" . $chunkName);
        $ext = $file->guessExtension();
        $this->storage->disk("storage")->delete("temp/" . $chunkName);
        return $ext;
    }
}