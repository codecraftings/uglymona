<?php
namespace Mona\Core\Letter;

/**
 * class Letter
 * @package \Mona\Core\Letter
 * @property integer id
 * @property integer user_id
 * @property string status
 * @property string privacy
 * @property string message
 * @property string reply
 * @property \Mona\Core\User\User user
 * @property DateTime replied_at
 * @property DateTime created_at
 * @property DateTime updated_at
 */
use Mona\Core\BaseModel;

class Letter extends BaseModel
{
    const STATUS_RECEIVED = 0;
    const STATUS_REPLIED = 1;
    const STATUS_PUBLISHED = 2;
    const PRIVACY_PUBLIC = 0;
    const PRIVACY_USERS = 1;
    const PRIVACY_CONNECTIONS = 2;
    protected $table = "letter";
    protected $fillable = array("user_id", "status", "privacy", "message", "reply", "replied_at");
    protected $appends = array("formatted_message", "formatted_reply");

    public function getFormattedMessageAttribute()
    {
        $msg = "Dear Mona, \n" . $this->message . "\n ";
        return nl2br(e($msg));
    }

    public function getFormattedReplyAttribute()
    {
        $reply = "Dear " . $this->user->name . ",\n" . $this->reply . "\nLove,\nMona";
        return nl2br(e($reply));
    }

    public function getDates()
    {
        return ["replied_at", "created_at", "updated_at"];
    }

    public function user()
    {
        return $this->belongsTo("Mona\Core\User\User");
    }
}