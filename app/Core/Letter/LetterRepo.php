<?php
namespace Mona\Core\Letter;

use Mona\Core\Services\Search\Paginator;

class LetterRepo
{
    protected $letter;
    protected $paginator;

    public function __construct(Paginator $paginator, Letter $letter)
    {
        $this->letter = $letter;
        $this->paginator = $paginator;
    }

    public function get($letter)
    {
        if ($letter instanceof Letter) {
            return $letter;
        }
        $letter = $this->letter->find($letter);
        if (!$letter) {
            throw new \Exception("Letter not found", 404);
        }
        return $letter;
    }

    public function search($str, $filters, $pagging = 20, $page = 1, $onlyPublished = true, $onlyPublic = true)
    {
        $q = $this->letter->query();
        if ($onlyPublic) {
            $q->where('privacy', '=', Letter::PRIVACY_PUBLIC);
        } else {
            $q->where(function ($query) {
                $query->where('privacy', '=', Letter::PRIVACY_PUBLIC)
                    ->orWhere('privacy', '=', Letter::PRIVACY_USERS);
            });
        }
        if ($onlyPublished) {
            $q->where('status', '=', Letter::STATUS_PUBLISHED);
        }
        if ($str) {
            $q->where('message', 'like', '%' . $str . '%');
        }
        if (isset($filters['sort'])) {
            switch ($filters['sort']) {
                case 'updated':
                    $q->orderBy('updated_at', 'desc');
                    break;
                case 'old':
                    $q->orderBy('created_at', 'asc');
                    break;
                default:
                    $q->orderBy('created_at', 'desc');
                    break;
            }
        } else {
            $q->orderBy("updated_at", "desc");
        }
        return $this->paginator->make($q, $pagging, $page);
    }

    public function create($data)
    {
        if (false == isset($data['status'])) {
            $data['status'] = Letter::STATUS_RECEIVED;
        }
        if (false == isset($data['privacy'])) {
            $data['privacy'] = Letter::PRIVACY_PUBLIC;
        }
        $letter = $this->letter->create($data);
        if (!$letter) {
            throw new \Exception("Error creating letter entry!");
        }
        return $letter;
    }

    public function update($letter, $data)
    {
        $letter = $this->get($letter);
        if (!$letter->update($data)) {
            throw new \Exception("Error updating letter!");
        }
        return $letter;
    }

    public function delete($letter)
    {
        $letter = $this->get($letter);
        if (!$letter->delete()) {
            throw new \Exception("Error deleting letter!");
        }
        return true;
    }
}