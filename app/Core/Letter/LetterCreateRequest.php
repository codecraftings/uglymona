<?php


namespace Mona\Core\Letter;


use Mona\Http\Requests\AuthenticatedRequest;

class LetterCreateRequest extends AuthenticatedRequest {
    public function rules(){
        return [
            'message' => "required|min:100|max:3000",
            "user_id" => "required"
        ];
    }
    public function authorize(){
        return $this->user()->hasPermission('write_letter')&&$this->user()->id==$this->input('user_id');
    }
}