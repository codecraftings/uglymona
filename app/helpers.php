<?php
use Mona\Exceptions\DieAndDumpException;

function api_error($msg = "Bad Request", $code = 400, $stackTrace = null)
{
    $response = array(
        'status' => $code,
        'error'  => array('message' => $msg)
    );
    if ($stackTrace != null && \Config::get('app.debug') == true) {
        $response['stack'] = $stackTrace;
    }
    return \Response::json($response, $code);
}

function api_success($data, $code = 200)
{
    $response = array(
        'status' => $code
    );
    if (is_string($data)) {
        $response['message'] = $data;
    } elseif (is_array($data)) {
        $response = array_merge($response, $data);
    } else {
        $response['data'] = $data;
    }
    return Response::json($response, $code);
}

function direct($path)
{
    return Redirect::to($path);
}
function api_url($path = "/"){
    return concat_url(Config::get('app.api_url'), $path);
}
function web_url($path = "/"){
    return concat_url(Config::get('app.web_url'), $path);
}
function asset_url($path, $cdn_name = "self")
{
    $url = "";
    switch ($cdn_name) {
        case 's3':
            break;
        case 'self':
        case 'uploads':
        case 'local':
        default:
            $url = concat_url(Config::get('app.asset_url'), $path);
            break;
    }
    return $url;
}

function concat_url($base, $path)
{
    if (starts_with($path, "/")) {
        return $base . $path;
    } else {
        return $base . "/" . $path;
    }
}

function limit_str($str, $len, $tail = "..")
{
    if (strlen($str) <= $len) {
        return $str;
    }
    $str = substr($str, 0, $len) . $tail;
    return $str;
}
function test($var){
    throw new DieAndDumpException($var);
}